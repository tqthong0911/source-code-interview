import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { isNil, isEmpty, equals } from 'ramda';
import { formatMessage } from 'umi/locale';
import memoize from 'memoize-one';
import PageHeaderLayout from '@/layouts/PageHeaderLayout';
import { renderTextColumn, renderOptionText, renderBasicText } from '@/utils/layout/renderTable';
import StandardTable from '@/componentsPP/StandardTable';
import deleteModal from '@/componentsPP/DeleteModal';
import IconButton from '@/componentsPP/IconButton';
import { 
  CONFIRM_MODAL_TYPE, 
  PAGE_SIZE, 
  PP_FONT_MEDIUM, 
  PP_FONT_SEMIBOLD, 
  SORT_ORDER, 
  PP_TEXT_UPPERCASE,
} from '@/constants';
import { mapClientsToView } from './service';
import LeftAction from './components/LeftAction';
import messages from './messages';
import Styles from './styles.less';

function getSelectedRows(selectedRows, clients) {
  if (isEmpty(selectedRows)) {
    return [];
  }
  return selectedRows.reduce((acc, selectedRow) => {
    const { id: searchId } = selectedRow;
    const matched = clients.find(id => id === searchId);
    if (isNil(matched)) {
      return [...acc, selectedRow];
    }
    return [...acc, matched];
  }, []);
}

const memoizedGetSelectedRows = memoize(getSelectedRows, equals);

class ClientView extends PureComponent {
  static propTypes = {
    clients: PropTypes.array.isRequired,
    totalPage: PropTypes.number.isRequired,
    totalItem: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    currentPage: PropTypes.number.isRequired,
    handleReloadPage: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
  };

  state = {
    selectedRows: [],
  };

  static getDerivedStateFromProps(props, state) {
    const { clients } = props;
    if (isNil(clients) || isEmpty(clients)) {
      return {
        selectedRows: [],
      };
    }
    const { selectedRows } = state;
    return { selectedRows: memoizedGetSelectedRows(selectedRows, clients) } ;
  }

  getColumnConfig = () => {
    return [{
      title: renderOptionText(formatMessage(messages.name), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => (
        <Link to={`/client/clients/${record.id}`} className={PP_FONT_SEMIBOLD}>
          {renderOptionText(record.name, { lines: 2 })}
        </Link>
      ),
      sorter: true,
      defaultSortOrder: SORT_ORDER.ascend,
      width: '15%',
    }, {
      title: renderBasicText(formatMessage(messages.address)),
      dataIndex: 'address',
      key: 'address',
      render: renderTextColumn({ lines: '2' }),
      sorter: true,
      width: '25%',
    }, {
      title: renderBasicText(formatMessage(messages.phone)),
      dataIndex: 'phones',
      key: 'phones',
      render: renderTextColumn,
      sorter: true,
      width: '180px',
    }, {
      title: renderOptionText(formatMessage(messages.owner), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'owner',
      key: 'owner',
      render: renderTextColumn,
      width: '200px',
    }, {
      title: renderBasicText(formatMessage(messages.recentActivity)),
      dataIndex: 'recentActivity',
      key: 'recentActivity',
      render: renderTextColumn,
      width: '250px',
    }];
  };

  getPaginationConfig = () => {
    const { totalPage, currentPage } = this.props;
    const allItems = totalPage * PAGE_SIZE;
    return {
      simple: true,
      total: allItems,
      current: currentPage,
      pageSize: PAGE_SIZE,
    };
  };

  handleChange = (pagination, filters, sorter) => {
    const { columnKey, order } = sorter;
    const sort = isEmpty(sorter) ? undefined : { field: columnKey, order };
    const { props } = this;
    props.handleChange({ sort }, pagination.current);
  };

  handleSelectRow = selectedRows => {
    this.setState({ selectedRows });
  };

  handleRefresh = () => {
    const { handleReloadPage } = this.props;
    handleReloadPage();
  };

  handleCustomizeColumn = () => {
    // TODO
  };

  handleDelete = () => {
    const { handleDelete } = this.props;
    const { selectedRows } = this.state;
    const { name } = selectedRows[0];
    deleteModal({
      type: CONFIRM_MODAL_TYPE.delete,
      title: formatMessage(messages.deleteClient),
      content: (
        <div>
          {formatMessage(messages.deleteContent)}
          <span className={Styles.clientName}>{name}</span>
          ?
        </div>
      ),
      okText: formatMessage(messages.delete),
      cancelText: formatMessage(messages.cancel),
      onOk(closeModal) {
        return new Promise(() => handleDelete(selectedRows, closeModal)).catch(() => { });
      },
      onCancel() { },
    });
  };

  renderLeftAction = () => {
    const { clients } = this.props;
    const { selectedRows } = this.state;
    const selectedClients = clients.filter(client => {
      return selectedRows.some(row => row.id === client.id);
    });
    return (
      <LeftAction
        selectedClients={selectedClients}
        handleDelete={this.handleDelete}
        handleExport={() => { }}
      />
    );
  };

  renderTitle = () => {
    const { totalItem } = this.props;
    return (
      <div className={Styles.header}>
        <span className={PP_FONT_MEDIUM}>{`${formatMessage(messages.allClients)} (${totalItem})`}</span>
        <i className="material-icons">arrow_drop_down</i>
      </div>
    );
  };

  handleRefresh = () => {
    const { handleReloadPage } = this.props;
    handleReloadPage();
    this.setState({ selectedRows: [] })
  };

  renderRightAction = () => {
    return (
      <div className={Styles.rightAction}>
        <IconButton
          icon="refresh"
          tooltip={{ title: formatMessage(messages.refresh), placement: 'topRight' }}
          onClick={this.handleRefresh}
        />
      </div>
    );
  };

  render() {
    const { clients, loading } = this.props;
    const { selectedRows } = this.state;
    const data = {
      list: mapClientsToView(clients),
      pagination: this.getPaginationConfig(),
    };

    return (
      <PageHeaderLayout
        icon="menu-unfold"
        title={this.renderTitle()}
        hideBreadcrumb
        action={this.renderRightAction()}
        leftAction={this.renderLeftAction()}
      >
        <StandardTable
          scroll={{ y: 'calc(100vh - 276px)' }}
          loading={loading}
          data={data}
          columns={this.getColumnConfig()}
          selectedRows={selectedRows}
          onSelectRow={this.handleSelectRow}
          onChange={this.handleChange}
          rowKey={record => record.id}
          className={Styles.clientTable}
        />
      </PageHeaderLayout>
    );
  }
}

export default ClientView;
