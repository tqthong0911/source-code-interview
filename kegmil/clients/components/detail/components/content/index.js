import React from 'react';
import PropTypes from 'prop-types';
import Switch from '@/componentsPP/Switch';
import Overview from './components/overview';
import Jobs from './components/jobs';
import Assets from './components/assets';
import Cases from './components/cases';
import Contracts from './components/contracts';
import PMPlans from './components/pmPlans';
import { DETAIL_MENU } from '../../constants';
import Styles from './styles.less';

const { Item: SwitchItem } = Switch;

const Content = ({ activeMenu, client, initData, onFetchQuantity, clientId }) => {
  const { commonLocations } = initData;
  return (
    <div className={Styles.wrapper}>
      <Switch activeKey={activeMenu}>
        <SwitchItem key={DETAIL_MENU[0].key}>
          <Overview client={client} clientId={clientId} />
        </SwitchItem>
        <SwitchItem key={DETAIL_MENU[1].key}>
          <Assets client={client} initData={initData} />
        </SwitchItem>
        <SwitchItem key={DETAIL_MENU[2].key}>
          <Jobs client={client} initData={initData} />
        </SwitchItem>
        <SwitchItem key={DETAIL_MENU[3].key}>
          <Cases client={client} commonLocations={commonLocations} />
        </SwitchItem>
        <SwitchItem key={DETAIL_MENU[4].key}>
          <Contracts onFetchQuantity={onFetchQuantity} />
        </SwitchItem>
        <SwitchItem key={DETAIL_MENU[5].key}>
          <PMPlans onFetchQuantity={onFetchQuantity} />
        </SwitchItem>
      </Switch>
    </div>
  )
};

Content.propTypes = {
  activeMenu: PropTypes.string.isRequired,
  client: PropTypes.object,
}

Content.defaultProps = {
  client: {},
};

export default Content;
