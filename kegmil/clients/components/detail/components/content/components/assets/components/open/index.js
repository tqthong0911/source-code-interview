import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from 'umi/link';
import { formatMessage } from 'umi/locale';
import { isEqual } from 'lodash';
import { renderTextColumn, renderOptionText } from '@/utils/layout/renderTable';
import StandardTable from '@/componentsPP/StandardTable';
import { getOriginalId, convertAssetsToDataView } from '@/utils/asset';
import { PP_FONT_SEMIBOLD, PP_TEXT_UPPERCASE } from '@/constants';
import messages from '../../messages';

class Open extends PureComponent {
  static propTypes = {
    assets: PropTypes.array.isRequired,
    selectedRows: PropTypes.array.isRequired,
    loadingAssets: PropTypes.bool.isRequired,
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    const { assets } = nextProps;
    const reUpdateDataView = !isEqual(assets, nextState.props.assets);

    if (reUpdateDataView) {
      return {
        props: {
          assets,
        },
        dataView: convertAssetsToDataView(assets, true),
      }
    }
    return null;
  }

  constructor(props) {
    super(props);
    const { assets } = this.props;
    this.state = {
      props: {
        assets,
      },
      sortedInfo: null,
      dataView: convertAssetsToDataView(assets, true),
    };
  }

  get columnConfig() {
    let { sortedInfo } = this.state;
    sortedInfo = sortedInfo || {};
    return [{
      title: renderOptionText(formatMessage(messages.name), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'name',
      key: 'name',
      width: '50%',
      className: PP_FONT_SEMIBOLD,
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
      render: (text, record) => (
        <Link to={`/assets/${record.id}`}>
          {text}
        </Link>
      ),
    }, {
      title: renderOptionText(formatMessage(messages.id), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'assetNumber',
      key: 'assetNumber',
      sorter: (a, b) => a.assetNumber.length - b.assetNumber.length,
      sortOrder: sortedInfo.columnKey === 'assetNumber' && sortedInfo.order,
      render: renderTextColumn,
    }, {
      title: renderOptionText(formatMessage(messages.status), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'status',
      key: 'status',
      sorter: (a, b) => a.status.length - b.status.length,
      sortOrder: sortedInfo.columnKey === 'status' && sortedInfo.order,
      render: renderTextColumn,
    }];
  };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      sortedInfo: sorter,
    });
  };

  handleSelectRow = selectedRows => {
    const { handleSelectRow } = this.props;
    const newSelectedRows = selectedRows.filter(row => row.id === getOriginalId(row.id));
    handleSelectRow(newSelectedRows);
  };

  render() {
    const { loadingAssets, selectedRows } = this.props;
    const { dataView } = this.state;

    return (
      <StandardTable
        loading={loadingAssets}
        data={{ list: dataView }}
        columns={this.columnConfig}
        onChange={this.handleChange}
        selectedRows={selectedRows}
        onSelectRow={this.handleSelectRow}
        rowKey={record => record.id}
        scroll={{  y: 'calc(100vh - 480px)' }}
      />
    );
  }
}

export default Open;
