import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { isEqual } from 'lodash';
import moment from 'moment';
import { Dropdown, Button, Menu, Icon, Tooltip } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';
import { EMPTY_VALUE, DATE_TIME_FORMAT } from '@/constants';
import messages from '../messages';
import Styles from './LeftActionHeader.less';

class LeftActionHeader extends Component {
  static propTypes = {
    pmPlan: PropTypes.object.isRequired,
    pmPlanUpdating: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    handleChangeStatus: PropTypes.func.isRequired,
    handleRefresh: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
    initData: PropTypes.object.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    return !isEqual(nextProps, this.props);
  }

  render() {
    const {
      pmPlan: { name, status, planId, startDate, endDate },
      initData: { statuses = [] },
      pmPlanUpdating,
      loading,
      handleChangeStatus,
      handleRefresh,
      handleEdit,
      handleDelete,
    } = this.props;

    const startDateView = startDate ? moment(parseInt(startDate, 0)).format(DATE_TIME_FORMAT) : EMPTY_VALUE;
    const endDateView = endDate ? moment(parseInt(endDate, 0)).format(DATE_TIME_FORMAT) : EMPTY_VALUE;
    const menu = () => (
      <Menu>
        {statuses.map(item => {
          return <Menu.Item onClick={handleChangeStatus} key={item.id}>{item.name}</Menu.Item>
        })}
      </Menu>
    );

    return (
      <Fragment>
        <div className={Styles.wrapperLeftInfo}>
          <div style={{ display: 'flex' }}>
            <div className={Styles.description}>
              <p>{name}</p>
              <div className={Styles.subHeader}>
                <Tooltip placement="top" title={planId} arrowPointAtCenter>
                  {planId || EMPTY_VALUE}
                </Tooltip>

                &nbsp;&bull;&nbsp;
                <b>Start on</b>
                <Tooltip placement="top" title={startDateView} arrowPointAtCenter>
                  {` ${startDateView || EMPTY_VALUE}`}
                </Tooltip>

                &nbsp;&bull;&nbsp;
                <b>End on</b>
                <Tooltip placement="top" title={endDateView} arrowPointAtCenter>
                  {` ${endDateView || EMPTY_VALUE}`}
                </Tooltip>
              </div>
              <div className={Styles.wrapperFooter}>
                <div className={Styles.footer}>
                  <div className={Styles.dropdown}>
                    <Dropdown overlay={menu}>
                      <Button size="small" loading={pmPlanUpdating || loading}>
                        {status}
                      </Button>
                    </Dropdown>
                    <Icon type="down" style={{ margin: '0 5px' }} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={Styles.wrapperRightAction}>
          <IconButton onClick={handleRefresh} icon="refresh" tooltip={{ title: formatMessage(messages.refresh) }} />
          <IconButton onClick={handleEdit} icon="edit" tooltip={{ title: formatMessage(messages.edit) }} />
          <IconButton onClick={handleDelete} icon="delete" tooltip={{ title: formatMessage(messages.delete) }} />
        </div>
      </Fragment>
    );
  }
}

export default LeftActionHeader;
