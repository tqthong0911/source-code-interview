import React from 'react';
import { shallow } from 'enzyme';

import ClientDetailContainer, { NAMESPACE } from '..';
import ClientDetailView from '../view';

describe('<ClientDetailContainer />', () => {
  let props;
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    props = {
      client: {},
      initData: {},
      match: { params: { id: 'id' } },
      dispatch: jest.fn(),
      changedIndex: 1,
    };
    wrapper = shallow(<ClientDetailContainer.WrappedComponent {...props} />);
    wrappedInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(ClientDetailView).length).toBe(1);
    expect(props.dispatch).toBeCalled();
  });

  it('should call dispatch correctly when it calls fetchClientDetails', () => {
    wrappedInstance.fetchClientDetails();

    expect(props.dispatch).toBeCalledWith({
      type: `${NAMESPACE}/fetchClientDetails`,
      payload: { id: 'id' },
    });
  });

  it('should refresh page when its changedIndex props was changed', () => {
    wrapper.setProps({
      changedIndex: 2,
    });

    expect(props.dispatch).toBeCalled();
  });
});
