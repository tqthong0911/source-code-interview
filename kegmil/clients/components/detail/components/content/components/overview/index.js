import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { isNil, equals } from 'ramda';

import { NAMESPACE } from './constants';
import OverviewView from './view';

@connect((state) => ({
  locations: state[NAMESPACE].locations,
  contacts: state[NAMESPACE].contacts,
}))
class OverviewContainer extends PureComponent {

  state = {
    showHeader: false,
  }

  componentDidMount() {
    this.fetchContactsIfNeeded();
    this.fetchLocationsIfNeeded();
  }

  componentDidUpdate(prevProps) {
    const { clientId } = this.props;
    const { clientId: prevClientId } = prevProps;
    if (!equals(clientId, prevClientId)) {
      this.fetchContactsIfNeeded();
      this.fetchLocationsIfNeeded();
    }
  }

  hasClientId = () => {
    const { clientId } = this.props;
    return !isNil(clientId);
  }

  fetchLocationsIfNeeded = () => {
    if (!this.hasClientId()) {
      return;
    }
    this.fetchLocations();
  }

  fetchContactsIfNeeded = () => {
    if (!this.hasClientId()) {
      return;
    }
    this.fetchContacts();
  }

  handleTabChange = activeKey => this.setState({
    showHeader: activeKey === 'contacts' || activeKey === 'locations',
  });

  fetchContacts = () => {
    const { clientId, dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchContacts`,
      payload: { clientId },
    });
  }

  fetchLocations = () => {
    const { clientId, dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchLocations`,
      payload: { clientId },
    });
  }

  render() {
    const { client, contacts, locations } = this.props;
    const { showHeader } = this.state;
    return (
      <OverviewView
        client={client}
        contacts={contacts}
        locations={locations}
        fetchContacts={this.fetchContacts}
        showHeader={showHeader}
        onTabChange={this.handleTabChange}
        fetchLocations={this.fetchLocations}
      />
    );
  }
}

export default OverviewContainer;
export { default as model } from './model';
