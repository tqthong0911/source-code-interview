import React from 'react';
import { formatMessage } from 'umi/locale';

import IconButton from '@/componentsPP/IconButton';
import messages from '../messages';
import Styles from './RightAction.less';

const RightAction = () => {
  return (
    <div className={Styles.rightActionWrapper}>
      <IconButton
        icon="refresh"
        tooltip={{ title: formatMessage(messages.refresh), placement: 'topRight' }}
      />
      <IconButton
        icon="edit"
        tooltip={{ title: formatMessage(messages.edit), placement: 'topRight' }}
      />
      <IconButton
        icon="delete"
        tooltip={{ title: formatMessage(messages.delete), placement: 'topRight' }}
      />
      <IconButton
        icon="more_horiz"
        tooltip={{ title: formatMessage(messages.more), placement: 'topRight' }}
      />
    </div>
  );
};

export default RightAction;
