export const NAMESPACE = 'pages.client.detail.overview.locations';

export const ACTIONS = {
  new: 'new',
  edit: 'edit',
};
