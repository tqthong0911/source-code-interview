import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { isEqual } from 'lodash';
import { Spin, Tabs } from '@/componentsPP/UIElements';
import Detail from './components/detail';
import { NAMESPACE } from './constants';
import Styles from './styles.less';

const { TabPane } = Tabs;

@connect(state => ({
  initData: state[NAMESPACE].initData,
}))
class Overview extends PureComponent {
  static propTypes = {
    pmPlan: PropTypes.object.isRequired,
    initData: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
  };

  componentDidUpdate(prevProps) {
    const { pmPlan, dispatch } = this.props;
    if (!isEqual(pmPlan, prevProps.pmPlan)) {
      const { clientId, locationId, contractId } = pmPlan;
      if (clientId || locationId || contractId) {
        dispatch({
          type: `${NAMESPACE}/fetchInitData`,
          payload: { clientId, locationId, contractId },
        });
      }
    }
  }

  render() {
    const { pmPlan, initData, actions: { handleChangeAutoGen }, loading } = this.props;
    return (
      <Tabs defaultActiveKey="1" className={Styles.tabWrapper} type="card">
        <TabPane tab="Details" key="1" className={Styles.tabPane} forceRender>
          <Spin spinning={loading} wrapperClassName={Styles.contentView}>
            <Detail pmPlan={pmPlan} initData={initData} handleChangeAutoGen={handleChangeAutoGen} />
          </Spin>
        </TabPane>
      </Tabs>
    );
  }
}

export default Overview;
export { default as model } from './model';
