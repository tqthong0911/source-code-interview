import { NAMESPACE } from './constants';
import { deleteClientContact } from './services';

const initialState = {};

export default {
  namespace: NAMESPACE,

  state: initialState,

  effects: {
    *delete({ payload: { id, callback } }, { call }) {
      const response = yield call(deleteClientContact, id);

      if (response) {
        callback();
      }
    },
  },

  reducers: {
    clear: () => initialState,
  },
}
