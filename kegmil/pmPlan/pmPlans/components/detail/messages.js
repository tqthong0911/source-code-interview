import globalMessages from '@/messages';

export default {
  deletePMPlan: globalMessages.pmPlan.deletePMPlan,
  deleteContent: globalMessages.common.deleteContent,
  deleteJobCreatedByPmPlan: globalMessages.pmPlan.deleteJobCreatedByPmPlan,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  refresh: globalMessages.common.refresh,
  edit: globalMessages.common.edit,
  overview: globalMessages.common.overview,
  assets: globalMessages.asset.assets,
  jobs: globalMessages.job.jobs,
};
