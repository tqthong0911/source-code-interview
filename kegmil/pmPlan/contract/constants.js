export const NAMESPACE = 'pages.contract';

export const MODAL_TYPE = Object.freeze({
  import: 'import',
  contract: 'contract',
});
