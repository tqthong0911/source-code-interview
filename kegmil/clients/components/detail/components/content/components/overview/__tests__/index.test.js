import React from 'react';
import { shallow } from 'enzyme';

import OverviewContainer from '..';
import OverviewView from '../view';

describe('<OverviewContainer />', () => {
  let props;
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    props = {
      client: {},
      contacts: [],
      locations: [],
    };
    wrapper = shallow(<OverviewContainer.WrappedComponent {...props} />);
    wrappedInstance = wrapper.instance();
  });
  it('should render correctly', () => {
    expect(wrapper.find(OverviewView).length).toBe(1);
  });
  it('should update state correctly when it calls handleTabChange', () => {
    wrappedInstance.handleTabChange('contacts');

    expect(wrappedInstance.state.showHeader).toBeTruthy();
  });
  it('should update state correctly when it calls handleTabChange', () => {
    wrappedInstance.handleTabChange('locations');

    expect(wrappedInstance.state.showHeader).toBeTruthy();
  });
});
