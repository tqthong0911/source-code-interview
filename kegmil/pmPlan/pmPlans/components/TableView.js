import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { get, isEmpty, isEqual } from 'lodash';
import { formatMessage } from 'umi/locale';
import { Link } from 'dva/router';
import StandardTable from '@/componentsPP/StandardTable';
import { Tag } from '@/componentsPP/UIElements';
import { renderTextColumn, renderBasicText, renderOptionText } from '@/utils/layout/renderTable';
import { PAGE_SIZE, SORT_ORDER, PP_FONT_SEMIBOLD, EMPTY_VALUE } from '@/constants';
import { mapPMPlansToView } from '../service';
import messages from '../messages';
import Styles from './TableView.less';

class TableView extends PureComponent {
  static propTypes = {
    pmPlans: PropTypes.array.isRequired,
    jobsType: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    currentPage: PropTypes.number.isRequired,
    totalPage: PropTypes.number.isRequired,
    selectedRows: PropTypes.array.isRequired,
    handleUpdateSelectedRows: PropTypes.func.isRequired,
    handleChangeFilterTableView: PropTypes.func.isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  state = {
    data: {
      list: [],
    },
  }

  componentDidUpdate(prevProps) {
    const { pmPlans, totalPage } = this.props;
    if (!isEqual(prevProps.pmPlans, pmPlans) || !isEqual(totalPage, prevProps.totalPage)) {
      this.setState({
        data: {
          list: mapPMPlansToView(pmPlans),
          pagination: this.getPaginationConfig,
        },
      })
    }
  };

  get getPaginationConfig() {
    const { totalPage, currentPage } = this.props;
    const allItems = totalPage * PAGE_SIZE;
    return {
      simple: true,
      current: currentPage,
      total: allItems,
      pageSize: PAGE_SIZE,
    };
  };

  get getColumnConfig() {
    return [{
      title: renderBasicText(formatMessage(messages.id)),
      dataIndex: 'planId',
      key: 'planId',
      render: (text, record) => {
        return (
          <Link to={`/pm-plan/pm-plans/${record.id}`}>
            {renderOptionText(text, { className: PP_FONT_SEMIBOLD })}
          </Link>
        )
      },
      sorter: true,
      defaultSortOrder: SORT_ORDER.ascend,
      width: '20%',
    }, {
      title: renderBasicText(formatMessage(messages.name)),
      dataIndex: 'name',
      key: 'name',
      render: renderTextColumn,
      sorter: true,
    }, {
      title: renderBasicText(formatMessage(messages.jobType)),
      dataIndex: 'jobType',
      key: 'jobType',
      sorter: true,
      render: this.renderTypeColumn,
      width: '100px',
    }, {
      title: renderBasicText(formatMessage(messages.client)),
      dataIndex: 'clientName',
      key: 'clientId',
      render: (text, { clientId }) => (
        <Link to={`/client/clients/${clientId}`} className={PP_FONT_SEMIBOLD}>
          {renderBasicText(text)}
        </Link>
      ),
      sorter: true,
      width: '20%',
    }, {
      title: renderBasicText(formatMessage(messages.startDate)),
      dataIndex: 'startDate',
      key: 'startDate',
      sorter: true,
      render: renderTextColumn,
      width: '10%',
    }, {
      title: renderBasicText(formatMessage(messages.endDate)),
      dataIndex: 'endDate',
      key: 'endDate',
      sorter: true,
      render: renderTextColumn,
      width: '10%',
    }];
  };

  getJobType = (id) => {
    if (id === EMPTY_VALUE) {
      return id;
    }
    const { jobsType } = this.props;
    return get(jobsType.find(jobType => jobType.id === id), 'name', EMPTY_VALUE);
  };

  renderTypeColumn = text => (
    <Tag className={Styles.tag}>
      {renderOptionText(this.getJobType(text), { className: PP_FONT_SEMIBOLD })}
    </Tag>
  );

  handleChange = (pagination, _, sorter) => {
    const { columnKey, order } = sorter;
    const sort = isEmpty(sorter) ? undefined : { field: columnKey, order };
    const { handleChangeFilterTableView } = this.props;
    handleChangeFilterTableView(sort, pagination.current);
  };

  render() {
    const { selectedRows, handleUpdateSelectedRows, loading, className } = this.props;
    const { data } = this.state;

    return (
      <StandardTable
        scroll={{ y: 'calc(100vh - 262px)' }}
        loading={loading}
        data={data}
        columns={this.getColumnConfig}
        selectedRows={selectedRows}
        onSelectRow={handleUpdateSelectedRows}
        onChange={this.handleChange}
        rowKey={record => record.id}
        className={className}
      />
    );
  }
}

export default TableView;
