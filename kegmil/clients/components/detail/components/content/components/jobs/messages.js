import globalMessages from '@/messages';

export default {
  edit: globalMessages.common.edit,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  deleteJob:globalMessages.job.deleteJob,
  deleteContent: globalMessages.common.deleteContent,
  deleteMultiContent: globalMessages.common.deleteMultiContent,

  id: globalMessages.common.id,
  type: globalMessages.job.type,
  status: globalMessages.job.status,
  priority: globalMessages.job.priority,
  startTime: globalMessages.job.startTime,
  endTime: globalMessages.job.endTime,
};
