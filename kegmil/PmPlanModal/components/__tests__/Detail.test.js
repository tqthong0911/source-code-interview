import { getWrapper } from '@/utils/test';

import { Form } from '@/componentsPP/UIElements';
import Detail from '../Detail';

const { Item: FormItem } = Form;

describe('<Detail />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      form: {
        getFieldDecorator: () => jest.fn(),
        getFieldValue: () => jest.fn(),
      },
    };
    wrapper = getWrapper(Detail, props);
  });

  it('should render correctly', () => {
    expect(wrapper.find(FormItem).length).toBe(6);
  });
});
