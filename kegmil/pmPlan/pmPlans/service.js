import moment from 'moment';
import request from '@/utils/request';
import { buildFilterParam } from '@/utils/job';
import { EMPTY_VALUE, PAGE_SIZE, DATE_TIME_MODIFY_FORMAT } from '@/constants';

export async function getPMPlans(filter = {}, currentPage = 1) {
  const filterQueryString = buildFilterParam(filter);
  return request(`/job/contract/pmplans?page=${currentPage}&size=${PAGE_SIZE}${filterQueryString}`);
}

export function mapPMPlansToView(pmPlans) {
  return pmPlans.map((pmPlan) => {
    const {
      id,
      planId,
      name,
      jobType,
      clientName,
      clientId,
      endDate,
      startDate,
    } = pmPlan;

    return {
      id,
      clientId,
      name: name || EMPTY_VALUE,
      jobType: jobType || EMPTY_VALUE,
      planId: planId || EMPTY_VALUE,
      clientName: clientName || EMPTY_VALUE,
      endDate: endDate ? moment(+endDate).format(DATE_TIME_MODIFY_FORMAT) : EMPTY_VALUE,
      startDate: startDate ? moment(+startDate).format(DATE_TIME_MODIFY_FORMAT) : EMPTY_VALUE,
    };
  });
}
