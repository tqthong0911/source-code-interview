import { pathOr } from 'ramda';
import { fetchClientPMPlans as clientPMPlansFetcher } from '@/utils/pmPlan';

export const fetchClientPMPlans = async(clientId, params) => {
  const response = await clientPMPlansFetcher({
    ...params,
    clientId,
  })();
  const pmPlans = pathOr([], ['results', 'pmPlans'], response);
  const totalPage = pathOr(0, ['results', 'totalPage'], response);
  const totalItem = pathOr(0, ['results', 'totalItem'], response);
  return {
    pmPlans,
    totalPage,
    totalItem,
  };
}
