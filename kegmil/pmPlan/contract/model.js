import { get } from 'lodash';
import { getContract, deleteContract } from './service';
import { NAMESPACE } from './constants';

const model = {
  namespace: NAMESPACE,

  state: {
    contracts: [],
    totalPage: 0,
    totalItem: 0,
  },

  effects: {
    *fetch({ payload: { currentPage, sort } }, { call, put }) {
      const response = yield call(getContract, { sort }, currentPage);
      if (response) {
        yield put({
          type: 'save',
          payload: {
            contracts: get(response, 'results.contracts', []),
            totalPage: get(response, 'results.totalPage', 0),
            totalItem: get(response, 'results.totalItem', 0),
          },
        });
      }
    },
    *delete({ payload }, { call }) {
      const { contractId, clientId, callback } = payload;
      yield call(deleteContract, contractId, clientId);
      callback();
    },
  },

  reducers: {
    save(state, { payload: { contracts, totalPage, totalItem } }) {
      return {
        ...state,
        contracts,
        totalPage,
        totalItem,
      };
    },
    clear(state) {
      return {
        ...state,
        contracts: [],
        totalPage: 0,
        totalItem: 0,
      };
    },
  },
};

export default model;
