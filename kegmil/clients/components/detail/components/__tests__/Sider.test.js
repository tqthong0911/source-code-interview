import React from 'react';
import { shallow } from 'enzyme';

import { Menu } from '@/componentsPP/UIElements';

import Sider from '../Sider';

describe('<ClientDetailView />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      menu: [],
      activeMenu: '',
      onMenuSelect: jest.fn(),
    };
    wrapper = shallow(<Sider {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Menu).length).toBe(1);
  });
});
