import React from 'react';
import PropTypes from 'prop-types';
import Styles from './DetailItem.less';
import { renderOptionText } from '@/utils/layout/renderTable';

const DetailItem = ({ label, children }) => (
  <div className={Styles.detailItem}>
    <span className={Styles.label}>{label}</span>
    {renderOptionText(children, { className: Styles.value })}
  </div>
);

DetailItem.propTypes = {
  label: PropTypes.string.isRequired,
}

export default DetailItem;
