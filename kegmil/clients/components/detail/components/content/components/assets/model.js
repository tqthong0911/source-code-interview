import { get } from 'lodash';
import { getAssetByClient, deleteAsset } from '@/utils/asset';
import { getAssetDetail } from './service';
import { NAMESPACE } from './constants';

const model = {
  namespace: NAMESPACE,

  state: {
    assets: [],
  },

  effects: {
    *getListAssetDetail({ clientId }, { call, all, put }) {
      const responseAssets = yield call(getAssetByClient, clientId);
      const assetIds = get(responseAssets, 'results.assets', []).map(({ id }) => id);

      const assetDetailPromises = assetIds.map(assetId => {
        return call(getAssetDetail, assetId);
      });

      const responseAssetDetails = yield all(assetDetailPromises);
      if (responseAssetDetails) {
        yield put({
          type: 'saveListAssetDetail',
          payload: responseAssetDetails,
        });
      }
    },
    *deleteAssets({ assetIds, callback }, { call, all }) {
      const assetsDeletePromises = assetIds.map(assetId => {
        return call(deleteAsset, assetId);
      });
      yield all(assetsDeletePromises);
      callback();
    },
  },
  reducers: {
    saveListAssetDetail(state, { payload }) {
      const assets = payload.map(item => get(item, 'results.asset', {}));
      return {
        ...state,
        assets,
      }
    },
  },
};

export default model;
