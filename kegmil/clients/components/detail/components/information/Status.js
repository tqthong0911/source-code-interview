import React from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { Menu, Dropdown, Icon } from '@/componentsPP/UIElements';
import messages from '../../messages';
import Styles from './Status.less';

const { Item: MenuItem } = Menu;

const statusActive = formatMessage(messages.active);
const statusInactive = formatMessage(messages.inActive);

const getMenu = () => (
  <Menu>
    <MenuItem key="0">
      {statusInactive}
    </MenuItem>
    <MenuItem key="1">
      {statusActive}
    </MenuItem>
  </Menu>
);

const Status = (props) => {
  const { client } = props;
  const { active } = client;
  const isActive = active === 1;
  const statusText = isActive ? statusActive : statusInactive;
  return (
    <div className={Styles.wrapper}>
      <Dropdown overlay={getMenu(props)}>
        <div className={Styles.statusDropdown}>
          <span className={Styles.status}>
            {statusText}
          </span>
          <Icon type="down" className={Styles.arrow} />
        </div>
      </Dropdown>
    </div>
  );
}

Status.propTypes = {
  client: PropTypes.object,
}

Status.defaultProps = {
  client: {},
}

export default Status;
