import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { isEqual, isEqualWith, get, set, cloneDeep, parseInt } from 'lodash';
import moment from 'moment';
import PmPlanModalView from './view';
import { mapToFormData, valueComparator } from './service';
import { NAMESPACE, PMPLAN_FORM } from './constants';

@connect(state => ({
  pmPlan: state[NAMESPACE].pmPlan,
  initData: state[NAMESPACE].initData,
  loading: state.loading.effects[`${NAMESPACE}/create`] ||
    state.loading.effects[`${NAMESPACE}/update`] ||
    state.loading.effects[`${NAMESPACE}/initData`] ||
    state.loading.effects[`${NAMESPACE}/fetchDataWhenClientChange`],
}))
class PmPlanModalContainer extends PureComponent {
  static propTypes = {
    pmPlanId: PropTypes.string,
    clientId: PropTypes.string,
    pmPlan: PropTypes.object,
    initData: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    handleCancel: PropTypes.func.isRequired,
    callback: PropTypes.func,
    dispatch: PropTypes.func.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
    pmPlan: {},
    pmPlanId: '',
    clientId: '',
    callback: null,
  };

  constructor(props) {
    super(props);
    const { clientId } = props;
    const pmPlanInfo = mapToFormData({ clientId });
    this.state = {
      pmPlanInfo,
    };
    this.pmPlanInfo = pmPlanInfo;
    this.viewRef = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { pmPlan, clientId, pmPlanId, isOpen, dispatch } = this.props;
    if (isOpen && isOpen !== prevProps.isOpen) {
      dispatch({
        type: `${NAMESPACE}/initData`,
        payload: { pmPlanId, clientId },
      });
    }
    if (!isEqual(pmPlan, prevProps.pmPlan) || !isEqual(clientId, prevProps.clientId)) {
      const newPmPlan = clientId ? { ...pmPlan, clientId } : pmPlan;
      const pmPlanInfo = mapToFormData(newPmPlan);
      this.setState({ pmPlanInfo }, () => { this.pmPlanInfo = pmPlanInfo; });
    }
  }

  handleCancel = () => {
    const { handleCancel } = this.props;
    handleCancel(this.handleClearData);
  };

  handleClearData = () => {
    this.setState({ pmPlanInfo: {} }, () => {
      const { dispatch } = this.props;
      dispatch({ type: `${NAMESPACE}/clear` });
    });
  };

  handleSave = form => {
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { callback, dispatch } = this.props;
      const { pmPlanInfo: { assets } } = this.state;
      dispatch({
        type: `${NAMESPACE}/create`,
        payload: {
          pmPlan: values, assets, callback: () => {
            this.handleCancel();
            callback();
          },
        },
      });
    });
  };

  handleSaveAndNew = form => {
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { callback, dispatch } = this.props;
      const { pmPlanInfo: { assets } } = this.state;
      dispatch({
        type: `${NAMESPACE}/create`,
        payload: {
          pmPlan: values, assets, callback: () => {
            this.handleClearData();
            callback();
          },
        },
      });
    });
  };

  handleUpdate = form => {
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { pmPlan: { id } } = this.props;
      const { callback, dispatch } = this.props;
      const { pmPlanInfo: { assets } } = this.state;
      dispatch({
        type: `${NAMESPACE}/update`,
        payload: {
          pmPlan: { id, ...values }, assets, callback: () => {
            this.handleCancel();
            callback();
          },
        },
      });
    });
  };

  handleFieldsChange = fields => {
    const { pmPlanInfo: prevPmPlanInfo } = this.state;
    const clonedPrevPMPlanInfo = cloneDeep(prevPmPlanInfo);
    const pmPlanInfo = { ...clonedPrevPMPlanInfo, ...fields };
    const prevClientId = get(prevPmPlanInfo, 'clientId.value');
    const clientId = get(pmPlanInfo, 'clientId.value');
    if (!isEqual(clientId, prevClientId)) {
      set(pmPlanInfo, 'locationId.value', undefined);
      set(pmPlanInfo, PMPLAN_FORM.assets, {});
      set(pmPlanInfo, `${PMPLAN_FORM.contractId}.value`, undefined);
    }
    const prevContractId = get(prevPmPlanInfo, `${PMPLAN_FORM.contractId}.value`);
    const contractId = get(pmPlanInfo, `${PMPLAN_FORM.contractId}.value`);
    if (!isEqual(prevContractId, contractId)) {
      set(pmPlanInfo, PMPLAN_FORM.assets, {});
      const { initData } = this.props;
      const serviceContracts = get(initData, 'serviceContracts', []);
      const matchedServiceContract = serviceContracts.find(({ id }) => id === contractId);
      if (matchedServiceContract) {
        const { locationId } = matchedServiceContract;
        const { startDate: rawStartDate } = matchedServiceContract;
        const { endDate: rawEndDate } = matchedServiceContract;
        const startDate = rawStartDate ? moment(parseInt(rawStartDate)) : undefined;
        const endDate = rawEndDate ? moment(parseInt(rawEndDate)) : undefined;
        set(pmPlanInfo, `${PMPLAN_FORM.locationId}.value`, locationId);
        set(pmPlanInfo, `${PMPLAN_FORM.startDate}.value`, startDate);
        set(pmPlanInfo, `${PMPLAN_FORM.endDate}.value`, endDate);
      }
    }
    const autoGen = get(pmPlanInfo, 'autoGen.value', false);
    if (!autoGen) {
      set(pmPlanInfo, 'genHorizon.value', undefined);
    }
    this.setState({ pmPlanInfo });
  };

  handleAssetsChange = (assets) => {
    this.setState(({ pmPlanInfo }) => {
      return {
        pmPlanInfo: {
          ...pmPlanInfo,
          assets,
        },
      };
    });
  }

  handleValuesChange = (values) => {
    const { clientId } = this.props;
    if (clientId) {
      return;
    }
    const [changedField] = Object.keys(values);
    if (changedField === PMPLAN_FORM.clientId && values[changedField]) {
      this.handleAssetsChange({});
      this.fetchDataByClient(values[changedField]);
    }
  }

  fetchDataByClient = clientId => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchDataByClient`,
      payload: { clientId },
    });
  }

  render() {
    const { pmPlanId, initData, isOpen, loading, clientId } = this.props;
    const { pmPlanInfo } = this.state;
    const isEdit = !!pmPlanId;
    const memoizedPMPlanInfo = this.pmPlanInfo;
    const disableUpdate = isEdit && isEqualWith(pmPlanInfo, memoizedPMPlanInfo, valueComparator);
    return (
      <PmPlanModalView
        isEdit={isEdit}
        pmPlan={pmPlanInfo}
        initData={initData}
        isOpen={isOpen}
        handleSave={this.handleSave}
        handleSaveAndNew={this.handleSaveAndNew}
        handleUpdate={this.handleUpdate}
        handleCancel={this.handleCancel}
        handleValuesChange={this.handleValuesChange}
        handleFieldsChange={this.handleFieldsChange}
        wrappedComponentRef={this.viewRef}
        loading={loading}
        onAssetsChange={this.handleAssetsChange}
        disableUpdate={disableUpdate}
        clientId={clientId}
      />
    );
  }
}

export default PmPlanModalContainer;
export { default as model } from './model';
export { NAMESPACE, PMPLAN_FORM } from './constants';
