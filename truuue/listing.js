import {CALL_API} from 'redux-api-middleware'
import {getCSRFTokenKey} from '../utils/TokenHelper'

import {TRUUUE_BASE_URL} from './'

window.CALL_API = CALL_API;

const GET_LISTINGS_REQUEST = 'GET_LISTINGS_REQUEST';
const GET_LISTINGS_SUCCESS = 'GET_LISTINGS_SUCCESS';
const GET_LISTINGS_FAILURE = 'GET_LISTINGS_FAILURE';

const GET_LISTING_DETAIL_REQUEST = 'GET_LISTING_DETAIL_REQUEST';
const GET_LISTING_DETAIL_SUCCESS = 'GET_LISTING_DETAIL_SUCCESS';
const GET_LISTING_DETAIL_FAILURE = 'GET_LISTING_DETAIL_FAILURE';

const GET_RECENT_TRANSACTION_REQUEST = 'GET_RECENT_TRANSACTION_REQUEST';
const GET_RECENT_TRANSACTION_SUCCESS = 'GET_RECENT_TRANSACTION_SUCCESS';
const GET_RECENT_TRANSACTION_FAILURE = 'GET_RECENT_TRANSACTION_FAILURE';

const GET_COMMENTARIES_LISTING_DETAIL_REQUEST = 'GET_COMMENTARIES_LISTING_DETAIL_REQUEST';
const GET_COMMENTARIES_LISTING_DETAIL_SUCCESS = 'GET_COMMENTARIES_LISTING_DETAIL_SUCCESS';
const GET_COMMENTARIES_LISTING_DETAIL_FAILURE = 'GET_COMMENTARIES_LISTING_DETAIL_FAILURE';

const CREATE_LISTING_REQUEST = 'CREATE_LISTING_REQUEST';
const CREATE_LISTING_SUCCESS = 'CREATE_LISTING_SUCCESS';
const CREATE_LISTING_FAILURE = 'CREATE_LISTING_FAILURE';

const GET_LISTING_EDIT_DETAIL_REQUEST = 'GET_LISTING_EDIT_DETAIL_REQUEST';
const GET_LISTING_EDIT_DETAIL_SUCCESS = 'GET_LISTING_EDIT_DETAIL_SUCCESS';
const GET_LISTING_EDIT_DETAIL_FAILURE = 'GET_LISTING_EDIT_DETAIL_FAILURE';

const PUT_LISTING_EDIT_DETAIL_REQUEST = 'PUT_LISTING_EDIT_DETAIL_REQUEST';
const PUT_LISTING_EDIT_DETAIL_SUCCESS = 'PUT_LISTING_EDIT_DETAIL_SUCCESS';
const PUT_LISTING_EDIT_DETAIL_FAILURE = 'PUT_LISTING_EDIT_DETAIL_FAILURE';

const PATCH_LISTING_EDIT_DETAIL_REQUEST = 'PATCH_LISTING_EDIT_DETAIL_REQUEST';
const PATCH_LISTING_EDIT_DETAIL_SUCCESS = 'PATCH_LISTING_EDIT_DETAIL_SUCCESS';
const PATCH_LISTING_EDIT_DETAIL_FAILURE = 'PATCH_LISTING_EDIT_DETAIL_FAILURE';

const GET_LISTING_EDIT_OPTION_REQUEST = 'GET_LISTING_EDIT_OPTION_REQUEST';
const GET_LISTING_EDIT_OPTION_SUCCESS = 'GET_LISTING_EDIT_OPTION_SUCCESS';
const GET_LISTING_EDIT_OPTION_FAILURE = 'GET_LISTING_EDIT_OPTION_FAILURE';

const GET_LISTING_ALL_IMAGES_REQUEST = 'GET_LISTING_ALL_IMAGES_REQUEST';
const GET_LISTING_ALL_IMAGES_SUCCESS = 'GET_LISTING_ALL_IMAGES_SUCCESS';
const GET_LISTING_ALL_IMAGES_FAILURE = 'GET_LISTING_ALL_IMAGES_FAILURE';

const UPDATE_LISTING_IMAGES_REQUEST = 'UPDATE_LISTING_IMAGES_REQUEST';
const UPDATE_LISTING_IMAGES_SUCCESS = 'UPDATE_LISTING_IMAGES_SUCCESS';
const UPDATE_LISTING_IMAGES_FAILURE = 'UPDATE_LISTING_IMAGES_FAILURE';

const CREATE_LISTING_IMAGES_REQUEST = 'CREATE_LISTING_IMAGES_REQUEST';
const CREATE_LISTING_IMAGES_SUCCESS = 'CREATE_LISTING_IMAGES_SUCCESS';
const CREATE_LISTING_IMAGES_FAILURE = 'CREATE_LISTING_IMAGES_FAILURE';

const TOGGLE_MULTIMEDIA = 'TOGGLE_MULTIMEDIA';
const UPDATE_WATCHLIST_LISTING = 'UPDATE_WATCHLIST_LISTING';

export const actionTypes = {
    GET_LISTINGS_REQUEST,
    GET_LISTINGS_SUCCESS,
    GET_LISTINGS_FAILURE,
    GET_LISTING_DETAIL_REQUEST,
    GET_LISTING_DETAIL_SUCCESS,
    GET_LISTING_DETAIL_FAILURE,
    GET_RECENT_TRANSACTION_REQUEST,
    GET_RECENT_TRANSACTION_SUCCESS,
    GET_RECENT_TRANSACTION_FAILURE,
    GET_COMMENTARIES_LISTING_DETAIL_REQUEST,
    GET_COMMENTARIES_LISTING_DETAIL_SUCCESS,
    GET_COMMENTARIES_LISTING_DETAIL_FAILURE,
    GET_LISTING_EDIT_DETAIL_REQUEST,
    GET_LISTING_EDIT_DETAIL_SUCCESS,
    GET_LISTING_EDIT_DETAIL_FAILURE,
    PUT_LISTING_EDIT_DETAIL_REQUEST,
    PUT_LISTING_EDIT_DETAIL_SUCCESS,
    PUT_LISTING_EDIT_DETAIL_FAILURE,
    PATCH_LISTING_EDIT_DETAIL_REQUEST,
    PATCH_LISTING_EDIT_DETAIL_SUCCESS,
    PATCH_LISTING_EDIT_DETAIL_FAILURE,
    CREATE_LISTING_REQUEST,
    CREATE_LISTING_SUCCESS,
    CREATE_LISTING_FAILURE,
    GET_LISTING_EDIT_OPTION_REQUEST,
    GET_LISTING_EDIT_OPTION_SUCCESS,
    GET_LISTING_EDIT_OPTION_FAILURE,
    GET_LISTING_ALL_IMAGES_REQUEST,
    GET_LISTING_ALL_IMAGES_SUCCESS,
    GET_LISTING_ALL_IMAGES_FAILURE,
    UPDATE_LISTING_IMAGES_REQUEST,
    UPDATE_LISTING_IMAGES_SUCCESS,
    UPDATE_LISTING_IMAGES_FAILURE,
    CREATE_LISTING_IMAGES_REQUEST,
    CREATE_LISTING_IMAGES_SUCCESS,
    CREATE_LISTING_IMAGES_FAILURE,
    TOGGLE_MULTIMEDIA,
    UPDATE_WATCHLIST_LISTING,
};

export const getListings = (page = 1, limit = 10) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/?limit=${limit}&offset=${(page - 1) * limit}`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                {
                    type: GET_LISTINGS_REQUEST,
                    payload: () => {
                        return {
                            page,
                            limit
                        }
                    }
                },
                GET_LISTINGS_SUCCESS,
                GET_LISTINGS_FAILURE,
            ]
        }
    })
};

export const getListingDetail = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            credentials: 'same-origin',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/listings/${listingHash}/`,
            headers: {'Content-Type': 'application/json'},
            types: [
                GET_LISTING_DETAIL_REQUEST,
                GET_LISTING_DETAIL_SUCCESS,
                GET_LISTING_DETAIL_FAILURE
            ]
        }
    })
};

export const getPastTransaction = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            credentials: 'same-origin',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/listings/${listingHash}/past-transactions/`,
            headers: {'Content-Type': 'application/json'},
            types: [
                GET_RECENT_TRANSACTION_REQUEST,
                GET_RECENT_TRANSACTION_SUCCESS,
                GET_RECENT_TRANSACTION_FAILURE
            ]
        }
    })
};

export const getCommentariesListingDetail = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            credentials: 'same-origin',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/listings/${listingHash}/commentaries/`,
            headers: {'Content-Type': 'application/json'},
            types: [
                GET_COMMENTARIES_LISTING_DETAIL_REQUEST,
                GET_COMMENTARIES_LISTING_DETAIL_SUCCESS,
                GET_COMMENTARIES_LISTING_DETAIL_FAILURE
            ]
        }
    })
};

export const createListing = (body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'POST',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/create/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                CREATE_LISTING_REQUEST,
                CREATE_LISTING_SUCCESS,
                CREATE_LISTING_FAILURE
            ]
        }
    })
};

export const removeListing = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'PATCH',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                listing_status: 2
            }),
            types: [
                PATCH_LISTING_EDIT_DETAIL_REQUEST,
                PATCH_LISTING_EDIT_DETAIL_SUCCESS,
                PATCH_LISTING_EDIT_DETAIL_FAILURE,
            ]
        }
    })
};

export const getListingUpdateDetail = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                GET_LISTING_EDIT_DETAIL_REQUEST,
                GET_LISTING_EDIT_DETAIL_SUCCESS,
                GET_LISTING_EDIT_DETAIL_FAILURE,
            ]
        }
    })
};

export const updateListingUpdateDetail = (listingHash, body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'PUT',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                PUT_LISTING_EDIT_DETAIL_REQUEST,
                PUT_LISTING_EDIT_DETAIL_SUCCESS,
                PUT_LISTING_EDIT_DETAIL_FAILURE,
            ]
        }
    })
};

export const patchListingUpdateDetail = (listingHash, body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'PATCH',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                PATCH_LISTING_EDIT_DETAIL_REQUEST,
                PATCH_LISTING_EDIT_DETAIL_SUCCESS,
                PATCH_LISTING_EDIT_DETAIL_FAILURE,
            ]
        }
    })
};

export const getListingUpdateOptions = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'OPTIONS',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                GET_LISTING_EDIT_OPTION_REQUEST,
                GET_LISTING_EDIT_OPTION_SUCCESS,
                GET_LISTING_EDIT_OPTION_FAILURE,
            ]
        }
    })
};

export const getAllListingImages = (listingHash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/images`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                GET_LISTING_ALL_IMAGES_REQUEST,
                GET_LISTING_ALL_IMAGES_SUCCESS,
                GET_LISTING_ALL_IMAGES_FAILURE,
            ]
        }
    })
};

export const createListingImage = (listingHash, body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'POST',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/images/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                CREATE_LISTING_IMAGES_REQUEST,
                CREATE_LISTING_IMAGES_SUCCESS,
                CREATE_LISTING_IMAGES_FAILURE,
            ]
        }
    })
};

export const updateListingImage = (listingHash, body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'PUT',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/listings/${listingHash}/edit/images/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                UPDATE_LISTING_IMAGES_REQUEST,
                UPDATE_LISTING_IMAGES_SUCCESS,
                UPDATE_LISTING_IMAGES_FAILURE,
            ]
        }
    })
};

export const toggleMultimedia = (type = '') => {
    return {
        type: TOGGLE_MULTIMEDIA,
        payload: {
            type: type
        }
    }
};

export const updateWatchlistInListing = (itemIdHash = undefined) => {
    return {
        type: UPDATE_WATCHLIST_LISTING,
        payload: {
            itemIdHash: itemIdHash
        }
    }
};

export const actionCreators = {
    getListingDetail,
    createListing,
    getListingUpdateDetail,
    updateListingUpdateDetail,
    patchListingUpdateDetail,
    getListingUpdateOptions,
    getAllListingImages,
    createListingImage,
    updateListingImage
};
