import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'ramda';

import { Row, Col } from '@/componentsPP/UIElements';
import { formatAddress } from '@/utils/utils';
import Ellipsis from '@/componentsPP/Ellipsis';
import IconButton from '@/componentsPP/IconButton';
import Avatar from '@/componentsPP/Avatar';
import { DEFAULT_AVATAR, CONFIRM_MODAL_TYPE } from '@/constants';
import deleteModal from '@/componentsPP/DeleteModal';
import { getAvatarURL } from '@/utils/image';

import Styles from './LocationItem.less';

const onEditHandler = (props) => (e) => {
  e.preventDefault();
  const { location, onEdit } = props;
  onEdit(location);
}

const onDeleteHandler = (props) => (e) => {
  e.preventDefault();
  const { location, onDelete } = props;
  const { id, name } = location;
  deleteModal({
    type: CONFIRM_MODAL_TYPE.delete,
    title: 'Delete Location',
    content: (
      <div>
        Are you sure you want to delete&nbsp;
        <span className={Styles.locationName}>{name}</span>
        ?
      </div>
    ),
    okText: 'Delete',
    onOk: () => onDelete(id),
  });
}

function renderContact(contactIds, contacts) {
  const [firstContact] = contactIds;
  const firstContactFullInfo = contacts.find(({ id }) => id === firstContact);
  const { id, firstName, lastName, phones } = firstContactFullInfo;
  const phoneValue = isEmpty(phones) ? null : phones[0].value;
  return (
    <div className={Styles.contacts}>
      <Avatar fallbackSrc={DEFAULT_AVATAR} size={60} src={getAvatarURL(id)} />
      <div className={Styles.contactInfo}>
        <div>{`${firstName} ${lastName}`}</div>
        <div>{phoneValue}</div>
      </div>
    </div>
  )
}

const LocationItem = (props) => {
  const { location, contacts } = props;
  const { name, address, contactIds = [] } = location;
  const formattedAddress = formatAddress(address);
  return (
    <div className={Styles.locationItemWrapper}>
      <Row gutter={16}>
        <Col md={9}>
          <div className={Styles.shortInfo}>
            <span className={Styles.name}>
              {name}
            </span>
            <span className={Styles.address}>
              <Ellipsis lines="1" tooltip={formattedAddress}>
                {formattedAddress}
              </Ellipsis>
            </span>
          </div>
        </Col>
        <Col md={9}>
          {!isEmpty(contactIds) && renderContact(contactIds, contacts)}
        </Col>
        <Col md={6} className={Styles.locationAction}>
          <div className={Styles.actionContainer}>
            <IconButton icon="edit" className={Styles.action} onClick={onEditHandler(props)} />
            <IconButton icon="delete" className={Styles.action} onClick={onDeleteHandler(props)} />
            <IconButton icon="more_horiz" className={Styles.action} />
          </div>
        </Col>
      </Row>
    </div>
  );
}

LocationItem.propTypes = {
  location: PropTypes.object,
}

LocationItem.defaultProps = {
  location: {},
}

export default LocationItem;
