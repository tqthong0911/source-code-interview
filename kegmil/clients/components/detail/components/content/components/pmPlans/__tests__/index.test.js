import React from 'react';
import { shallow } from 'enzyme';
import PMPlansContainer from '..';
import PMPlansView from '../view';

describe('<PMPlansContainer />', () => {
  let props;
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    props = {
      pmPlans: [],
      totalPage: 0,
      loading: false,
      id: 1,
      dispatch: jest.fn(),
      match: { params: { id: 'id' } },
    };
    wrapper = shallow(<PMPlansContainer.WrappedComponent {...props} />);
    wrappedInstance = wrapper.instance();

  });

  it('should render correctly', () => {
    expect(wrapper.find(PMPlansView).length).toBe(1);
    expect(props.dispatch).toBeCalled();
  });

  it('should call onRefreshPMPlans when it calls handleRefreshPMPlans', () => {
    wrappedInstance.handleRefreshPMPlans(1, {});
    
    expect(props.dispatch).toBeCalled();
  });
});
