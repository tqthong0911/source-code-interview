import { getWrapper } from '@/utils/test';
import PmPlanModal from '../index';
import PmPlanModalView from '../view';
import * as service from '../service';
import { NAMESPACE } from '../constants';

service.mapToFormData = jest.fn(value => value);

describe('<PmPlanModal />', () => {
  let props;
  let wrapper;
  let wrappedInstance;
  const options = [1, 2].map(value => ({
    id: `${value}`,
    name: `name${value}`,
  }));
  beforeEach(() => {
    props = {
      pmPlan: {},
      initData: {
        clients: options,
        jobType: options,
        status: options,
        locations: options,
        serviceContracts: options,
        assets: options,
      },
      dispatch: jest.fn(),
      loading: false,
      isOpen: false,
      handleCancel: jest.fn(callback => callback()),
    };
    wrapper = getWrapper(PmPlanModal.WrappedComponent, props);
    wrappedInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(PmPlanModalView).length).toBe(1);
  });

  it('should behave correctly when it call handleCancel', () => {
    wrappedInstance.handleCancel();

    expect(props.handleCancel).toBeCalled();
    expect(wrappedInstance.state.pmPlanInfo).toEqual({});
    expect(props.dispatch).toBeCalledWith({
      type: `${NAMESPACE}/clear`,
    });
  });

  it('should init data when it open', () => {
    wrapper.setProps({
      isOpen: true,
      pmPlanId: 'pm-01',
      clientId: 'client-01',
    });

    expect(props.dispatch).toBeCalledWith({
      type: `${NAMESPACE}/initData`,
      payload: { pmPlanId: 'pm-01', clientId: 'client-01' },
    });
  });

  it('should set data when data is updated', () => {
    wrapper.setProps({
      pmPlan: { key: 'value' },
    });

    expect(wrappedInstance.state.pmPlanInfo).toEqual({ key: 'value' });
  });

  describe('handleSave', () => {
    it('should do nothing when it call handleSave with error', () => {
      const form = {
        validateFieldsAndScroll: jest.fn(callback => callback(true)),
      };

      wrappedInstance.handleSave(form);

      expect(props.dispatch).not.toBeCalled();
    });

    it('should save when it call handleSave with no error', () => {
      const form = {
        validateFieldsAndScroll: jest.fn(callback => callback(false)),
      };

      wrappedInstance.handleSave(form);

      expect(props.dispatch).toBeCalled();
    });
  });
});
