import React from 'react';
import { shallow } from 'enzyme';
import PmPlanDetailContainer from '../index';
import PmPlanDetailView from '../view';
import { NAMESPACE } from '../constants';

describe('<PmPlanDetailContainer />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      pmPlan: {
        id: 1,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
      },
      initData: {
        statuses: [1, 2].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
      fetchPmPlanIndex: 0,
      changedIndex: 0,
      pmPlanUpdating: false,
      isCounting: false,
      quantity: {
        jobs: 0,
        assets: 0,
      },
      match: { params: { id: '1' } },
      loading: false,
      dispatch: jest.fn(),
    };
    wrapper = shallow(<PmPlanDetailContainer.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(PmPlanDetailView).length).toBe(1);
  });

  it('should render correctly when its call componentDidMount', () => {
    wrapperInstance.initData = jest.fn();
    wrapperInstance.componentDidMount();
    expect(wrapperInstance.initData).toHaveBeenCalled();
  });

  describe('componentDidUpdate', () => {
    it('should render correctly when props change changedIndex', () => {
      const prevProps = {
        fetchPmPlanIndex: 0,
        changedIndex: 1,
      }
      wrapperInstance.fetchPmPlanDetail = jest.fn();
      wrapperInstance.componentDidUpdate(prevProps);
      expect(wrapperInstance.fetchPmPlanDetail).toHaveBeenCalled();
    });
    it('should render correctly when props change fetchPmPlanIndex', () => {
      const prevProps = {
        fetchPmPlanIndex: 1,
        changedIndex: 0,
      }
      wrapperInstance.counting = jest.fn();
      wrapperInstance.componentDidUpdate(prevProps);
      expect(wrapperInstance.counting).toHaveBeenCalled();
    });
    it('should render correctly when props change location', () => {
      const prevProps = {
        fetchPmPlanIndex: 1,
        changedIndex: 0,
        location: {
          url: '/pmPlan',
        },
      }
      wrapperInstance.counting = jest.fn();
      wrapperInstance.componentDidUpdate(prevProps);
      expect(wrapperInstance.counting).toHaveBeenCalled();
    });
  })

  it('should call clear account info in componentWillUnmount', () => {
    wrapperInstance.componentWillUnmount();

    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/clear` });
  });

  it('should fetch pmPlan detail & init data when it call initData', () => {
    wrapperInstance.initData();
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetchInitData` });
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetchPmPlanDetail`, payload: { pmPlanId: '1' } });
  });

  it('should fetch counting when it call counting', () => {
    wrapperInstance.counting();
    expect(props.dispatch).toHaveBeenCalled();
  });

  it('should compare changedIndex when it call shouldRefetchPmPlanDetail', () => {
    const prevProps = {
      changedIndex: 1,
    }
    const result = wrapperInstance.shouldRefetchPmPlanDetail(prevProps);
    expect(result).toBe(true);
  });

  it('should fetch initData when it call fetchInitData', () => {
    wrapperInstance.fetchInitData();
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetchInitData` });
  });

  it('should fetch pmPlan detail when it call fetchPmPlanDetail', () => {
    wrapperInstance.fetchInitData();
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetchPmPlanDetail`, payload: { pmPlanId: '1' } });
  });

  it('should update pmPlan detail when it call handleDelete', () => {
    const closeModal = jest.fn();
    const isDeleteAllJobs = true;
    wrapperInstance.handleDelete('1', isDeleteAllJobs, closeModal);
    
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/delete`,
      payload: {
        pmPlanId: '1',
        isDeleteAllJobs,
        closeModal,
      },
    });
  });

  it('should update pmPlan detail when it call handleRefresh', () => {
    wrapperInstance.initData = jest.fn();
    wrapperInstance.handleRefresh();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/refreshPage`,
      payload: {
        callback: wrapperInstance.initData,
      },
    });
  });
});
