import React from 'react';
import { shallow } from 'enzyme';

import ContractContainer from '../index';
import ContractView from '../view';
import { NAMESPACE } from '../constants';

describe('<ContractContainer />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      contracts: [1, 2].map(value => ({
        id: `${value}`,
        name: `Contract ${value}`,
      })),
      totalPage: 2,
      totalItem: 20,
      dispatch: jest.fn(),
      loading: false,

    };
    wrapper = shallow(<ContractContainer.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(ContractView).length).toBe(1);
  });

  it('should fetch contracts list and init data in componentDidMount', () => {
    wrapperInstance.componentDidMount();
    const { sort, currentPage } = wrapperInstance;
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetch`, payload: { sort, currentPage } });
  });

  it('should clear contracts list in componentWillUnmount', () => {
    wrapperInstance.componentWillUnmount();

    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/clear` });
  });

  it('should fetch contracts when it calls fetchContracts', () => {
    wrapperInstance.fetchContracts();
    const { sort, currentPage } = wrapperInstance;
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetch`, payload: { sort, currentPage } });
  });

  it('should fetch contracts list by current page when it calls handleChangeFilterTableView', () => {
    const currentPage = 2;
    wrapperInstance.handleChangeFilterTableView({}, currentPage);

    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetch`,
      payload: {
        sort: {},
        currentPage,
      },
    });
  });

  it('should fetch contracts when it calls fetchContracts', () => {
    wrapperInstance.fetchContracts = jest.fn();
    wrapperInstance.fetchContracts();

    expect(wrapperInstance.fetchContracts).toHaveBeenCalled();
  });

  it('should fetch contracts when it calls refeshViewAfterDelete', () => {
    wrapperInstance.fetchContracts = jest.fn();
    wrapperInstance.refeshViewAfterDelete();

    expect(wrapperInstance.fetchContracts).toHaveBeenCalled();
  });

  it('should handle delete correctly when selected PM Plan is not empty', () => {
    const seletedItems = [{ id: '1' }];

    wrapperInstance.handleDelete(seletedItems);

    expect(props.dispatch).toBeCalled();
  });
});
