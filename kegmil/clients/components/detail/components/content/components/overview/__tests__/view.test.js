import React from 'react';
import { shallow } from 'enzyme';

import { Tabs } from '@/componentsPP/UIElements';

import DropdownMenu from '@/componentsPP/DropdownMenu';

import OverviewView from '../view';

describe('<Overview />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      client: {},
      contacts: [],
      locations: [],
      showHeader: false,
      onTabChange: jest.fn(),
    };
    wrapper = shallow(<OverviewView {...props} />)
  });
  it('should render correctly when it does not show header', () => {
    expect(wrapper.find(Tabs).length).toBe(1);
    expect(wrapper.find(DropdownMenu).length).toBe(0);
  });
  it('should render correctly when it shows header', () => {
    props = {
      ...props,
      showHeader: true,
    };
    wrapper = shallow(<OverviewView {...props} />);

    expect(wrapper.find(Tabs).length).toBe(1);
    expect(wrapper.find(DropdownMenu).length).toBe(1);
  });
});
