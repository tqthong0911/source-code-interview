export const NAMESPACE = 'pages.client';

export const MODAL_TYPE = Object.freeze({
  client: 'client',
  contact: 'contact',
  location: 'location',
  clientImport: 'clientImport',
  contactImport: 'contactImport',
});
