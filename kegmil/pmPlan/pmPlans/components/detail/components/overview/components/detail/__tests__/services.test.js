import { getFieldsToDisplay } from '../services';
import { EMPTY_VALUE } from '@/constants';

describe('Detail Overview in PmPlan Detail service', () => {
  it('should call a request when it calls deleteJob', () => {
    const data = {
      id: `1`,
      name: `1`,
      description: `1`,
      status: `1`,
      frequency: `1`,
      frequencyType: `1`,
      genTimeframe: `1`,
      genTimeframeType: `1`,
      jobType: `1`,
    };

    const initData = {
      client: {
        name: '1',
      },
      location: {
        name: '1',
      },
      statuses: [{
        id: `1`,
        name: `1`,
      }],
      jobTypes: [{
        id: `1`,
        name: `1`,
      }],
    }

    const result = getFieldsToDisplay(data, initData);

    expect(result.name).toBe(data.name);
    expect(result.clientName).toBe(initData.client.name);
    expect(result.jobTypeName).toBe(initData.jobTypes[0].name);
    expect(result.status).toBe(data.status);
    expect(result.locationName).toBe(initData.location.name);
    expect(result.description).toBe(data.description);
    expect(result.firstDate).toBe(EMPTY_VALUE);
    expect(result.endDate).toBe(EMPTY_VALUE);
    expect(result.frequencyDisplay).toBe(`${data.frequency} ${data.frequencyType}`);
    expect(result.genTimeframeDisplay).toBe(`${data.genTimeframe} ${data.genTimeframeType}`);
  });
});
