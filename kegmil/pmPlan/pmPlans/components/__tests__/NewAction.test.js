import React from 'react';
import { shallow } from 'enzyme';

import { Button } from '@/componentsPP/UIElements';

import NewActionContainer from '../NewAction';

describe('<NewActionContainer />', () => {
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    wrapper = shallow(<NewActionContainer />);
    wrappedInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(Button).length).toBe(1);
  });

  it('should handle open modal correctly', () => {
    wrappedInstance.handleNewClick();

    expect(wrappedInstance.state.isPMPlanOpen).toBeTruthy();
  });

  it('should handle cancel modal correctly', () => {
    const callback = jest.fn();

    wrappedInstance.handleCancel(callback);

    expect(wrappedInstance.state.isPMPlanOpen).toBeFalsy();
    expect(callback).toBeCalled();
  });
});
