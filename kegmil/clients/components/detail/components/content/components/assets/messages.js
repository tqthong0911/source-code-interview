import globalMessages from '@/messages';

export default {
  deleteAsset: globalMessages.asset.deleteAsset,
  assets: globalMessages.asset.assets,
  delete: globalMessages.common.delete,
  edit: globalMessages.common.edit,
  cancel: globalMessages.common.cancel,
  selected: globalMessages.common.selected,
  deleteContent: globalMessages.common.deleteContent,
  deleteMultiContent: globalMessages.common.deleteMultiContent,
  name: globalMessages.common.name,
  id: globalMessages.common.id,
  status: globalMessages.asset.status,
};
