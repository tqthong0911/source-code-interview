import React from 'react';
import { shallow } from 'enzyme';

import PageHeaderLayout from '@/layouts/PageHeaderLayout';
import StandardTable from '@/componentsPP/StandardTable';
import ClientView from '../view';

describe('<ClientView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      clients: [1, 2].map(value => ({ id: value, name: `Client ${value}` })),
      totalPage: 2,
      totalItem: 10,
      loading: false,
      handleChange: jest.fn(),
      handleExport: jest.fn(),
      handleEdit: jest.fn(),
      handleDelete: jest.fn(),
    };
    wrapper = shallow(<ClientView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(PageHeaderLayout).length).toBe(1);
    expect(wrapper.find(StandardTable).length).toBe(1);
  });

  it('should update selectedRows state when it calls handleSelectRow', () => {
    wrapperInstance.state.selectedRows = [];
    const selectedRows = [1, 2].map(value => ({ id: value, name: `Client ${value}` }));
    wrapperInstance.handleSelectRow(selectedRows);

    expect(wrapperInstance.state.selectedRows.length).toBe(2);
    expect(wrapperInstance.state.selectedRows[0].id).toBe(1);
    expect(wrapperInstance.state.selectedRows[1].id).toBe(2);
  });

  it('should update table when it calls handleSelectRow', () => {
    const pagination = {};
    const filters = {};
    const sorter = {};
    wrapperInstance.handleChange(pagination, filters, sorter);

    expect(props.handleChange).toHaveBeenCalled();
  });
});
