import React, { PureComponent } from 'react';
import propTypes from 'prop-types';
import { connect } from 'dva';
import { isEmpty, isEqual } from 'lodash';

import { NAMESPACE as CLIENT_NAMESPACE } from '@/componentsPP/_share/ClientModal';
import ClientView from './view';
import { SORT_ORDER } from '@/constants';
import { NAMESPACE } from './constants';

@connect(state => ({
  clients: state[NAMESPACE].clients,
  newClient: state[CLIENT_NAMESPACE].newClient,
  totalPage: state[NAMESPACE].totalPage,
  totalItem: state[NAMESPACE].totalItem,
  loading: state.loading.effects[`${NAMESPACE}/fetch`],
  isClientDeleted: !state.loading.effects[`${NAMESPACE}/delete`],
}))
class ClientContainer extends PureComponent {
  static propTypes = {
    clients: propTypes.array.isRequired,
    newClient: propTypes.object.isRequired,
    totalPage: propTypes.number.isRequired,
    totalItem: propTypes.number.isRequired,
    dispatch: propTypes.func.isRequired,
    loading: propTypes.bool,
    isClientDeleted: propTypes.bool,
  };

  static defaultProps = {
    loading: false,
    isClientDeleted: false,
  };

  filter = { sort :{ order: SORT_ORDER.ascend, field: 'name', columnKey: 'name' } };

  currentPage = 1;

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'breadcrumb/clear' });
    dispatch({ type: `${NAMESPACE}/fetch`, currentPage: this.currentPage, filter: this.filter });
  }

  componentDidUpdate(prevProps) {
    const { newClient, isClientDeleted } = this.props;
    if (!isEmpty(newClient) && !isEqual(prevProps.newClient, newClient)) {
      this.handleReloadPage();
    } else if (isClientDeleted && !isEqual(prevProps.isClientDeleted, isClientDeleted)) {
      this.handleReloadPage();
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  }

  handleReloadPage = () => {
    const { filter, currentPage } = this;
    this.handleChange(filter, currentPage);
  };

  handleChange = (filter, pageIndex) => {
    const { dispatch } = this.props;
    this.filter = filter;
    this.currentPage = pageIndex;

    dispatch({
      type: `${NAMESPACE}/fetch`,
      currentPage: pageIndex,
      filter: this.filter,
    });
  };

  refeshViewAfterDelete = () => {
    const { props: { clients }, currentPage } = this;
    const isLatestUserInPage = (clients.length === 1 && currentPage !== 1);
    this.currentPage = isLatestUserInPage ? (currentPage - 1) : currentPage;
  };

  handleDelete = (selectedClients, closeModal) => {
    if (selectedClients && selectedClients.length > 0) {
      const { dispatch } = this.props;
      const callback = () => {
        this.refeshViewAfterDelete();
        closeModal();
      }
      dispatch({
        type: `${NAMESPACE}/delete`,
        payload: { clientId: selectedClients[0].id, callback },
      });
    }
  };

  render() {
    const { clients, totalPage, totalItem, loading } = this.props; 

    return (
      <ClientView
        clients={clients}
        totalPage={totalPage}
        totalItem={totalItem}
        loading={loading}
        currentPage={this.currentPage}
        filter={this.filter}
        handleReloadPage={this.handleReloadPage}
        handleChange={this.handleChange}
        handleDelete={this.handleDelete}
      />
    );
  }
}

export default ClientContainer;
