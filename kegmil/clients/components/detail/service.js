import { get } from 'lodash';
import { fetchClientContacts as createFetchClientContacts } from '@/utils/contact';
import request from '@/utils/request';
import { PAGE_BIG_SIZE } from '@/constants';

export async function getClientDetail(clientId) {
  return request(`/client/${clientId}`);
}

export async function deleteClient(clientId) {
  return request(`/client/${clientId}`, {
    method: 'DELETE',
  });
}

const clientContactsFetcher = (parrams) => async(id) => {
  const fetcher = createFetchClientContacts(`/client/${id}/contact`)(parrams);
  const response = await fetcher();
  const clientContacts = get(response, 'results.contacts', []);
  return clientContacts;
}

export const fetchClientContacts = clientContactsFetcher({
  page: 1,
  size: PAGE_BIG_SIZE,
});
