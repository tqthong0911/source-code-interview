import React from 'react';
import { shallow } from 'enzyme';
import IconButton from '@/componentsPP/IconButton';
import LeftActionHeader from '../LeftActionHeader';

describe('<LeftActionHeader />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      pmPlan: {
        id: 1,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
      },
      initData: {
        statuses: [1, 2].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
    };
    wrapper = shallow(<LeftActionHeader {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(IconButton).length).toBe(3);
  });

  it('should return props change when it calls shouldComponentUpdate', () => {
    const nextProps = {
      pmPlan: {
        id: 1,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
      },
      initData: {
        statuses: [1, 2, 3].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
    }
    const result = wrapperInstance.shouldComponentUpdate(nextProps);

    expect(result).toBe(true);
  });
});
