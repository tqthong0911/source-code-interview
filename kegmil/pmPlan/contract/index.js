import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';

import ContractView from './view';
import { SORT_ORDER } from '@/constants';
import { NAMESPACE } from './constants';

@connect(state => ({
  contracts: state[NAMESPACE].contracts,
  totalPage: state[NAMESPACE].totalPage,
  totalItem: state[NAMESPACE].totalItem,
  loading: state.loading.effects[`${NAMESPACE}/fetch`],
}))
class ContractContainer extends PureComponent {
  static propTypes = {
    contracts: PropTypes.array.isRequired,
    totalPage: PropTypes.number.isRequired,
    totalItem: PropTypes.number.isRequired,
    dispatch: PropTypes.func.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
  };

  currentPage = 1;

  sort = { order: SORT_ORDER.ascend, field: 'name' };

  componentDidMount() {
    const { props: { dispatch }, sort, currentPage } = this;
    dispatch({
      type: `${NAMESPACE}/fetch`,
      payload: {
        sort,
        currentPage,
      },
    });
  };

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  };

  fetchContracts = () => {
    const { props: { dispatch }, currentPage, sort } = this;
    dispatch({
      type: `${NAMESPACE}/fetch`,
      payload: { currentPage, sort },
    });
  };

  handleChangeFilterTableView = (sorter, pageIndex) => {
    this.sort = sorter;
    this.currentPage = pageIndex;
    this.fetchContracts();
  };

  refeshViewAfterDelete = () => {
    const { props: { contracts }, currentPage } = this;
    const isLatestUserInPage = (contracts.length === 1 && currentPage !== 1);
    this.currentPage = isLatestUserInPage ? (currentPage - 1) : currentPage;
    this.fetchContracts();
  };

  handleDelete = (selectedContracts, closeModal) => {
    if (selectedContracts && selectedContracts.length > 0) {
      const { props: { dispatch }, refeshViewAfterDelete } = this;
      dispatch({
        type: `${NAMESPACE}/delete`,
        payload: {
          contractId: selectedContracts[0].id,
          clientId: selectedContracts[0].clientId,
          callback: () => {
            closeModal();
            refeshViewAfterDelete();
          },
        },
      });
    }
  };

  render() {
    const { contracts, totalPage, loading, totalItem } = this.props;

    return (
      <ContractView
        contracts={contracts}
        totalItem={totalItem}
        currentPage={this.currentPage}
        totalPage={totalPage}
        loading={loading}
        handleDelete={this.handleDelete}
        handleReloadContract={this.fetchContracts}
        handleChangeFilterTableView={this.handleChangeFilterTableView}
      />
    );
  }
}

export default ContractContainer;
