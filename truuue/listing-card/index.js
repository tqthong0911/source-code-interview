import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import Countdown from '../listing/countdown';
import currencyFormatter from 'currency-formatter';
import * as _ from 'lodash';
import {postWatchlistItem, removeWatchlistItem} from '../../actions/watchlist';

import './index.scss';

class ListingCard extends PureComponent {

    constructor(props) {
        super(props);
        this.layerTimeout = null;
        const itemIdHash = this.getItemIdHash();
        this.state = {
            watchlist: {
                watchlistError: '',
                flow: 0,
                itemIdHash: itemIdHash,
                status: itemIdHash ? 2 : 1
            },
            fadeOutTimer: 2000,
            mobileSize: 768,
            overtimeCondition: 900,
            isMobileView: false,
        }
    };

    getItemIdHash = (newProps) => {
        const props = newProps ? newProps : this.props;
        const {watchlistId, listing:{click_object_id}} = props;
        return watchlistId[click_object_id];
    };

    componentWillReceiveProps(nextProps) {
        const itemIdHash = this.getItemIdHash(nextProps);
        if (!_.isEqual(this.state.watchlist.itemIdHash, itemIdHash)) {
            const status = itemIdHash ? 2 : 1;
            this.setState({
                ...this.state,
                watchlist: {
                    ...this.state.watchlist,
                    itemIdHash: itemIdHash,
                    status: status
                }
            });
        }
    };

    windowResize = () => {
        const {mobileSize, isMobileView} = this.state;
        let criteria = window.innerWidth < mobileSize;
        if (isMobileView !== criteria) {
            this.setState({isMobileView: criteria})
        }
    };

    componentWillMount() {
        this.windowResize();
    }

    componentDidMount() {
        window.addEventListener("resize", this.windowResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.windowResize);
        window.clearTimeout(this.layerTimeout);
        this.layerTimeout = null;
    }

    getListingType = (click_object_type) => {
        const mapType = () => {
            const pieces = /([a-zA-Z]+)-([a-z]+)[-_]?/g.exec(click_object_type);
            return {
                prefix: pieces ? pieces[1] : '',
                type: pieces ? pieces[2] : ''
            };
        };
        const {prefix, type} = mapType();
        return {
            prefix: prefix,
            type: type === "listing" ? "sale" : type
        };
    };

    renderHeaderStatus = (auctionStatus) => {
        let status = '';
        switch (auctionStatus) {
            case 'unsold':
                status = 'Available For';
                break;
            case 'sold':
                status = 'END';
                break;
            case 'soon':
                status = 'SOON';
                break;
            case 'live':
                status = 'LIVE';
                break;
            case 'overtime':
                status = 'ENDING';
                break;
            default:
                status = '';
        }
        return (
            <div className="m-auto listing-card-status-text p-0 col">
                <span>{!['Ending', 'Available For'].includes(status) ? 'Biddin' : ''} <b>{status}</b></span>
            </div>
        );
    };

    renderCountDown = ({days, hours, minutes, seconds}) => {
        const content = days > 0 ? `${days}d ${hours}h` : (hours > 0 ? `${hours}h ${minutes}m` :
            `${minutes}m ${seconds}s`);
        return <span>{content}</span>
    };

    renderHeaderValue = (auctionStatus, {listing}) => {
        const {start_time, seconds_left} = listing.auction;
        switch (auctionStatus) {
            case 'unsold':
            case 'sold':
                return (
                    <div className="listing-card-status-content col-5 col-sm-4">
                        <div className="w-100 m-auto">{auctionStatus === 'sold' ? 'TAKEN' : 'OFFER'}</div>
                    </div>
                );
            case 'soon':
                const datetime = new Date(start_time);
                return (
                    <div className="listing-card-status-content col-5 col-sm-4">
                        <div className="w-100 m-auto">
                            <div className="datetime">
                                <div className="date">{datetime.getDate()}</div>
                                <div className="month">{moment(datetime).format('MMM')}</div>
                            </div>
                        </div>
                    </div>
                );
            case 'live':
                return (
                    <div className="listing-card-status-content col-5 col-sm-4">
                        <div className="w-100 m-auto">
                            <div className="listing-card-text-time-live">
                                <Countdown seconds={seconds_left} renderer={this.renderCountDown}/>
                            </div>
                        </div>
                    </div>
                );
            case 'overtime':
                return (
                    <div className="listing-card-status-content col-5 col-sm-4">
                        <div className="w-100 m-auto">
                            <Countdown seconds={seconds_left} renderer={this.renderCountDown}/>
                        </div>
                    </div>
                );
            default:
                return '';
        }
    };

    cardClassName = (auctionPriceState) => {
        switch (auctionPriceState) {
            case 'unsold':
            case 'sold':
                return 'biddin-end';
            case 'soon':
                return 'biddin-soon';
            case 'live':
                return 'biddin-live';
            case 'overtime':
                return 'biddin-overtime';
            default:
                return '';
        }
    };

    auctionHasClose = () => {
        const {auction: {status, end_time, extra_time}} = this.props.listing;
        if (status === 'C') {
            return true;
        } else {
            let endTime = new Date(end_time);
            endTime.setMinutes(endTime.getMinutes() + parseFloat(extra_time || 0));
            let now = new Date().getTime();
            return now - endTime.getTime() >= 0
        }
    };

    auctionIsStart = () => {
        const {auction: {start_time}} = this.props.listing;
        let isClose = this.auctionHasClose();
        if (!isClose) {
            let startTime = new Date(start_time).getTime();
            let now = new Date().getTime();
            return now >= startTime;
        }
        return false;
    };

    auctionComingSoon = () => {
        const {auction: {start_time}} = this.props.listing;
        let isClose = this.auctionHasClose();
        if (!isClose) {
            let startTime = new Date(start_time).getTime();
            let now = new Date().getTime();
            return now <= startTime;
        }
    };

    auctionIsOvertime = () => {
        const {auction: {seconds_left}} = this.props.listing;
        const isStart = this.auctionIsStart();
        if(isStart){
            return seconds_left <= this.state.overtimeCondition;
        }
        return false;
    };

    get auctionState() {
        return {
            listing: this.props.listing,
            isComingSoon: this.auctionComingSoon(),
            isOvertime: this.auctionIsOvertime(),
            isClose: this.auctionHasClose(),
            isStart: this.auctionIsStart()
        }
    };

    auctionStatus = ({isClose, isComingSoon, isStart, isOvertime, listing: {auction}}) => {
        if (isClose) {
            return auction.is_reserve_price_met ? 'sold' : 'unsold';
        }
        return isComingSoon ? 'soon' : (isStart ? (isOvertime ? 'overtime' : 'live') : null)
    };

    getPriceStatus = (auctionStatus) => {
        switch (auctionStatus) {
            case 'sold':
                return 'Transacted At: ';
            case 'unsold':
                return 'Make a Private Offer';
            case 'soon':
                return 'Starting Bid: ';
            case 'live':
                return 'Current Bid: ';
            default:
                return;
        }
    };

    auctionPrice = ({isClose, isComingSoon, isStart, listing}) => {
        const {is_reserve_price_met, standing_bid_amt, starting_price} = listing.auction;
        if (isClose) {
            return is_reserve_price_met ? standing_bid_amt : null;
        } else if (isComingSoon) {
            return starting_price;
        } else if (isStart) {
            return !standing_bid_amt ? starting_price : standing_bid_amt;
        }
        return null;
    };

    renderListingTitle = (title) => {
        let pieces = [title];
        if(title.indexOf('@') >= 0){
            pieces = title.split('@');
        }
        return (
            <div className="listing-card-footer-title row m-0">
                <div className="display-3dots">{`${pieces[0]} ${pieces.length > 1 ? '@' : ''}`}</div>
                <div className="display-3dots">{pieces[1]}</div>
            </div>
        );
    };

    toggleWatchlist = (e) => {
        const {flow, itemIdHash} = this.state.watchlist;

        if (flow === 0) {
            const {click_object_id} = this.props.listing;

            const resolveWatchlistError = (payload) => {
                if ([200, 201, 204].includes(payload.status)) {
                    return '';
                }

                switch (payload.status) {
                    case 400:
                        return "Bad Request";
                    case 401:
                        return "Unauthorized, please login to continue";
                    case 403:
                        return "You are not allowed!.";
                    case 404:
                        return "Not Found";
                    default:
                        return "Unknown error.";
                }
            };
            const _removeSuccess = (res) => (res.type === "REMOVE_WATCHLIST_ITEM_SUCCESS");
            const _addSuccess = (res) => (!_.isEmpty(res.payload) && res.type === "POST_WATCHLIST_ITEM_SUCCESS");

            if (itemIdHash) {
                this.props.removeWatchlistItem(itemIdHash).then((res) => {
                    const removed = _removeSuccess(res);
                    this.setState({
                        ...this.state,
                        watchlist: {
                            ...this.state.watchlist,
                            watchlistError: !removed ? resolveWatchlistError(res.payload) : '',
                            status: removed ? 1 : 0,
                            flow: 1,
                            itemIdHash: removed ? undefined : this.state.watchlist.itemIdHash
                        }
                    })
                });
            } else {
                this.props.postWatchlistItem({model: "listing", model_id_hash: click_object_id}).then((res) => {
                    const added = _addSuccess(res);
                    if (!added && res.payload.status === 401) {
                        window.location.href = '/signin';
                        return;
                    }
                    this.setState({
                        ...this.state,
                        watchlist: {
                            ...this.state.watchlist,
                            watchlistError: !added ? resolveWatchlistError(res.payload) : '',
                            status: added ? 2 : 0,
                            flow: 1,
                            itemIdHash: added ? res.payload.id_hash : this.state.watchlist.itemIdHash
                        }
                    })
                });
            }
        }
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
    };

    renderIconHeart = (type) => {
        const {itemIdHash, status, flow} = this.state.watchlist;
        const isShowLayer = status !== 0 && flow === 1;
        const fileName = `${isShowLayer ? `-white` : (type ? `-${type}` : '')}${itemIdHash ? '-filled' : '-outline'}`;

        return (
            <div className="listing-card-footer-icon">
                <img src={require(`./assets/heart${fileName}.png`)} onClick={(e) => {this.toggleWatchlist(e)}}
                     alt="icon-heart"/>
            </div>
        );
    };

    renderCardHeader = (auctionState, auctionStatus) => {
        return this.props.isShowHeader ? (
            <div className="listing-card-header card row m-0">
                {/*<div className="fire-dark">*/}
                    {/*<img src={require('./assets/fire.svg')} alt="fire"/>*/}
                {/*</div>*/}
                {this.renderHeaderStatus(auctionStatus)}
                {this.renderHeaderValue(auctionStatus, auctionState)}
            </div>
        ) : '';
    };

    renderFooterContent = (auctionPrice, auctionStatus) => {
        const currency = auctionPrice ? `S${currencyFormatter.format(auctionPrice, {code: 'SGD', precision: 0})}` : '';

        return (
            <div className="listing-card-footer-content">
                <div className="listing-card-footer-subtitle">
                    {this.getPriceStatus(auctionStatus)}
                </div>
                <div className="listing-card-footer-price">
                    {currency}
                </div>
            </div>
        )
    };

    renderCardFooter = (auctionPrice, auctionStatus, title, click_object_type) => {
        const {isMobileView} = this.state;
        const {type, prefix} = this.getListingType(click_object_type);

        return (
            <div className={`listing-card-footer ${type}`}>
                {!isMobileView ? <b className="card-type">{`${prefix} - ${type}`}</b> : null}
                {this.renderListingTitle(title)}
                {isMobileView ? <b className="card-type">{`${prefix} - ${type}`}</b> : null}
                <div className="card m-0">
                    {this.renderFooterContent(auctionPrice, auctionStatus)}
                    {this.renderIconHeart(type)}
                </div>
                <div className="listing-card-underline"/>
            </div>
        )
    };

    renderLayerCard = ({isComingSoon, isStart}) => {
        const {watchlist: {watchlistError, flow, status}, fadeOutTimer} = this.state;
        const textAdded = () => {
            return {
                "headerText": "Added to watchlist",
                "contentText": isComingSoon ? "You will be notified when biddin starts" :
                    "You will be notified 2 hours before biddin ends"
            }
        };
        const textRemoved = () => {
            return {
                "headerText": "Removed from watchlist",
                "contentText": (isComingSoon || isStart) ? "This listing is no longer being watched" : ""
            }
        };

        if(flow === 1){
            const result = watchlistError ? {'headerText': watchlistError} : (status === 2 ? textAdded() : textRemoved());
            if (!this.layerTimeout) {
                this.layerTimeout = setTimeout(() => {
                    window.clearTimeout(this.layerTimeout);
                    this.layerTimeout = null;
                    this.setState({
                        ...this.state,
                        watchlist: {
                            ...this.state.watchlist,
                            watchlistError: '',
                            flow: 0
                        }
                    });
                }, fadeOutTimer);
            }

            return (
                <div>
                    <div className={`overlay-effect ${watchlistError ? 'error' : ''}`}/>
                    <div className="text-effect">
                        <div className="listing-card-background-add-card">
                            <div className="listing-card-background-add-card-content">
                                <div
                                    className="listing-card-background-add-card-content-header">{result.headerText}</div>
                                <div
                                    className="listing-card-background-add-card-content-text">{result.contentText}</div>
                            </div>
                        </div>
                    </div>
                </div>);
        }
        return '';
    };

    render() {
        const { listing: {title, thumbnail_url, link, meta: {index}}} = this.props;
        const auctionState = this.auctionState;
        const auctionStatus = this.auctionStatus(auctionState);
        const auctionPrice = this.auctionPrice(auctionState);
        const cardClass = this.cardClassName(auctionStatus);

        return (
            <div className="col-12 col-ipX-6 col-md-6 col-lg-6 col-xl-4 my-3">
                <div className={`listing-card ${cardClass}`} style={{backgroundImage: `url(${thumbnail_url})`}}>
                    <a rel="noopener noreferrer" href={link}>
                        <div className="h-100">
                            {this.renderCardHeader(auctionState, auctionStatus)}
                            {this.renderCardFooter(auctionPrice, auctionStatus, title, index)}
                            {this.renderLayerCard(auctionState)}
                        </div>
                    </a>
                </div>
            </div>
        )
    }
}

ListingCard.defaultProps   = {
    isShowHeader: true
};

const mapStateToProps = (state) => {
    return {
        watchlistId: state.shortlist.watchlistId,
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        postWatchlistItem: (body) => dispatch(postWatchlistItem(body)),
        removeWatchlistItem: (id_hash) => dispatch(removeWatchlistItem(id_hash))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListingCard)
