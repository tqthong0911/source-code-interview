import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { isEqual } from 'lodash';
import StandardTable from '@/componentsPP/StandardTable';
import { renderTextColumn } from '@/utils/layout/renderTable';
import { getOriginalId, convertAssetsToDataView } from '@/utils/asset';
import { PP_FONT_SEMIBOLD, PP_TEXT_UPPERCASE } from '@/constants';
import messages from '../../messages';

class Open extends PureComponent {
  static propTypes = {
    assets: PropTypes.array.isRequired,
    selectedRows: PropTypes.array.isRequired,
    loadingAssets: PropTypes.bool.isRequired,
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    const { assets } = nextProps;
    const reUpdateDataView = !isEqual(assets, nextState.props.assets);

    if (reUpdateDataView) {
      return {
        props: {
          assets,
        },
        dataView: convertAssetsToDataView(assets, true),
      }
    }
    return null;
  }

  constructor(props) {
    super(props);
    const { assets } = this.props;
    this.state = {
      props: {
        assets,
      },
      sortedInfo: {},
      dataView: convertAssetsToDataView(assets, true),
    };
  }

  get columnConfig() {
    const { sortedInfo } = this.state;
    const renderHeader = renderTextColumn({ className: PP_TEXT_UPPERCASE });

    return [{
      title: renderHeader(formatMessage(messages.name)),
      dataIndex: 'name',
      key: 'name',
      width: '50%',
      className: PP_FONT_SEMIBOLD,
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
      render: text => text,
    }, {
      title: renderHeader(formatMessage(messages.id)),
      dataIndex: 'assetNumber',
      key: 'assetNumber',
      sorter: (a, b) => a.assetNumber.length - b.assetNumber.length,
      sortOrder: sortedInfo.columnKey === 'assetNumber' && sortedInfo.order,
      render: renderTextColumn,
    }, {
      title: renderHeader(formatMessage(messages.status)),
      dataIndex: 'status',
      key: 'status',
      sorter: (a, b) => a.status.length - b.status.length,
      sortOrder: sortedInfo.columnKey === 'status' && sortedInfo.order,
      render: renderTextColumn,
    }];
  };

  handleChange = (_pagination, _filters, sorter) => {
    this.setState({
      sortedInfo: sorter,
    });
  };

  handleSelectRow = selectedRows => {
    const { handleSelectRow } = this.props;
    const newSelectedRows = selectedRows.filter(row => row.id === getOriginalId(row.id));
    handleSelectRow(newSelectedRows);
  };

  render() {
    const { loadingAssets, selectedRows } = this.props;
    const { dataView } = this.state;

    return (
      <Fragment>
        <StandardTable
          loading={loadingAssets}
          data={{ list: dataView }}
          columns={this.columnConfig}
          onChange={this.handleChange}
          selectedRows={selectedRows}
          onSelectRow={this.handleSelectRow}
          rowKey={record => record.id}
          scroll={{  y: 'calc(100vh - 480px)' }}
        />
      </Fragment>
    );
  }
}

export default Open;
