import React from 'react';
import { shallow } from 'enzyme';

import { Modal } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';

import LeftActionView from '../LeftAction';
import NewAction from '../NewAction';

describe('<LeftActionView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      selectedContracts: [1, 2].map(value => ({ id: `${value}`, title: `Contract ${value}` })),
      handleDelete: jest.fn(),
      handleAssign: jest.fn(),
    };
    wrapper = shallow(<LeftActionView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly when length of selectedContracts is equal 0', () => {
    wrapper.setProps({ selectedContracts: [] });

    expect(wrapper.find(NewAction).length).toBe(1);
  });

  it('should render correctly when length of selectedContracts is equal 1', () => {
    wrapper.setProps({ selectedContracts: [{ id: '1', title: 'Contract 1' }] });

    expect(wrapper.find(IconButton).length).toBe(2);
  });

  it('should excuse a confirm modal when it calls handleDelete', () => {
    Modal.confirm = jest.fn();
    wrapperInstance.handleDelete();

    expect(Modal.confirm).toHaveBeenCalled();
  });

  it('should handle openEditModal correctly', () => {
    wrapperInstance.toggleEdit();

    expect(wrapperInstance.state.isEdit).toBeTruthy();
  });

  it('should handle cancelEditModal correctly', () => {
    const callback = jest.fn();

    wrapperInstance.handleCloseEditModal(callback);

    expect(wrapperInstance.state.isEdit).toBeFalsy();
    expect(callback).toBeCalled();
  });
});
