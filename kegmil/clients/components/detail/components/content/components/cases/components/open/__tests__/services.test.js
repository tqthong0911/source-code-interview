import { EMPTY_VALUE } from '@/constants';

import { convertCasesToDataView } from '../services';

describe('Tab Open Case In asset detail service', () => {
  it('should convert cases to data view when it calls convertCasesToDataView', async() => {
    const data = {
      id: 'id',
      caseNumber: 'caseNumber',
      subject: 'subject',
      type: 'type',
      priority: 'priority',
      status: 'status',
    }
    const result = await convertCasesToDataView(data);

    expect(result.id).toEqual(data.id);
    expect(result.caseNumber).toEqual(data.caseNumber);
    expect(result.subject).toEqual(data.subject);
    expect(result.type).toEqual(data.type);
    expect(result.priority).toEqual(data.priority);
    expect(result.status).toEqual(data.status);
    expect(result.dueDate).toEqual(EMPTY_VALUE);
  });
});
