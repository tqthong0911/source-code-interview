export const NAMESPACE = 'share.pmPlanModal';

export const PMPLAN_FORM = {
  id: 'id',
  name: 'name',
  clientId: 'clientId',
  contractId: 'contractId',
  jobType: 'jobType',
  status: 'status',
  startDate: 'startDate',
  endDate: 'endDate',
  locationId: 'locationId',
  description: 'description',
  frequency: 'frequency',
  frequencyType: 'frequencyType',
  genTimeframe: 'genTimeframe',
  genTimeframeType: 'genTimeframeType',
  firstDate: 'firstDate',
  autoGen: 'autoGen',
  assets: 'assets',
  genHorizon: 'genHorizon',
};

export const FREQUENCY_TYPE = ['Daily', 'Weekly', 'Monthly', 'Yearly'];
export const GEN_TIMEFRAME_TYPE = ['Days', 'Weeks', 'Months', 'Years'];

export const TASK_PAGE_SIZE = 5;
