import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';

import PageHeaderLayout from '@/layouts/PageHeaderLayout';
import IconButton from '@/componentsPP/IconButton';
import DropdownMenu from '@/componentsPP/DropdownMenu';

import LeftAction from './components/LeftAction';
import TableView from './components/TableView';
import messages from './messages';
import Styles from './styles.less';
import { PP_FONT_MEDIUM } from '@/constants';

class ContractView extends PureComponent {
  static propTypes = {
    contracts: PropTypes.array.isRequired,
    currentPage: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleReloadContract: PropTypes.func.isRequired,
    handleChangeFilterTableView: PropTypes.func.isRequired,
  };

  state = {
    selectedRows: [],
  };

  get renderAction() {
    const { handleReloadContract } = this.props;
    const { refresh } = messages;
    return (
      <div className={Styles.action}>
        <IconButton
          icon="refresh"
          onClick={handleReloadContract}
          tooltip={{ title: formatMessage(refresh), placement: 'topRight' }}
        />
        {/* <IconButton icon="vertical_align_top" onClick={() => { }} /> */}
      </div>
    );
  };

  get renderTitle() {
    const { totalItem } = this.props;
    const { contractAll } = messages;

    const viewListItems = [
      { name: formatMessage(contractAll), icon: '', key: 'contractAll' },
    ];
    return (
      <DropdownMenu items={viewListItems} onItemClick={() => { }}>
        <span className={PP_FONT_MEDIUM}>{`${formatMessage(contractAll)} (${totalItem})`}</span>
        <i className="material-icons">arrow_drop_down</i>
      </DropdownMenu>
    );
  };

  handleUpdateSelectedRows = selectedRows => {
    this.setState({ selectedRows });
  };

  renderLeftAction = () => {
    const { contracts, handleDelete, handleReloadContract } = this.props;
    const { selectedRows } = this.state;
    const selectedContracts = contracts.filter(contract => selectedRows.some(row => row.id === contract.id));
    return (
      <LeftAction
        selectedContracts={selectedContracts}
        handleDelete={handleDelete}
        handleReloadContract={handleReloadContract}
      />
    );
  };

  render() {
    const { contracts, handleChangeFilterTableView, totalPage, currentPage, loading } = this.props;
    const { selectedRows } = this.state;

    return (
      <PageHeaderLayout
        icon="menu-unfold"
        title={this.renderTitle}
        hideBreadcrumb
        action={this.renderAction}
        leftAction={this.renderLeftAction()}
        contentClassName={Styles.wrapper}
        iconContainerClassName={Styles.iconContainer}
      >
        <TableView
          contracts={contracts}
          currentPage={currentPage}
          loading={loading}
          totalPage={totalPage}
          selectedRows={selectedRows}
          handleUpdateSelectedRows={this.handleUpdateSelectedRows}
          handleChangeFilterTableView={handleChangeFilterTableView}
          className={Styles.tableWrapper}
        />
      </PageHeaderLayout>
    );
  }
}

export default ContractView;
