import { get } from 'lodash';
import { routerRedux } from 'dva/router'

import { PMPLAN_FORM } from '@/componentsPP/_share/PmPlanModal/constants';
import { updatePmPlan, deletePMPlan, getPmPlanDetail, getStatusPmPlan } from '@/utils/pmPlan';
import { getCommonLocations } from '@/utils/location';
import { getJobsByPmPlanId } from '@/utils/job/service';
import { NAMESPACE } from '../constants';

const defaultState = {
  pmPlan: {},
  quantity: {
    jobs: 0,
    assets: 0,
  },
  initData: {
    clients: [],
    jobType: [],
    statuses: [],
    commonLocations: [],
  },
  changedIndex: 0,
  fetchPmPlanIndex: 0,
  refreshPageIndex: 0,
};

const model = {
  namespace: NAMESPACE,

  state: defaultState,

  effects: {
    *fetchPmPlanDetail({ payload }, { call, put }) {
      const { pmPlanId } = payload;
      const response = yield call(getPmPlanDetail, pmPlanId);
      if (response) {
        const pmPlan = get(response, 'results.pmPlan', {});
        yield put({
          type: 'savePmPlanDetail',
          payload: { pmPlan },
        });
        yield put({ 
          type: 'breadcrumb/add',
          payload: { 
            item: { id: pmPlan.id, url: `/pm-plan/pm-plans/${pmPlan.id}`, name: pmPlan.name },
          },
        });
      }
    },
    *fetchInitData(_, { call, put, all }) {
      const [
        responseCommonLocations,
        responseStatuses,
      ] = yield all([
        call(getCommonLocations),
        call(getStatusPmPlan),
      ]);
      if (responseCommonLocations || responseStatuses) {
        yield put({
          type: 'saveInitData',
          payload: {
            commonLocations: get(responseCommonLocations, 'results.locations', []),
            statuses: get(responseStatuses, 'results.statuses', []),
          },
        })
      }
    },
    *counting({ payload: { pmPlan } }, { call, all, put }) {
      const { id: pmPlanId } = pmPlan;

      const [jobsResponse] = yield all([
        call(getJobsByPmPlanId, pmPlanId, 1, {}),
      ]);
      if (jobsResponse) {
        yield put({
          type: 'saveCounting',
          payload: {
            quantity: {
              jobs: get(jobsResponse, 'results.totalItem', 0),
              assets: get(pmPlan, PMPLAN_FORM.assets, []).length,
            },
          },
        });
      }
    },
    *updatePmPlan({ payload: { pmPlanInfo } }, { call, put }) {
      yield call(updatePmPlan, pmPlanInfo);
      yield put({ type: 'update' });
    },
    *delete({ payload: { pmPlanId, closeModal, isDeleteAllJobs } }, { call, put }) {
      const response = yield call(deletePMPlan, pmPlanId, isDeleteAllJobs);
      if (response) {
        yield put({ type: 'update' });
        yield put(routerRedux.push('/pm-plan/pm-plans'));
      }
      closeModal();
    },
    *refreshPage({ payload }, { put }) {
      const callback = get(payload, 'callback');
      yield put({ type: 'refresh' });
      if (typeof callback === 'function') {
        callback();
      }
    },
  },

  reducers: {
    saveInitData(state, { payload }) {
      const newInitData = Object.assign({}, state.initData, payload);
      return {
        ...state,
        initData: newInitData,
      };
    },
    savePmPlanDetail(state, { payload: { pmPlan } }) {
      return {
        ...state,
        pmPlan,
        fetchPmPlanIndex: state.fetchPmPlanIndex + 1,
      };
    },
    saveCounting(state, { payload: { quantity } }) {
      return {
        ...state,
        quantity,
      }
    },
    update(state) {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
      };
    },
    refresh(state) {
      return {
        ...state,
        refreshPageIndex: state.refreshPageIndex + 1,
      }
    },
    clear(state) {
      return {
        ...state,
        ...defaultState,
        changedIndex: state.changedIndex,
        fetchPmPlanIndex: state.fetchPmPlanIndex,
        refreshPageIndex: state.refreshPageIndex,
      };
    },
  },
};

export default model;
