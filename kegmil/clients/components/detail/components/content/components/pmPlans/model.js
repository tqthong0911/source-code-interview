import { isEmpty, isNil } from 'ramda';

import { NAMESPACE } from './constants';
import { fetchClientPMPlans } from './services';
import { fetchJobTypes } from '@/componentsPP/_share/JobModal';
import { deletePMPlan } from '@/utils/pmPlan';

const SORT_ORDER_MAP = Object.freeze({
  ascend: 'asc',
  descend: 'desc',
});

const initialState = {
  pmPlans: [],
  totalPage: 0,
  jobTypes: [],
}

export default {
  namespace: NAMESPACE,

  state: initialState,

  effects: {
    *fetch({ payload: { id, page, sorter, size } }, { call, put }) {
      const sort = isNil(sorter) || isEmpty(sorter) ? undefined : `${sorter.field}:${SORT_ORDER_MAP[sorter.order]}`;
      const { totalPage, pmPlans } = yield call(fetchClientPMPlans, id, { page, sort, size });
      yield put({
        type: 'save',
        payload: {
          totalPage,
          pmPlans,
        },
      });
    },
    *fetchJobTypes(_, { call, put }) {
      const jobTypes = yield call(fetchJobTypes);
      yield put({
        type: 'saveJobTypes',
        payload: {
          jobTypes,
        },
      });
    },
    *delete({ payload }, { call }) {
      const { id, shouldDeleteJob, callback } = payload;
      yield call(deletePMPlan, id, shouldDeleteJob);
      callback();
    },
  },

  reducers: {
    save: (state, { payload: { pmPlans, totalPage } }) => ({
      ...state,
      totalPage,
      pmPlans,
    }),
    saveJobTypes: (state, { payload: { jobTypes } }) => ({
      ...state,
      jobTypes,
    }),
    clear: () => initialState,
  },
}
