import React, { Fragment } from 'react';

import { Card } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';

import { ACTIONS } from './constants';
import ContactItem from './ContactItem';
import Styles from './styles.less';
import ContactModal from '@/componentsPP/_share/ContactModal';

const ContactsView = (props) => {
  const {
    contacts,
    selectedContact,
    onNew,
    onEdit,
    onCloseNew,
    onCloseEdit,
    modalType,
    clientId,
    onCreateOrUpdateSuccess,
    onDelete,
  } = props;
  return (
    <Fragment>
      <ContactModal
        isOpen={modalType === ACTIONS.new}
        handleCancel={onCloseNew}
        isClientDisabled
        clientId={clientId}
        onSuccess={onCreateOrUpdateSuccess}
      />
      <ContactModal
        isOpen={modalType === ACTIONS.edit}
        contact={selectedContact}
        handleCancel={onCloseEdit}
        isClientDisabled
        clientId={clientId}
        onSuccess={onCreateOrUpdateSuccess}
      />
      <Card
        title="Client Contacts"
        className={Styles.wrapper}
        extra={<IconButton className={Styles.addButton} icon="add" onClick={onNew} />}
      >
        {contacts.map((contact) => (
          <ContactItem contact={contact} key={contact.id} onEdit={onEdit} onDelete={onDelete} />
        ))}
      </Card>
    </Fragment>
  );
}

export default ContactsView;
