import React from 'react';
import { shallow } from 'enzyme';

import ImportModal from '@/componentsPP/_share/ImportModal';
import ContractModal from '@/componentsPP/_share/ContractModal';

import NewActionContainer from '../NewAction';

describe('<NewActionContainer />', () => {
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    wrapper = shallow(<NewActionContainer />);
    wrappedInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(ContractModal).length).toBe(1);
    expect(wrapper.find(ImportModal).length).toBe(1);

  });

  it('should handle open modal correctly', () => {
    wrappedInstance.toggleNew();

    expect(wrappedInstance.state.isOpen).toBeTruthy();
  });

  it('should handle cancel modal correctly', () => {
    const callback = jest.fn();

    wrappedInstance.handleCancel(callback);

    expect(wrappedInstance.state.isOpen).toBeFalsy();
    expect(callback).toBeCalled();
  });
});
