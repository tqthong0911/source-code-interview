import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';

import { NAMESPACE, ACTIONS } from './constants';
import LocationsView from './view';

@connect()
class Locations extends PureComponent {
  static propTypes = {
    clientId: PropTypes.string,
    contacts: PropTypes.array,
    locations: PropTypes.array,
    fetchLocations: PropTypes.func,
  }

  static defaultProps = {
    clientId: '',
    contacts: [],
    locations: [],
    fetchLocations: () => {},
  }

  state = {
    modalType: '',
    selectedLocation: {},
  }

  handleDelete = (id) => {
    const { dispatch, fetchLocations } = this.props;
    dispatch({
      type: `${NAMESPACE}/delete`,
      payload: { id, callback: fetchLocations },
    })
  }

  handleNew = () => this.setState({ modalType: ACTIONS.new });

  handleCloseNew = () => this.setState({ modalType: '' });

  handleEdit = (location) => this.setState({
    selectedLocation: location,
    modalType: ACTIONS.edit,
  });

  handleCloseEdit = () => this.setState({
    selectedLocation: {},
    modalType: '',
  });

  handleCreateOrUpdateSuccess = () => {
    const { fetchLocations } = this.props;
    fetchLocations();
  }

  render() {
    const { contacts, locations, clientId } = this.props;
    const { selectedLocation, modalType } = this.state;
    return (
      <LocationsView
        clientId={clientId}
        contacts={contacts}
        locations={locations}
        selectedLocation={selectedLocation}
        modalType={modalType}
        onNew={this.handleNew}
        onCloseNew={this.handleCloseNew}
        onEdit={this.handleEdit}
        onCloseEdit={this.handleCloseEdit}
        onDelete={this.handleDelete}
        onCreateOrUpdateSuccess={this.handleCreateOrUpdateSuccess}
      />
    )
  }
}

export default Locations;
export { default as model } from './model';
