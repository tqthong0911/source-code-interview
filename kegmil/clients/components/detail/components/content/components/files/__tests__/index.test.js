import React from 'react';
import { shallow } from 'enzyme';

import { Tabs } from '@/componentsPP/UIElements';

import Files from '..';

describe('<ClientDetailView />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {};
    wrapper = shallow(<Files {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Tabs).length).toBe(1);
  });
});
