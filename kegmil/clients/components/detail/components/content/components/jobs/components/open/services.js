import moment from 'moment';
import { get } from 'lodash';
import { EMPTY_VALUE, DATE_TIME_FORMAT } from '@/constants';

export const convertJobsToDataView = (data, jobsType = []) => {
  const mapping = job => {
    const {
      id,
      jobNumber,
      jobType,
      status,
      statusObj,
      priority,
    } = job;
    return {
      id,
      jobNumber: jobNumber || EMPTY_VALUE,
      jobType: getNameJobType(jobsType, jobType),
      status: status || EMPTY_VALUE,
      statusObj: statusObj || {},
      startTime: getJobTime(job),
      endTime: getJobTime(job, 'end'),
      priority: priority || EMPTY_VALUE,
    };
  };

  if (data.constructor === Array) {
    const dataView = [];
    for (let i = 0; i < data.length; i += 1) {
      dataView[i] = mapping(data[i]);
    }
    return dataView;
  }
  return mapping(data);
};

export function getJobTime(job, type = 'start') {
  const { startScheduleTime, endScheduleTime } = job;
  const isJobScheduled = startScheduleTime && endScheduleTime;

  const time = isJobScheduled ? job[`${type}ScheduleTime`] : job[`${type}Time`];
  return time ? moment(parseInt(time, 0)).format(DATE_TIME_FORMAT) : EMPTY_VALUE;
}

function getNameJobType(jobsType, id) {
  const currentJobType = jobsType.find(jobType => jobType.id === id);
  return get(currentJobType, 'name', EMPTY_VALUE);
}
