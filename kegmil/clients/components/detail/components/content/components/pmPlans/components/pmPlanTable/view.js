import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { compose, isNil } from 'ramda';
import Link from 'umi/link';
import StandardTable from '@/componentsPP/StandardTable';
import { Tag } from '@/componentsPP/UIElements';
import Ellipsis from '@/componentsPP/Ellipsis';
import { PAGE_SIZE, SORT_ORDER, EMPTY_VALUE, DATE_TIME_FORMAT, PP_FONT_SEMIBOLD } from '@/constants';
import messages from './messages';
import Styles from './styles.less';

function renderColumn(value = EMPTY_VALUE) {
  return (
    <Ellipsis lines="1" tooltip={value}>
      {value}
    </Ellipsis>
  );
}

function renderLinkColumn(value, record) {
  if (isNil(value)) {
    return renderColumn();
  }
  return (
    <Link
      to={`/pm-plan/pm-plans/${record.id}`}
      className={PP_FONT_SEMIBOLD}
    >
      {renderColumn(value)}
    </Link>
  );
}

function parseDateTimeValue(value) {
  const columnValue = isNil(value) ? EMPTY_VALUE : moment(parseInt(value, 10)).format(DATE_TIME_FORMAT);
  return columnValue;
}

const typeColumnRenderer = (props) => (value) => {
  const { jobTypes } = props;
  const jobTypeInfo = jobTypes.find(({ id }) => id === value);
  if (isNil(jobTypeInfo)) {
    return (
      <Tag className={Styles.tag}>{renderColumn()}</Tag>
    );
  }
  const { name } = jobTypeInfo;
  return (
    <Tag className={Styles.tag}>{renderColumn(name)}</Tag>
  );
}

function getColumnConfig(props) {
  const dateTimeColumnRenderer = compose(renderColumn, parseDateTimeValue);
  return [
    {
      title: formatMessage(messages.id),
      dataIndex: 'planId',
      key: 'planId',
      width: '15%',
      sorter: true,
      render: renderLinkColumn,
      defaultSortOrder: SORT_ORDER.ascend,
    },
    {
      title: formatMessage(messages.name),
      dataIndex: 'name',
      key: 'name',
      width: '25%',
      sorter: true,
      render: renderColumn,
    },
    {
      title: formatMessage(messages.jobType),
      dataIndex: 'jobType',
      key: 'jobType',
      width: '20%',
      sorter: true,
      render: typeColumnRenderer(props),
    },
    {
      title: formatMessage(messages.startDate),
      dataIndex: 'startDate',
      key: 'startDate',
      width: '20%',
      sorter: true,
      render: dateTimeColumnRenderer,
    },
    {
      title: formatMessage(messages.endDate),
      dataIndex: 'endDate',
      key: 'endDate',
      width: '20%',
      sorter: true,
      render: dateTimeColumnRenderer,
    },
  ];
}

function paginationConfig(props) {
  const { totalPage, page, onPageChange } = props;
  const allItems = totalPage * PAGE_SIZE;

  return {
    simple: true,
    total: allItems,
    current: page,
    pageSize: PAGE_SIZE,
    onChange: onPageChange,
  };
}

const PMPlanTableView = (props) => {
  const { pmPlans, loading, onChange, selectedRows, onSelectRow } = props;
  return (
    <StandardTable
      loading={loading}
      columns={getColumnConfig(props)}
      onChange={onChange}
      selectedRows={selectedRows}
      onSelectRow={onSelectRow}
      data={{
        list: pmPlans,
        pagination: paginationConfig(props),
      }}
      rowKey={({ id }) => id}
      className={Styles.pmPlanTable}
    />
  );
}

PMPlanTableView.propTypes = {
  pmPlans: PropTypes.array,
  jobTypes: PropTypes.array, // eslint-disable-line
  loading: PropTypes.bool,
  selectedRows: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  onSelectRow: PropTypes.func.isRequired,
}

PMPlanTableView.defaultProps = {
  pmPlans: [],
  jobTypes: [],
  loading: false,
  selectedRows: [],
}

export default PMPlanTableView;
