import React from 'react';
import { shallow } from 'enzyme';

import { PAGE_SIZE } from '@/constants';
import StandardTable from '@/componentsPP/StandardTable';

import OpenJob from '../index';

describe('<OpenJob />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      loadingJobs: false,
      totalItem: 2,
      totalPage: 1,
      jobTypes: [1, 2].map(e => ({ id: e, name: e })),
      accessedStatuses: [1, 2].map(e => ({ id: e, name: e })),
      selectedRows: [1, 2].map(e => ({ id: e, name: e })),
      jobs: [1, 2].map(e => ({ id: e, name: e })),
      handleSelectRow: jest.fn(),
      handleSortPage: jest.fn(),
      fetchData: jest.fn(),
      updateJob: jest.fn(),
      dispatch: jest.fn(),
    };
    wrapper = shallow(<OpenJob {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(StandardTable).length).toBe(1);
  });

  it('paginationConfig', () => {
    const result = wrapperInstance.paginationConfig;
    expect(result.total).toEqual(props.totalPage * PAGE_SIZE);
  });

  it('columnConfig', () => {
    const result = wrapperInstance.columnConfig;
    expect(result.length).toBe(6);
  });

  it('handlePagination', () => {
    wrapperInstance.state.currentPage = 1;
    wrapperInstance.handlePagination(2);
    expect(wrapperInstance.state.currentPage).toBe(2);
  });

  it('handleChange', () => {
    const pagination = { current: 1 }
    const sorter = { columnKey: '1', order: '1' }
    wrapperInstance.handleChange(pagination, {}, sorter);
    expect(props.handleSortPage).toHaveBeenCalled();
  });

});
