import globalMessages from '@/messages';

export default {
  id: globalMessages.common.id,
  name: globalMessages.common.name,
  jobType: globalMessages.pmPlan.jobType,
  startDate: globalMessages.pmPlan.startDate,
  endDate: globalMessages.pmPlan.endDate,
};
