import React, { PureComponent } from 'react';
import ContractTableView from './view';

class ContractTableContainer extends PureComponent {
  state = {
    sorter: {},
    page: 1,
  }

  handleChange = (_pagination, _filter, sorter) => this.setState({ sorter }, this.refreshContracts);

  handlePageChange = page => this.setState({ page }, this.refreshContracts);

  refreshContracts = () => {
    const { page, sorter } = this.state;
    const { onRefreshContracts } = this.props;
    onRefreshContracts(page, sorter);
  }

  render() {
    const { sorter } = this.state;
    const { contracts, loading, totalPage, selectedRows, onSelectRow } = this.props;
    return (
      <ContractTableView
        loading={loading}
        sorter={sorter}
        selectedRows={selectedRows}
        onChange={this.handleChange}
        onPageChange={this.handlePageChange}
        contracts={contracts}
        totalPage={totalPage}
        onSelectRow={onSelectRow}
      />
    );
  }
}

export default ContractTableContainer;
