import request from '@/utils/request';

export async function deleteClientContact(id) {
  return request(`/client/contact/${id}`, { method: 'DELETE' });
}
