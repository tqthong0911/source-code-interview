import React from 'react';
import PropTypes from 'prop-types';
import { Card, Collapse } from '@/componentsPP/UIElements';
import BasicInfo from './BasicInfo';
import Styles from './styles.less';

const { Panel } = Collapse;

const Details = ({ client }) => {
  const { description } = client;
  return (
    <Card title="Client Details" className={Styles.wrapper}>
      <Collapse bordered={false} defaultActiveKey="description" className={Styles.description}>
        <Panel header="Description" bordered={false} style={{ border: '0' }}>
          <p>{description}</p>
        </Panel>
      </Collapse>
      <BasicInfo client={client} />
    </Card>
  );
}

Details.propTypes = {
  client: PropTypes.object,
}

Details.defaultProps = {
  client: {},
}

export default Details;
