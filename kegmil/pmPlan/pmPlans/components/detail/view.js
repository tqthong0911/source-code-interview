import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { get } from 'lodash';
import { Checkbox, Row, Col, Spin, Menu } from '@/componentsPP/UIElements';
import deleteModal from '@/componentsPP/DeleteModal';
import PmPlanModal, { PMPLAN_FORM } from '@/componentsPP/_share/PmPlanModal';
import withBreadcrumb from '@/utils/breadcrumb';
import { CONFIRM_MODAL_TYPE } from '@/constants';
import LeftActionHeader from './components/LeftActionHeader';
import Overview from './components/overview';
import Assets from './components/assets';
import Jobs from './components/jobs';
import messages from './messages';
import Styles from './styles.less';

const { Item } = Menu;
const root = { id: 'pmPlan', url: '/pm-plan/pm-plans', name: 'PM Plans' };
const PageHeaderLayout = withBreadcrumb(root);

const SIDE_BAR = Object.freeze({
  overview: {
    key: 'overview',
    name: formatMessage(messages.overview),
    renderComponent: (props) => (<Overview {...props} />),
  },
  assets: {
    key: 'assets',
    name: formatMessage(messages.assets),
    renderComponent: (props) => {
      const assetIds = get(props, 'pmPlan.assets', []).map(asset => asset.id);
      return (<Assets {...props} assetIds={assetIds} />);
    },
  },
  jobs: {
    key: 'jobs',
    name: formatMessage(messages.jobs),
    renderComponent: (props) => (<Jobs {...props} />),
  },
});

export class PmPlanDetailView extends PureComponent {
  static propTypes = {
    pmPlan: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    isCounting: PropTypes.bool.isRequired,
    pmPlanUpdating: PropTypes.bool.isRequired,
    handleDelete: PropTypes.func.isRequired,
    updatePmPlanDetail: PropTypes.func.isRequired,
    handleRefresh: PropTypes.func.isRequired,
    pmPlanId: PropTypes.string.isRequired,
    initData: PropTypes.object.isRequired,
    quantity: PropTypes.object.isRequired,
  };

  state = {
    isDeleteAllJobs: true,
    isOpenEditModal: false,
    menuCurrent: [SIDE_BAR.overview.key],
  };

  handleCloseEditModal = () => this.setState({ isOpenEditModal: false });

  handleRefresh = () => {
    const { handleRefresh } = this.props;
    handleRefresh();
  };

  handleCheckbox = (e) => this.setState({ isDeleteAllJobs: e.target.checked });

  handleDelete = () => {
    const { pmPlan, handleDelete } = this.props;
    const { isDeleteAllJobs: prevIsDeleteAllJobs } = this.state;

    deleteModal({
      type: CONFIRM_MODAL_TYPE.delete,
      title: formatMessage(messages.deletePMPlan),
      content: (
        <Fragment>
          <div>
            {formatMessage(messages.deleteContent)}
            <span className={Styles.highlight}>{` ${pmPlan.name}`}</span>
            ?
          </div>
          <div className={Styles.deleteGeneratedJobs}>
            <Checkbox onChange={this.handleCheckbox} defaultChecked={prevIsDeleteAllJobs}>
              {formatMessage(messages.deleteJobCreatedByPmPlan)}
            </Checkbox>
          </div>
        </Fragment>
      ),
      okText: formatMessage(messages.delete),
      cancelText: formatMessage(messages.cancel),
      onOk: (closeModal) => {
        const { isDeleteAllJobs } = this.state;
        return new Promise(() => handleDelete(pmPlan.id, isDeleteAllJobs, closeModal)).catch(() => { });
      },
      onCancel: () => { },
    });
  };

  handleEdit = () => this.setState({ isOpenEditModal: true });

  handleChangeStatus = ({ key }) => {
    const { updatePmPlanDetail } = this.props;
    updatePmPlanDetail({ [PMPLAN_FORM.status]: key });
  };

  handleChangeAutoGen = (value) => {
    const { updatePmPlanDetail } = this.props;
    updatePmPlanDetail({ [PMPLAN_FORM.autoGen]: value });
  };

  handleChangeAsset = (value) => {
    const { updatePmPlanDetail } = this.props;
    updatePmPlanDetail({ [PMPLAN_FORM.assets]: value });
  };

  renderCouting = (nameDisplay) => {
    const { quantity, isCounting } = this.props;
    const { assets: assetsQuantity, jobs: jobsQuantity } = quantity;
    switch (nameDisplay) {
    case SIDE_BAR.assets.name:
      return this.renderTabView(isCounting, nameDisplay, assetsQuantity);
    case SIDE_BAR.jobs.name:
      return this.renderTabView(isCounting, nameDisplay, jobsQuantity);
    default:
      return (<div className={Styles.textAlignLeft}>{nameDisplay}</div>);
    }
  }

  renderTabView = (loading, name, quanlity = 0) => (
    <Row>
      <Col span={18} className={Styles.textAlignLeft}>{name}</Col>
      <Col span={6} className={Styles.textAlignRight}>
        {loading ? <Spin spinning={loading} /> : quanlity}
      </Col>
    </Row>
  );

  onSelect = (value) => this.setState({ menuCurrent: [value.key] });

  renderLeftContent = () => {
    const { menuCurrent } = this.state;
    return (
      <div className={Styles.menuWrapper}>
        <Menu
          mode='inline'
          onSelect={this.onSelect}
          defaultSelectedKeys={[SIDE_BAR.overview.key]}
          selectedKeys={menuCurrent}
        >
          {Object.keys(SIDE_BAR).map((key) => (<Item key={key}>{this.renderCouting(SIDE_BAR[key].name)}</Item>))}
        </Menu>
      </div>
    );
  }

  render() {
    const { pmPlan, pmPlanId, initData, loading, pmPlanUpdating } = this.props;
    const { isOpenEditModal, menuCurrent } = this.state;
    const leftActionHeader = (
      <LeftActionHeader
        pmPlan={pmPlan}
        initData={initData}
        loading={loading}
        pmPlanUpdating={pmPlanUpdating}
        handleRefresh={this.handleRefresh}
        handleEdit={this.handleEdit}
        handleDelete={this.handleDelete}
        handleChangeStatus={this.handleChangeStatus}
      />
    );

    return (
      <PageHeaderLayout
        actionClassName={Styles.action}
        pageHeaderWrapper={Styles.pageHeaderWrapper}
        wrapperClassName={Styles.wrapperClassName}
        contentClassName={Styles.contentClassName}
        hideBreadcrumb
        action={leftActionHeader}
      >
        <div className={Styles.contentDetail}>
          <div className={Styles.leftContent}>
            {this.renderLeftContent()}
          </div>
          <div className={Styles.rightContent}>
            {SIDE_BAR[menuCurrent].renderComponent({
              pmPlan,
              initData,
              actions: {
                handleChangeAutoGen: this.handleChangeAutoGen,
                handleChangeAsset: this.handleChangeAsset,
              },
              loading: pmPlanUpdating,
            })}
          </div>
        </div>
        <PmPlanModal
          isOpen={isOpenEditModal}
          pmPlanId={pmPlanId}
          handleCancel={this.handleCloseEditModal}
          callback={this.handleRefresh}
        />
      </PageHeaderLayout>
    );
  }
}

export default PmPlanDetailView;
