import React from 'react';
import * as umiLocale from 'umi/locale';
import { shallow } from 'enzyme';
import StandardTable from '@/componentsPP/StandardTable';
import OpenCase from '../';
import { PAGE_SIZE } from '@/constants';

umiLocale.formatMessage = jest.fn(() => 'message');

describe('<OpenCase />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      loadingCases: false,
      cases: [1, 2, 3].map(e => ({
        id: e,
        name: `name-${e}`,
      })),
      totalItem: 3,
      totalPage: 1,
      selectedRows: [1, 2, 3].map(e => ({
        id: e,
        name: `name-${e}`,
      })),
      handleSortPage: jest.fn(),
      handleSelectRow: jest.fn(),
      fetchData: jest.fn(),
      updateCase: jest.fn(),
      priorityDS: [
        { value: 'Low', name: 'Low' },
        { value: 'Medium', name: 'Medium' },
        { value: 'High', name: 'High' },
      ],
    };
    wrapper = shallow(<OpenCase {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(StandardTable).length).toBe(1);
  });

  it('should return pagination config when it calls paginationConfig', () => {
    const result = wrapperInstance.paginationConfig;

    expect(result.total).toBe((props.totalPage * PAGE_SIZE));
    expect(result.current).toBe(1);
    expect(result.pageSize).toBe(PAGE_SIZE);
  });

  it('should return column config when it calls columnConfig', () => {
    const result = wrapperInstance.columnConfig;

    expect(result.length).toBe(6);
  });

  it('should handle change in table view when it calls handleChange', () => {
    const pagination = {
      current: 2,
    };
    const sorter = {
      columnKey: 'caseNumber',
      order: "abc",
    }

    wrapperInstance.handleChange(pagination, undefined, sorter);
    expect(props.handleSortPage).toHaveBeenCalled();
  });
});
