import moment from 'moment';
import { EMPTY_VALUE, DATE_TIME_FORMAT } from '@/constants';

export const convertCasesToDataView = (data) => {
  const mapping = item => {
    const {
      id,
      caseNumber,
      subject,
      type,
      priority,
      status,
      dueDate,
    } = item;
    return {
      id,
      caseNumber: caseNumber || EMPTY_VALUE,
      subject: subject || EMPTY_VALUE,
      type: type || EMPTY_VALUE,
      priority: priority || EMPTY_VALUE,
      status: status || EMPTY_VALUE,
      dueDate: dueDate ? moment(parseInt(dueDate, 10)).format(DATE_TIME_FORMAT) : EMPTY_VALUE,
    };
  };

  if (data.constructor === Array) {
    const dataView = [];
    for (let i = 0; i < data.length; i += 1) {
      dataView[i] = mapping(data[i]);
    }
    return dataView;
  }
  return mapping(data);
};
