import React from 'react';
import { shallow } from 'enzyme';
import StandardTable from '@/componentsPP/StandardTable';
import AssetPaneView from '../';

describe('<AssetPaneView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      assets: [],
      selectedRows: [],
      loadingAssets: false,
    };
    wrapper = shallow(<AssetPaneView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(StandardTable).length).toBe(1);
  });
  
  it('should return column config when it calls columnConfig', () => {
    const result = wrapperInstance.columnConfig;
    expect(result.length).toBe(3);
  });
});
