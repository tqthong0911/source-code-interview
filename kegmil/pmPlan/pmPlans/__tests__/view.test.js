import React from 'react';
import { shallow } from 'enzyme';

import PageHeaderLayout from '@/layouts/PageHeaderLayout';

import LeftAction from '../components/LeftAction';
import TableView from '../components/TableView';
import PMPlansView from '../view';

describe('<PMPlansView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      JobsType: [1, 2].map(value => ({
        id: `${value}`,
        name: `Job ${value}`,
      })),
      pmPlans: [1, 2].map(value => ({
        id: `${value}`,
        title: `Job ${value}`,
      })),
      totalPage: 2,
      totalItem: 10,
      loading: false,
      handleDelete: jest.fn(),
      handleReloadPMPlan: jest.fn(),
      handleChangeFilterTableView: jest.fn(),
    };
    wrapper = shallow(<PMPlansView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(PageHeaderLayout).length).toBe(1);
    expect(wrapper.find(TableView).length).toBe(1);
  });

  it('should update selectedRows state when it calls handleUpdateSelectedRows', () => {
    wrapperInstance.state.selectedRows = [];
    const selectedRows = [{ id: '1' }];
    wrapperInstance.handleUpdateSelectedRows(selectedRows);

    expect(wrapperInstance.state.selectedRows).toBe(selectedRows);
  });

  it('should render correctly when it calls renderLeftAction', () => {
    const LeftActionComp = wrapperInstance.renderLeftAction;
    const leftActionWrapper = shallow(<LeftActionComp />);

    expect(leftActionWrapper.find(LeftAction).length).toBe(1);
  });

  it('should render correctly when it calls handleReloadPMPlan', () => {
    wrapperInstance.handleReloadPMPlan();

    expect(wrapperInstance.state.selectedRows).toEqual([]);
  });
});
