import React from 'react';
import { shallow } from 'enzyme';

import ClientModal from '@/componentsPP/_share/ClientModal';
// import IconButton from '@/componentsPP/IconButton';

import { LeftAction } from '../LeftAction';
import NewAction from '../NewAction';

describe('<LeftAction />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      selectedClients: [1, 2].map(value => ({ id: `${value}`, title: `Client ${value}` })),
      handleDelete: jest.fn(),
      handleExport: jest.fn(),
      permission: {
        clientsContacts: {
          viewClients: true,
          updateClients: true,
          exportClients: true,
          deleteClients: true,
        },
      },
    };
    wrapper = shallow(<LeftAction {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly when length of selectedClients is equal 0', () => {
    wrapper.setProps({ selectedClients: [] });

    expect(wrapper.find(NewAction).length).toBe(1);
  });

  it('should render correctly when length of selectedClients is greater than 1', () => {
    // expect(wrapper.find(IconButton).length).toBe(2);
    expect(wrapper.find(ClientModal).length).toBe(1);
  });

  it('should render correctly when length of selectedClients is equal 1', () => {
    wrapper.setProps({ selectedClients: [{ id: '1', title: 'Client 1' }] });

    // expect(wrapper.find(IconButton).length).toBe(4);
    expect(wrapper.find(ClientModal).length).toBe(1);
  });

  it('should update isEdit state to true when it calls handleOpenEditModal', () => {
    wrapperInstance.state.isEdit = false;
    wrapperInstance.handleOpenEditModal();

    expect(wrapperInstance.state.isEdit).toBeTruthy();
  });

  it('should update isEdit state to false when it calls handleCloseEditModal', () => {
    wrapperInstance.state.isEdit = true;
    wrapperInstance.handleCloseEditModal();

    expect(wrapperInstance.state.isEdit).toBeFalsy();
  });
});
