import { getWrapper } from '@/utils/test';
import { Row } from '@/componentsPP/UIElements';
import BasicInfo from '../BasicInfo';

describe('<BasicInfo />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      client: {},
    };
    wrapper = getWrapper(BasicInfo, props);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Row).length).toBe(3);
  })
});
