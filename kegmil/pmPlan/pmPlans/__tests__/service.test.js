import { EMPTY_VALUE, PAGE_SIZE } from '@/constants';
import { getPMPlans, mapPMPlansToView } from '../service';

let mockRequest;
jest.mock('@/utils/request', () => {
  mockRequest = jest.fn();
  return mockRequest;
});

jest.mock('@/utils/utils', () => ({
  formatAddress: jest.fn().mockReturnValue('Valid Address'),
}));

describe('Job service', () => {
  describe('getJob', () => {
    it('should call a request by default page when it calls getPMPlans', async() => {
      await getPMPlans();
      expect(mockRequest).toHaveBeenCalledWith(`/job/contract/pmplans?page=1&size=${PAGE_SIZE}`);
    });

    it('should call a request by current page when it calls getPMPlans', async() => {
      await getPMPlans({}, 2);
      expect(mockRequest).toHaveBeenCalledWith(`/job/contract/pmplans?page=2&size=${PAGE_SIZE}`);
    });
  });

  it('should convert data when it calls mapPMPlansToView', () => {
    const data = [1, 2].map(e => ({
      id: `${e}`,
      planId: `${e}`,
      name: `${e}`,
      jobType: `${e}`,
      clientName: `${e}`,
    }));

    const result = mapPMPlansToView(data);

    expect(result.length).toBe(2);

    expect(result.length).toBe(2);
    expect(result[0].id).toBe(data[0].id);
    expect(result[0].planId).toBe(data[0].planId);
    expect(result[0].name).toBe(data[0].name);
    expect(result[0].jobType).toBe(data[0].jobType);
    expect(result[0].clientName).toBe(data[0].clientName);
    expect(result[0].endDate).toBe(EMPTY_VALUE);
    expect(result[0].startDate).toBe(EMPTY_VALUE);

    expect(result[1].id).toBe(data[1].id);
    expect(result[1].planId).toBe(data[1].planId);
    expect(result[1].name).toBe(data[1].name);
    expect(result[1].jobType).toBe(data[1].jobType);
    expect(result[1].clientName).toBe(data[1].clientName);
    expect(result[1].endDate).toBe(EMPTY_VALUE);
    expect(result[1].startDate).toBe(EMPTY_VALUE);
  });
});
