import { EMPTY_VALUE, PAGE_SIZE, SORT_ORDER } from '@/constants';
import { getContract, deleteContract, mapContractToView } from '../service';
import { buildFilterParam } from '@/utils/job';

let mockRequest;
jest.mock('@/utils/request', () => {
  mockRequest = jest.fn();
  return mockRequest;
});

describe('Job service', () => {
  it('should call a request by filter and page when it calls getContract', async() => {
    const filter = { sort: { order: SORT_ORDER.ascend, field: 'name' } };
    const currentPage = 1;
    const filterQueryString = buildFilterParam(filter);
    await getContract(filter, currentPage);
    expect(mockRequest).toHaveBeenCalledWith(
      `/client/contracts/query?page=${currentPage}&size=${PAGE_SIZE}${filterQueryString}`
    );
  });

  it('should call a request when it calls deleteContract', async() => {
    const clientId = 1;
    const contractId = 1;
    await deleteContract(contractId, clientId);

    expect(mockRequest).toHaveBeenCalledWith(`/client/${clientId}/contracts/${contractId}`, {
      method: 'DELETE',
    });
  });

  it('should map data to view when it calls mapContractToView', () => {
    const data = [1].map(e => ({
      type: `${e}`,
      id: `${e}`,
      name: `${e}`,
      terms: `${e}`,
      leadTimes: `${e}`,
      client: { name: `${e}` },
      clientId: `${e}`,
    }));

    const result = mapContractToView(data);

    expect(result.length).toBe(1);

    expect(result.length).toBe(1);

    expect(result[0].type).toBe(data[0].type);
    expect(result[0].id).toBe(data[0].id);
    expect(result[0].name).toBe(data[0].name);
    expect(result[0].terms).toBe(data[0].terms);
    expect(result[0].endDate).toBe(EMPTY_VALUE);
    expect(result[0].startDate).toBe(EMPTY_VALUE);
    expect(result[0].leadTimes).toBe(data[0].leadTimes);
    expect(result[0].client).toBe(data[0].client.name);
    expect(result[0].clientId).toBe(data[0].clientId);
  });
});
