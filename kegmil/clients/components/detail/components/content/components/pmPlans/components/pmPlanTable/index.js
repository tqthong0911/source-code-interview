import React, { PureComponent } from 'react';
import PMPlanTableView from './view';

class PMPlanTableContainer extends PureComponent {
  state = {
    sorter: {},
    page: 1,
  }

  handleChange = (_pagination, _filter, sorter) => this.setState({ sorter }, this.refreshPMPlans);

  handlePageChange = page => this.setState({ page }, this.refreshPMPlans);

  refreshPMPlans = () => {
    const { page, sorter } = this.state;
    const { onRefreshPMPlans } = this.props;
    onRefreshPMPlans(page, sorter);
  }

  render() {
    const { sorter } = this.state;
    const { pmPlans, loading, totalPage, jobTypes, selectedRows, onSelectRow } = this.props;
    return (
      <PMPlanTableView
        loading={loading}
        sorter={sorter}
        selectedRows={selectedRows}
        onChange={this.handleChange}
        onPageChange={this.handlePageChange}
        pmPlans={pmPlans}
        jobTypes={jobTypes}
        totalPage={totalPage}
        onSelectRow={onSelectRow}
      />
    );
  }
}

export default PMPlanTableContainer;
