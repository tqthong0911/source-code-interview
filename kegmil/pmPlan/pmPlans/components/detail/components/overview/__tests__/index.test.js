import React from 'react';
import { shallow } from 'enzyme';
import { Tabs } from '@/componentsPP/UIElements';

import Overview from '../index';

describe('<Overview />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      pmPlan: {
        id: 1,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
        locationId: '1',
        clientId: '1',
        loading: false,
      },
      initData: {
        statuses: [1, 2].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
      actions: {
        handleChangeAutoGen: jest.fn(),
      },
      dispatch: jest.fn(),
    };
    wrapper = shallow(<Overview.WrappedComponent {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Tabs).length).toBe(1);
  });
});
