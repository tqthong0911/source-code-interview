import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import Ellipsis from "@/componentsPP/Ellipsis";
import { formatMessage } from 'umi/locale';
import { isEmpty, isEqual } from 'lodash';
import StandardTable from '@/componentsPP/StandardTable';
import { Select } from '@/componentsPP/UIElements';
import { renderTextColumn, renderOptionText } from '@/utils/layout/renderTable';
import { PAGE_SIZE, PP_FONT_SEMIBOLD, PP_FONT_MEDIUM, PP_TEXT_UPPERCASE } from '@/constants';
import { JOB_PRIORITY } from '@/utils/job/constants';
import { convertCasesToDataView } from './services';
import messages from '../../messages';
import Styles from './styles.less';

class OpenCase extends PureComponent {
  static propType = {
    loadingCases: PropTypes.bool.isRequired,
    cases: PropTypes.array.isRequired,
    totalItem: PropTypes.number.isRequired,
    totalPage: PropTypes.number.isRequired,
    selectedRows: PropTypes.array.isRequired,
    handleSortPage: PropTypes.func.isRequired,
    handleSelectRow: PropTypes.func.isRequired,
    fetchData: PropTypes.func.isRequired,
    updateCase: PropTypes.func.isRequired,
    priorityDS: PropTypes.array,
  };

  static defaultProps = {
    priorityDS: [
      { value: 'Low', name: 'Low' },
      { value: 'Medium', name: 'Medium' },
      { value: 'High', name: 'High' },
    ],
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    const { cases } = nextProps;

    const shouldUpdateCases = !isEqual(cases, nextState.props.cases);

    if (shouldUpdateCases) {
      return {
        props: {
          cases,
        },
        dataView: convertCasesToDataView(cases),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    const { cases } = this.props;
    this.state = {
      props: {
        cases,
      },
      dataView: convertCasesToDataView(cases),
      currentPage: 1,
    };
  }

  get paginationConfig() {
    const { totalPage } = this.props;
    const { currentPage } = this.state;
    const allItems = totalPage * PAGE_SIZE;

    return {
      simple: true,
      total: allItems,
      current: currentPage,
      pageSize: PAGE_SIZE,
    };
  }

  get columnConfig() {
    return [{
      title: renderOptionText(formatMessage(messages.caseNumber), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'caseNumber',
      key: 'caseNumber',
      render: (text, record) => (
        <Link to={`/cases/${record.id}`}>
          <Ellipsis lines="1" tooltip={text}>{text}</Ellipsis>
        </Link>
      ),
      // sorter: true,
      className: PP_FONT_SEMIBOLD,
    }, {
      title: renderOptionText(formatMessage(messages.caseName), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'subject',
      key: 'subject',
      render: renderTextColumn,
      // sorter: true,
    }, {
      title: renderOptionText(formatMessage(messages.type), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'type',
      key: 'type',
      render: renderTextColumn,
    }, {
      title: renderOptionText(formatMessage(messages.priority), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'priority',
      key: 'priority',
      render: this.renderPriorityColumn,
      // sorter: true,
    }, {
      title: renderOptionText(formatMessage(messages.status), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'status',
      key: 'status',
      render: this.renderStatusColumn,
      // sorter: true,
    }, {
      title: renderOptionText(formatMessage(messages.dueDate), { className: PP_TEXT_UPPERCASE }),
      dataIndex: 'dueDate',
      key: 'dueDate',
      render: renderTextColumn,
      // sorter: true,
    }];
  };

  renderStatusColumn = (text, { id }) => {
    const { statusDS, updateCase } = this.props;
    return (
      <Select
        value={text}
        className={Styles.selectNoneBorder}
        dropdownClassName={Styles.dropDownMinWidth}
        onChange={value => updateCase(id, { 'status': value })}
        placeholder={formatMessage(messages.selectPlaceholder)}
      >
        {statusDS.map(item => (
          <Select.Option key={item.value} value={item.value}>
            { item.name }
          </Select.Option>
        ))}
      </Select>
    );
  };

  renderPriorityColumn = (text, { id }) => {
    const { priorityDS, updateCase } = this.props;
    const getStyle = value => ({ color: value === JOB_PRIORITY.high ? '#F5222D' : '#FAAD14' });

    return (
      <Select
        value={text}
        className={Styles.selectNoneBorder}
        onChange={value => updateCase(id, { 'priority': value })}
        dropdownClassName={Styles.dropDownMinWidth}
      >
        {priorityDS.map(item => (
          <Select.Option key={item.value} value={item.value}>
            <div className={Styles.priority}>
              <span className={PP_FONT_MEDIUM}>{item.name}</span>
              {item.name !== JOB_PRIORITY.low &&
                <i className={`material-icons ${Styles.priorityIcon}`} style={getStyle(item.name)}>report_problem</i>}
            </div>
          </Select.Option>
        ))}
      </Select>
    );
  };

  handleChange = (pagination, _, sorter) => {
    let sortParam = {};

    if (!isEmpty(sorter)) {
      const { columnKey, order } = sorter;
      sortParam = { field: columnKey, order };
    }

    const { handleSortPage } = this.props;
    if (handleSortPage) {
      this.setState({ currentPage: pagination.current }, () => {
        handleSortPage(pagination.current, { sort: sortParam });
      });
    }
  };

  render() {
    const { selectedRows, loadingCases, handleSelectRow } = this.props;
    const { dataView } = this.state;

    return (
      <StandardTable
        loading={loadingCases}
        selectedRows={selectedRows}
        data={{
          list: dataView,
          pagination: this.paginationConfig,
        }}
        columns={this.columnConfig}
        onChange={this.handleChange}
        onSelectRow={handleSelectRow}
        rowKey={record => record.id}
      />
    );
  }
}

export default OpenCase;
