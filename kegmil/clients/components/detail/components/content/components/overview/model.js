import { NAMESPACE } from './constants';
import fetchClientContacts from '@/componentsPP/_share/JobModal/services/fetchClientContacts';
import { fetchClientLocations } from '@/componentsPP/_share/JobModal';

const initialState = {
  contacts: [],
  locations: [],
}

export default {
  namespace: NAMESPACE,

  state: initialState,

  effects: {
    *fetchContacts({ payload: { clientId } }, { call, put }) {
      const contacts = yield call(fetchClientContacts, clientId);

      yield put({
        type: 'save',
        payload: { contacts },
      });
    },
    *fetchLocations({ payload: { clientId } }, { call, put }) {
      const locations = yield call(fetchClientLocations, clientId);

      yield put({
        type: 'save',
        payload: { locations },
      });
    },
  },

  reducers: {
    save: (state, { payload }) => ({
      ...state,
      ...payload,
    }),
    clear: () => initialState,
  },
}
