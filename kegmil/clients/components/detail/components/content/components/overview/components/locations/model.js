import { NAMESPACE } from './constants';
import { deleteLocation } from '@/pages/client/locations';

const initialState = {};

export default {
  namespace: NAMESPACE,

  state: initialState,

  effects: {
    *delete({ payload: { id, callback } }, { call }) {
      const response = yield call(deleteLocation, id);

      if (response) {
        callback();
      }
    },
  },

  reducers: {
    clear: () => initialState,
  },
}
