import { get } from 'lodash';
import { getClientLocations, getCommonLocations } from '@/utils/location';
import { getStatusPmPlan } from '@/utils/pmPlan';
import {
  create,
  update,
  getDetail,
  getClients,
  getJobType,
  getAssetByClient,
  fetchContracts,
} from './service';
import { NAMESPACE } from './constants';

const initData = {
  clients: [],
  jobType: [],
  status: [],
  locations: [],
  commonLocations: [],
  serviceContracts: [],
  assets: [],
};

const model = {
  namespace: NAMESPACE,

  state: {
    initData,
    pmPlan: {},
  },

  effects: {
    *initData({ payload }, { all, call, put }) {
      const { pmPlanId, clientId: injectedClientId } = payload;
      let pmPlan = {};
      if (pmPlanId) {
        const response = yield call(getDetail, pmPlanId);
        pmPlan = get(response, 'results.pmPlan', {});
      }
      const [
        clientsResponse,
        jobTypeResponse,
        statusReponse,
        commonLocationsResponse,
      ] = yield all([
        call(getClients),
        call(getJobType),
        call(getStatusPmPlan),
        call(getCommonLocations),
      ]);
      const clientId = pmPlan.clientId || injectedClientId || get(clientsResponse, 'results.clients[0].id');
      if (clientId) {
        yield put({
          type: 'fetchDataByClient',
          payload: { clientId },
        });
      }
      yield put({
        type: 'saveInitData',
        payload: {
          pmPlan,
          clients: get(clientsResponse, 'results.clients', []),
          jobType: get(jobTypeResponse, 'results.jobType', []),
          status: get(statusReponse, 'results.statuses', []),
          commonLocations: get(commonLocationsResponse, 'results.locations', []),
        },
      });
    },
    *create({ payload }, { call }) {
      const { pmPlan, callback, assets } = payload;
      const pmPlanDetail = { ...pmPlan, assets };
      const response = yield call(create, pmPlanDetail);
      if (response) {
        if (callback) callback();
      }
    },
    *update({ payload }, { call }) {
      const { pmPlan, callback, assets } = payload;
      const pmPlanDetail = { ...pmPlan, assets };
      const response = yield call(update, pmPlanDetail);
      if (response) {
        if (callback) callback();
      }
    },
    *fetchDataByClient({ payload: { clientId } }, { all, call, put }) {
      const [locationsResponse, assetsResponse, serviceContracts ] = yield all([
        call(getClientLocations, clientId),
        call(getAssetByClient, clientId),
        call(fetchContracts, clientId),
      ]);
      yield put({
        type: 'saveInitDataByClient',
        payload: {
          locations: get(locationsResponse, 'results.locations', []),
          assets: get(assetsResponse, 'results.assets', []),
          serviceContracts,
        },
      });
    },
  },

  reducers: {
    saveInitData(state, { payload }) {
      const newInitData = Object.assign({}, state.initData, payload);
      const { pmPlan } = payload;
      return {
        ...state,
        initData: newInitData,
        pmPlan,
      };
    },
    saveInitDataByClient(state, { payload }) {
      const newInitData = Object.assign({}, state.initData, payload);
      return {
        ...state,
        initData: newInitData,
      }
    },
    clear(state) {
      return {
        ...state,
        pmPlan: {},
      };
    },
  },
};

export default model;
