import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { isEqual, get } from 'lodash';
import { formatMessage } from 'umi/locale';
import IconButton from '@/componentsPP/IconButton';
import deleteModal from '@/componentsPP/DeleteModal';
import { Col, Row, Tabs } from '@/componentsPP/UIElements';
import DropdownMenu from '@/componentsPP/DropdownMenu';
import JobModal, { NAMESPACE as JOB_MODAL_NAMESPACE } from '@/componentsPP/_share/JobModal';
import { SORT_ORDER, CONFIRM_MODAL_TYPE, PP_FONT_MEDIUM } from '@/constants';
import Open from './components/open';
import { NAMESPACE as DETAIL_NAMESPACE } from '../../../../constants';
import { NAMESPACE } from './constants';
import messages from './messages';
import Styles from './styles.less';

const { TabPane } = Tabs;

@connect((state) => ({
  jobs: state[NAMESPACE].jobs,
  totalPage: state[NAMESPACE].totalPage,
  totalItem: state[NAMESPACE].totalItem,
  fetchJobIndex: state[NAMESPACE].fetchJobIndex,
  changedIndex: state[NAMESPACE].changedIndex,
  deletedIndex: state[NAMESPACE].deletedIndex,
  jobTypes: state[NAMESPACE].jobTypes,
  accessedStatuses: state[NAMESPACE].accessedStatuses,
  jobModalStatus: {
    isModalCreating: state.loading.effects[`${JOB_MODAL_NAMESPACE}/create`],
    isModalUpdating: state.loading.effects[`${JOB_MODAL_NAMESPACE}/update`],
  },
  loadingJobs: state.loading.effects[`${NAMESPACE}/fetchJobs`],
  refreshPageIndex: state[`${DETAIL_NAMESPACE}`].changedIndex,
}))
class Jobs extends Component {
  static propTypes = {
    client: PropTypes.object.isRequired,
    
    jobs: PropTypes.array.isRequired,
    totalPage: PropTypes.number.isRequired,
    totalItem: PropTypes.number.isRequired,
    fetchJobIndex: PropTypes.number.isRequired,
    changedIndex: PropTypes.number.isRequired,
    deletedIndex: PropTypes.number.isRequired,
    accessedStatuses: PropTypes.array.isRequired,
    jobTypes: PropTypes.array.isRequired,
    jobModalStatus: PropTypes.object.isRequired,
    loadingJobs: PropTypes.bool,
    refreshPageIndex: PropTypes.number.isRequired,
  };

  static defaultProps = {
    loadingJobs: false,
  };

  constructor(props) {
    super(props);
    this.filter = { sort: { order: SORT_ORDER.ascend, field: 'jobNumber', columnKey: 'jobNumber' } };
    this.state = {
      selectedRows: [],
      isOpen: false,
    }
  }

  componentDidMount() {
    this.fetchData();
    this.triggerSideBarReload();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(nextProps, this.props) || !isEqual(nextState, this.state);
  }

  componentDidUpdate(prevProps) {
    const { refreshPageIndex, fetchJobIndex, deletedIndex } = this.props;

    if (prevProps.fetchJobIndex !== fetchJobIndex) {
      this.fetchRelevantData();
    }
    if (this.shouldRefetchJobs(prevProps)) {
      this.fetchData();
    }
    if (refreshPageIndex !== prevProps.refreshPageIndex) {
      this.handleRefresh();
    }
    if (deletedIndex !== prevProps.deletedIndex) {
      this.setState({ selectedRows: [] })
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  }

  get renderJobModal() {
    const { selectedRows, isOpen } = this.state;
    const jobId = get(selectedRows, '0.id', '');

    return jobId ? (
      <JobModal
        isOpen={isOpen}
        id={jobId}
        onCancel={this.handleCancel}
      />
    ) : ''
  }

  get renderActionButton() {
    const { selectedRows } = this.state;
    const style = selectedRows.length <= 0 ? { display: 'none' } : {};
    const shouldDisplayEditIcon = selectedRows.length === 1;

    return (
      <div className={Styles.headerWrapper}>
        <div className={Styles.leftActionWrapper}>
          <DropdownMenu>
            <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
            <i className="material-icons">arrow_drop_down</i>
          </DropdownMenu>
          <div className={Styles.actions} style={style}>
            <span className={Styles.selected}>{`${selectedRows.length} selected`}</span>
            {
              shouldDisplayEditIcon && (
                <IconButton
                  icon="edit"
                  onClick={this.handleOpen}
                  tooltip={{ title: formatMessage(messages.edit) }}
                />
              )
            }
            <IconButton
              icon="delete"
              onClick={this.handButtonDeleteClicked}
              tooltip={{ title: formatMessage(messages.delete) }}
            />
          </div>
        </div>
      </div>
    )
  }

  handleOpen = e => {
    e.stopPropagation();
    this.setState({ isOpen: true });
  };

  handleCancel = () => {
    this.setState({ isOpen: false });
  };

  handButtonDeleteClicked = e => {
    e.stopPropagation();
    const { selectedRows } = this.state;

    const renderContent = () => {
      if (selectedRows.length === 1) {
        return (
          <span>
            {formatMessage(messages.deleteContent)}
            {' '}
            <a>{selectedRows[0].jobNumber}</a>
            {' '}
            {' '}

            ?
          </span>
        )
      }
      return <span>{formatMessage(messages.deleteMultiContent)}</span>
    };

    if (selectedRows && selectedRows.length > 0) {
      deleteModal({
        type: CONFIRM_MODAL_TYPE.delete,
        title: formatMessage(messages.deleteJob),
        content: renderContent(),
        okText: formatMessage(messages.delete),
        cancelText: formatMessage(messages.cancel),
        onOk: closeModal => {
          const jobIds = selectedRows.map(item => item.id);
          return this.deleteJob(jobIds, closeModal);
        },
        onCancel() { },
      })
    }
  };

  shouldRefetchJobs = (prevProps) => {
    const { changedIndex, jobModalStatus, client: { id: clientId } } = this.props;
    const { isModalCreating, isModalUpdating } = jobModalStatus;

    const clientIdChanged = !isEqual(clientId, prevProps.client.id);
    const jobChanged = changedIndex !== prevProps.changedIndex;
    const jobModalCreated = isModalCreating && isModalCreating !== prevProps.isModalCreating;
    const jobModalUpdated = isModalUpdating && isModalUpdating !== prevProps.isModalUpdating;

    return jobModalCreated || jobModalUpdated || jobChanged || clientIdChanged;
  };

  handleRefresh = (callback = this.fetchData) => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/refreshPage`,
      payload: {
        callback,
      },
    });
  };

  fetchRelevantData = () => {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/fetchRelevantData` });
  };

  fetchData = (currentPage = 1, filter = this.filter) => {
    const { dispatch, client: { id } } = this.props;
    if (id) {
      dispatch({
        type: `${NAMESPACE}/fetchJobs`,
        clientId: id,
        currentPage,
        filter,
      });
    }
  };

  handleSortPage = (currentPage, filter) => {
    this.filter = filter;
    this.fetchData(currentPage, filter);
  };

  updateJob = (jobId, item) => {
    const { dispatch, jobs } = this.props;
    const currentJob = jobs.filter(job => job.id === jobId)[0];
    if (currentJob) {
      dispatch({
        type: `${NAMESPACE}/updateJob`,
        payload: {
          jobInfo: Object.assign(currentJob, item),
        },
      })
    }
  };

  deleteJob = (jobIds, closeModal) => {
    const { client: { id: clientId } } = this.props;

    if (jobIds.length > 0 && clientId) {
      const { dispatch } = this.props;
      dispatch({
        type: `${NAMESPACE}/deleteJob`,
        payload: {
          jobIds,
          closeModal,
          triggerSideBarReload: this.triggerSideBarReload,
        },
      });
    }
  };

  handleSelectRow = selectedRows => {
    this.setState({ selectedRows });
  };

  triggerSideBarReload = () => {
    const { dispatch, client } = this.props;
    dispatch({
      type: `${DETAIL_NAMESPACE}/counting`,
      payload: {
        client,
      },
    });
  };

  handleTabChanged = () => {
    this.setState({ selectedRows: [] })
  };

  render() {
    const {
      jobs,
      totalItem,
      totalPage,
      loadingJobs,
      jobTypes,
      accessedStatuses,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <Row justify="space-between">
        <Col>
          {this.renderActionButton}
          <Tabs defaultActiveKey="1" type="card" onChange={this.handleTabChanged} className={Styles.tabWrapper}>
            <TabPane tab="Jobs" key="1" className={Styles.tabPane} forceRender>
              <Open
                jobs={jobs}
                totalItem={totalItem}
                totalPage={totalPage}
                jobTypes={jobTypes}
                accessedStatuses={accessedStatuses}
                loadingJobs={loadingJobs}
                selectedRows={selectedRows}
                handleSelectRow={this.handleSelectRow}
                updateJob={this.updateJob}
                fetchData={this.fetchData}
                handleSortPage={this.handleSortPage}
              />
            </TabPane>
          </Tabs>
        </Col>
        {this.renderJobModal}
      </Row>
    );
  }
}

export default Jobs;
