import { get } from 'lodash';
import { NAMESPACE } from './constants';
import { getCasesByClientId, getStatus, updateCase, deleteCase } from '@/utils/case';

const defaultState = {
  cases: [],
  fetchCaseIndex: 0,
  changedIndex: 0,
  deletedIndex: 0,
  statusDS: [],
  totalPage: 0,
  totalItem: 0,
};

const model = {
  namespace: NAMESPACE,

  state: defaultState,

  effects: {
    *fetchCases({ clientId, currentPage, filter }, { call, put }) {
      const response = yield call(getCasesByClientId, clientId, currentPage, filter);
      if (response) {
        yield put({
          type: 'saveCases',
          payload: {
            cases: get(response, 'results.cases', []),
            totalPage: get(response, 'results.totalPage', 0),
            totalItem: get(response, 'results.totalItem', 0),
          },
        });
      }
    },
    *fetchRelevantData(_, { call, put }) {
      const response = yield call(getStatus);
      if (response) {
        yield put({
          type: 'saveRelevantData',
          payload: {
            status: get(response, 'results.status', []),
          },
        });
      }
    },
    *refreshPage({ payload: { callback } }, { put }) {
      yield put({ type: 'null' });
      if (typeof callback === 'function') {
        callback();
      }
    },
    *updateCase({ payload: { caseInfo } }, { call, put }) {
      const response = yield call(updateCase, caseInfo);
      if (response) {
        yield put({ type: 'update' });
      }
    },
    *deleteCase({ payload: { caseIds, closeModal, triggerSideBarReload } }, { all, call, put }) {
      const response = yield all(caseIds.map(caseId => call(deleteCase, caseId)));
      if (response) {
        yield put({ type: 'delete' });
        if (typeof closeModal === 'function') {
          closeModal();
        }
        if (typeof triggerSideBarReload === 'function') {
          triggerSideBarReload();
        }
      }
    },
  },

  reducers: {
    saveCases(state, { payload: { cases, totalPage, totalItem } }) {
      return {
        ...state,
        cases,
        totalPage,
        totalItem,
        fetchCaseIndex: state.fetchCaseIndex + 1,
      };
    },
    saveRelevantData(state, { payload: { status } }) {
      const map = (data) => data.map(({ id, name }) => ({ id, name, value: id }));
      return {
        ...state,
        statusDS: map(status),
      }
    },
    update(state) {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
      };
    },
    delete(state) {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
        deletedIndex: state.deletedIndex + 1,
      };
    },
    clear(state) {
      return {
        ...state,
        ...defaultState,
        fetchCaseIndex: state.fetchCaseIndex,
        changedIndex: state.changedIndex,
        deletedIndex: state.deletedIndex,
      }
    },
  },
};

export default model;
