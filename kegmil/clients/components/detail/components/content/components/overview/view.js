import React from 'react';
import PropTypes from 'prop-types';
import { Tabs } from '@/componentsPP/UIElements';
import DropdownMenu from '@/componentsPP/DropdownMenu';
import { PP_FONT_MEDIUM } from '@/constants';
import Details from './components/details';
import Locations from './components/locations';
import Contacts from './components/contacts';
import Styles from './styles.less';

const { TabPane } = Tabs;

const OverviewView = ({
  client,
  contacts,
  locations,
  showHeader,
  onTabChange,
  fetchContacts,
  fetchLocations,
}) => {
  const { id } = client;
  return (
    <div className={Styles.overviewWrapper}>
      {showHeader && (
        <div className={Styles.headerWrapper}>
          <DropdownMenu>
            <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
            <i className="material-icons">arrow_drop_down</i>
          </DropdownMenu>
        </div>
      )}
      <Tabs type="card" className={Styles.overviewTabs} onChange={onTabChange}>
        <TabPane key="details" tab='Details' className={Styles.tab}>
          <Details client={client} />
        </TabPane>
        <TabPane key="contacts" tab={`Contacts(${contacts.length})`} className={Styles.tab}>
          <Contacts clientId={id} fetchContacts={fetchContacts} contacts={contacts} />
        </TabPane>
        <TabPane key="locations" tab={`Locations(${locations.length})`} className={Styles.tab}>
          <Locations
            contacts={contacts}
            locations={locations}
            fetchLocations={fetchLocations}
            clientId={id}
          />
        </TabPane>
      </Tabs>
    </div>
  );
}

OverviewView.propTypes = {
  client: PropTypes.object,
  contacts: PropTypes.array,
  locations: PropTypes.array,
  showHeader: PropTypes.bool,
  onTabChange: PropTypes.func.isRequired,
}

OverviewView.defaultProps = {
  client: {},
  contacts: [],
  locations: [],
  showHeader: false,
}

export default OverviewView;
