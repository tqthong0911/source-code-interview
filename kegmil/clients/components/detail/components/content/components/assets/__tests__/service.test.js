import { getAssetDetail } from '../service';

let mockRequest;
jest.mock('@/utils/request', () => {
  mockRequest = jest.fn();
  return mockRequest;
});

describe('AssetPanel service', () => {
  it('should get asset detail when it calls getAssetDetail', async() => {
    const jobId = 1;
    await getAssetDetail(jobId);
    expect(mockRequest).toHaveBeenCalled();
  });
});
