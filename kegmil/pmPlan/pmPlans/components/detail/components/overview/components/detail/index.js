import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty, isUndefined } from 'lodash';
import classNames from 'classnames';
import { Link } from 'dva/router';
import { Col, Row, Tag, Checkbox } from '@/componentsPP/UIElements';
import { PP_FONT_MEDIUM, EMPTY_VALUE } from '@/constants';
import { getFieldsToDisplay } from './services';
import Styles from './styles.less';

const Detail = (props) => {
  const { handleChangeAutoGen, pmPlan, initData } = props;
  const fields = getFieldsToDisplay(pmPlan, initData);
  const { clientId, clientName, contractId, contractName } = fields;
  const client = isUndefined(clientId) || isEmpty(clientId) ? EMPTY_VALUE : (
    <Link to={`/client/clients/${clientId}`}><b>{clientName}</b></Link>
  );
  const contract = isUndefined(contractId) ? EMPTY_VALUE :  (
    <Link to={`/pm-plan/contract/${contractId}`}><b>{contractName}</b></Link>
  );
  return (
    <div className={Styles.wrapperDetailTab}>
      <Row>
        <Col md={24}>
          <Row>
            <Col md={24} className={Styles.header}>
              <span>PM Plan Details</span>
            </Col>
          </Row>
          <Row gutter={16} className={Styles.attribute}>
            <Col md={24} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Description</span>
              <span className={Styles.data}>{fields.description}</span>
            </Col>
          </Row>

          <Row gutter={16} className={Styles.attribute}>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Client</span>
              <span className={Styles.data}>
                {client}
              </span>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Type</span>
              <Tag><span className={Styles.data}><b>{fields.jobTypeName}</b></span></Tag>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Contract</span>
              <span className={Styles.data}>{contract}</span>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Location</span>
              <span className={Styles.data}><Link to='/client/locations'><b>{fields.locationName}</b></Link></span>
            </Col>
          </Row>

          <Row gutter={16} className={Styles.attribute}>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Frequency</span>
              <span className={Styles.data}>{fields.frequencyDisplay}</span>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Gen Timeframe</span>
              <span className={Styles.data}>{fields.genTimeframeDisplay}</span>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Date of First Job</span>
              <span className={Styles.data}>{fields.firstDate}</span>
            </Col>
            <Col md={6} className={Styles.attribute}>
              <Checkbox onChange={(e) => { handleChangeAutoGen(e.target.checked) }} checked={fields.autoGen}>
                <span className={classNames(Styles.title, PP_FONT_MEDIUM)} style={{ display: 'inline' }}>

                  Auto-generate Jobs
                </span>
              </Checkbox>
            </Col>

          </Row>
          <Row gutter={16} className={Styles.attribute}>
            <Col md={6} className={Styles.attribute}>
              <span className={classNames(Styles.title, PP_FONT_MEDIUM)}>Generate Horizon</span>
              <span className={Styles.data}>{fields.genHorizon}</span>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

Detail.propTypes = {
  pmPlan: PropTypes.object.isRequired,
  handleChangeAutoGen: PropTypes.func.isRequired,
};

export default Detail;
