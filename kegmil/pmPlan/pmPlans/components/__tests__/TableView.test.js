import React from 'react';
import { shallow } from 'enzyme';

import StandardTable from '@/componentsPP/StandardTable';
import TableView from '../TableView';
import { PAGE_SIZE } from '@/constants';

describe('<TableView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      scrollWidth: '100%',
      JobsType: [1, 2].map(value => ({
        id: `${value}`,
        name: `Job ${value}`,
      })),
      pmPlans: [1, 2].map(value => ({ id: value, title: `Job ${value}` })),
      selectedRows: [],
      totalPage: 1,
      loading: false,
      handleChangeFilterTableView: jest.fn(),
      handleUpdateSelectedRows: jest.fn(),
    };
    wrapper = shallow(<TableView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(StandardTable).length).toBe(1);
  });

  it('should set state and call handleChange prop when it calls handleChange', () => {
    const pagination = { current: 1 };
    const sort = { columnKey: 'columnKey-1', order: 'ascend' };
    wrapperInstance.handleChange(pagination, {}, sort);

    expect(props.handleChangeFilterTableView).toHaveBeenCalledWith(
      {
        field: 'columnKey-1',
        order: 'ascend',
      },
      pagination.current,
    );
  });

  it('should return paginationConfig when it calls getPaginationConfig', () => {
    const result = wrapperInstance.getPaginationConfig;
    const totalPage = 1;
    expect(result.simple).toEqual(true);
    expect(result.total).toEqual(totalPage * PAGE_SIZE);
    expect(result.pageSize).toEqual(PAGE_SIZE);
  });

  it('should return column config when it calls getColumnConfig', () => {
    const result = wrapperInstance.getColumnConfig;
    expect(result.length).toBe(6);
  });

  it('should update table data when it change pmPlans in componentDidUpdate', () => {
    const newPMPlans = [3, 4].map(e => ({
      id: `${e}`,
      planId: `${e}`,
      name: `${e}`,
      type: `${e}`,
      clientId: `${e}`,
    }));
    wrapperInstance.setState = jest.fn();
    wrapper.setProps({ pmPlans: newPMPlans });
    expect(wrapperInstance.setState).toHaveBeenCalled();
  });

});
