import globalMessages from '@/messages';

export default {
  refresh: globalMessages.common.refresh,
  edit: globalMessages.common.edit,
  delete: globalMessages.common.delete,
  more: globalMessages.common.more,
  active: globalMessages.common.active,
  inActive: globalMessages.common.inActive,
};
