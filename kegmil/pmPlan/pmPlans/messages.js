import globalMessages from '@/messages';

export default {
  refresh: globalMessages.common.refresh,
  allPMPlans: globalMessages.pmPlan.allPMPlans,
  selected: globalMessages.common.selected,
  new: globalMessages.common.new,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  deletePMPlan: globalMessages.pmPlan.deletePMPlan,
  deleteContent: globalMessages.common.deleteContent,
  deleteJobCreatedByPmPlan: globalMessages.pmPlan.deleteJobCreatedByPmPlan,
  
  id: globalMessages.common.id,
  name: globalMessages.common.name,
  jobType: globalMessages.pmPlan.jobType,
  client: globalMessages.common.client,
  startDate: globalMessages.pmPlan.startDate,
  endDate: globalMessages.pmPlan.endDate,
};
