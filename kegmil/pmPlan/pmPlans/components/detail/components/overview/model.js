import { get } from 'lodash';

import { getClientDetail } from '@/utils/client';
import { getJobType } from '@/utils/jobType';
import { getStatusPmPlan } from '@/utils/pmPlan';
import { getLocationDetail } from '@/utils/location';

import { NAMESPACE } from './constants';
import { getContractById } from '@/componentsPP/_share/ContractModal';

const defaultState = {
  initData: {
    client: {},
    contract: {},
    jobTypes: [],
    statuses: [],
    location: {},
  },
};

const model = {
  namespace: NAMESPACE,

  state: defaultState,

  effects: {
    *fetchInitData({ payload }, { call, put, all }) {
      const { clientId, locationId, contractId } = payload;
      const [
        jobTypesResponse,
        statusReponse,
        clientResponse,
        locationResponse,
        contractResponse,
      ] = yield all([
        call(getJobType),
        call(getStatusPmPlan),
        clientId && call(getClientDetail, clientId),
        locationId && call(getLocationDetail, locationId),
        clientId && contractId && call(getContractById, clientId, contractId),
      ]);
      if (clientResponse || jobTypesResponse || statusReponse || locationResponse) {
        yield put({
          type: 'saveInitData',
          payload: {
            client: get(clientResponse, 'results.client', {}),
            jobTypes: get(jobTypesResponse, 'results.jobType', []),
            statuses: get(statusReponse, 'results.statuses', []),
            location: get(locationResponse, 'results.location', {}),
            contract: get(contractResponse, 'results.contract', {}),
          },
        })
      }
    },
  },

  reducers: {
    saveInitData(state, { payload }) {
      const newInitData = Object.assign({}, state.initData, payload);
      return {
        ...state,
        initData: newInitData,
      };
    },
  },
};

export default model;
