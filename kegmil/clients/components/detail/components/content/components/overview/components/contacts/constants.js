export const NAMESPACE = 'pages.client.detail.overview.contacts';

export const ACTIONS = {
  new: 'new',
  edit: 'edit',
};
