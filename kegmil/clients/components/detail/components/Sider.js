import React from 'react';
import PropTypes from 'prop-types';
import { Menu } from '@/componentsPP/UIElements';
import Styles from './Sider.less';

const { Item: MenuItem } = Menu;

export const MenuItemShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
});

const Sider = ({ menu, activeMenu, onMenuSelect, quantity }) => (
  <div className={Styles.sideMenuWrapper}>
    <Menu
      mode="inline"
      style={{ height: '100%', width: 240 }}
      selectedKeys={[activeMenu]}
      onSelect={onMenuSelect}
    >
      {menu.map(({ key, name }) => (
        <MenuItem key={key}>
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }}>{name}</div>
            <div style={{ marginLeft: 'auto' }}>{quantity[key]}</div>
          </div>
        </MenuItem>
      ))}
    </Menu>
  </div>
);

Sider.propTypes = {
  activeMenu: PropTypes.string.isRequired,
  onMenuSelect: PropTypes.func.isRequired,
  menu: PropTypes.arrayOf(MenuItemShape),
}

Sider.defaultProps = {
  menu: [],
}

export default Sider;
