import React, { PureComponent, Fragment } from 'react';
import propTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';
import { formatMessage } from 'umi/locale';

import { withPermissionContext, PERMISSIONS } from '@/utils/permission';
import ClientModal from '@/componentsPP/_share/ClientModal';
import IconButton from '@/componentsPP/IconButton';
import { PP_FONT_MEDIUM } from '@/constants';

import NewAction from './NewAction';
import messages from '../messages';
import Styles from './LeftAction.less';

export class LeftAction extends PureComponent {
  static propTypes = {
    selectedClients: propTypes.array.isRequired,
    handleDelete: propTypes.func.isRequired,
    // handleExport: propTypes.func.isRequired,
    permission: propTypes.object.isRequired,
  };

  state = {
    isEdit: false,
  };

  handleOpenEditModal = () => {
    this.setState({ isEdit: true });
  };

  handleCloseEditModal = () => {
    this.setState({ isEdit: false });
  };

  render() {
    const { selectedClients, handleDelete, permission } = this.props;
    const isUpdatePermission = get(permission, PERMISSIONS.clientsContacts.updateClients, false);
    const isDeletePermission = get(permission, PERMISSIONS.clientsContacts.deleteClients, false);
    // const isExportPermission = get(permission, PERMISSIONS.clientsContacts.exportClients, false);
    const { isEdit } = this.state;
    if (selectedClients && selectedClients.length > 0) {
      // const isExportVisile = isExportPermission && selectedClients.length > 0;
      const isEditVisile = isUpdatePermission && selectedClients.length === 1;
      const isDeleteVisile = isDeletePermission && selectedClients.length === 1;

      return (
        <Fragment>
          <div className={Styles.wrapper}>
            <span className={classNames(Styles.selectedRow, PP_FONT_MEDIUM)}>
              {`${selectedClients.length} ${formatMessage(messages.selected)}`}
            </span>
            {/* {isExportVisile && <IconButton icon="vertical_align_bottom" onClick={handleExport} />} */}
            {isEditVisile && <IconButton icon="edit" onClick={this.handleOpenEditModal} />}
            {isDeleteVisile && <IconButton icon="delete" onClick={handleDelete} />}
            {/* <IconButton icon="more_horiz" onClick={() => {}} /> */}
          </div>
          <ClientModal
            isOpen={isEdit}
            client={selectedClients[0]}
            handleCancel={this.handleCloseEditModal}
          />
        </Fragment>
      );
    }

    return <NewAction />;
  }
}

export default withPermissionContext(LeftAction);
