import React, { PureComponent, Fragment } from 'react';
import propTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { get } from 'lodash';

import { Menu } from '@/componentsPP/UIElements';
import DropDownMenuButton from '@/componentsPP/DropdownMenuButton';
import ClientModal from '@/componentsPP/_share/ClientModal';
import LocationModal from '@/componentsPP/_share/LocationModal';
import ContactModal from '@/componentsPP/_share/ContactModal';
import ImportModal, { IMPORT_TYPE } from '@/componentsPP/_share/ImportModal';
import { withPermissionContext, CLIENT_PERMISION_PATH } from '@/utils/permission';

import { MODAL_TYPE } from '../constants';
import messages from '../messages';
import Styles from './NewAction.less';

export class NewAction extends PureComponent {
  static propTypes = {
    permission: propTypes.object.isRequired,
  };

  state = {
    modelType: '',
  };

  onItemClick = item => {
    this.setState({ modelType: item.key });
  };

  handleCancel = () => {
    this.setState({ modelType: '' });
  };

  handleNewClick = () => {
    const { permission } = this.props;
    if (get(permission, CLIENT_PERMISION_PATH.update, false)) {
      this.setState({ modelType: MODAL_TYPE.client });
    }
  };

  renderImportModal = () => {
    const { modelType } = this.state;
    let importProps = {
      type: IMPORT_TYPE.client,
      title: formatMessage(messages.importClients),
      description: formatMessage(messages.importClientsDescription),
      isOpen: modelType === MODAL_TYPE.clientImport,
    };
    
    if (modelType === MODAL_TYPE.contactImport) {
      importProps = {
        type: IMPORT_TYPE.contact,
        title: formatMessage(messages.importContacts),
        description: formatMessage(messages.importContactsDescription),
        isOpen: true,
      };
    }

    return (
      <ImportModal
        {...importProps}
        handleCancel={this.handleCancel}
      />
    );
  };

  render() {
    const { permission } = this.props;
    const isUpdateContactPermission = get(permission, CLIENT_PERMISION_PATH.updateContact, false);

    const { modelType } = this.state;
    const isClientOpen = modelType === MODAL_TYPE.client;
    const isContactOpen = modelType === MODAL_TYPE.contact;
    const isLocationOpen = modelType === MODAL_TYPE.location;

    const menu = (
      <Menu onClick={this.onItemClick}>
        <Menu.Item key={MODAL_TYPE.location}>
          <i className={`material-icons ${Styles.itemIcon}`}>add</i>
          {formatMessage(messages.newLocation)}
        </Menu.Item>
        {isUpdateContactPermission && (
          <Menu.Item key={MODAL_TYPE.contact}>
            <i className={`material-icons ${Styles.itemIcon}`}>add</i>
            {formatMessage(messages.newContact)}
          </Menu.Item>
        )}
        <Menu.Item key={MODAL_TYPE.clientImport}>
          <i className={`material-icons ${Styles.itemIcon}`}>vertical_align_top</i>
          {formatMessage(messages.importClients)}
        </Menu.Item>
        <Menu.Item key={MODAL_TYPE.contactImport}>
          <i className={`material-icons ${Styles.itemIcon}`}>vertical_align_top</i>
          {formatMessage(messages.importContacts)}
        </Menu.Item>
      </Menu>
    );

    return (
      <Fragment>
        <div className={Styles.leftAction}>
          <DropDownMenuButton
            btn={{
              onClick: this.handleNewClick,
              childrenContent: formatMessage(messages.new),
            }}
            menu={menu}
          />
        </div>
        <ClientModal
          isOpen={isClientOpen}
          handleCancel={this.handleCancel}
        />
        <ContactModal
          isOpen={isContactOpen}
          handleCancel={this.handleCancel}
        />
        <LocationModal
          visible={isLocationOpen}
          onCancel={this.handleCancel}
        />
        {this.renderImportModal()}
      </Fragment>
    );
  }
}

export default withPermissionContext(NewAction);
