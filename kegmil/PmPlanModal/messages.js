import { defineMessages } from 'umi/locale';
import globalMessages from '@/messages';
import { NAMESPACE } from './constants';

const messages = defineMessages({
  details: {
    id: `${NAMESPACE}.details`,
    defaultMessage: 'Details',
  },
  triggers: {
    id: `${NAMESPACE}.triggers`,
    defaultMessage: 'Triggers',
  },
  asset: {
    id: `${NAMESPACE}.asset`,
    defaultMessage: 'Asset',
  },
  frequencyPlaceholder: {
    id: `${NAMESPACE}.frequencyPlaceholder`,
    defaultMessage: 'E.g. 3',
  },
  frequencyTypePlaceholder: {
    id: `${NAMESPACE}.frequencyTypePlaceholder`,
    defaultMessage: 'E.g. Monthly',
  },
  genTimeframePlaceholder: {
    id: `${NAMESPACE}.genTimeframePlaceholder`,
    defaultMessage: 'E.g. 3',
  },
  genTimeframeTypePlaceholder: {
    id: `${NAMESPACE}.genTimeframeTypePlaceholder`,
    defaultMessage: 'E.g. Years',
  },
  autoGen: {
    id: `${NAMESPACE}.autoGen`,
    defaultMessage: 'Auto-generate jobs',
  },
  labelGenHorizon: {
    id: `${NAMESPACE}.labelGenHorizon`,
    defaultMessage: 'Generate Horizon (Days)',
  },
  placeholderGenHorizon: {
    id: `${NAMESPACE}.placeholderGenHorizon`,
    defaultMessage: 'E.g. 1',
  },
  changingConfirm: {
    id: `${NAMESPACE}.changingConfirm`,
    defaultMessage: 'Change Client/Contract will remove all selected Assets. Are you sure you want to continue?',
  },
  generateTask: {
    id: `${NAMESPACE}.generateTask`,
    defaultMessage: 'Generate Task?',
  },
});

export default {
  ...messages,
  update: globalMessages.common.update,
  saveAndNew: globalMessages.common.saveAndNew,
  save: globalMessages.common.save,
  cancel: globalMessages.common.cancel,
  yes: globalMessages.common.yes,
  no: globalMessages.common.no,
  editPmPlan: globalMessages.pmPlan.editPmPlan,
  newPmPlan: globalMessages.pmPlan.newPmPlan,

  assetName: globalMessages.asset.assetName,
  assetId: globalMessages.asset.assetId,
  type: globalMessages.asset.type,
  status: globalMessages.asset.status,
  frequency: globalMessages.pmPlan.frequency,
  generateTimeframe: globalMessages.pmPlan.generateTimeframe,
  firstDate: globalMessages.pmPlan.firstDate,
  required: globalMessages.form.required,
  whitespace: globalMessages.form.whitespace,
  datePlaceholder: globalMessages.form.datePlaceholder,
  namePlaceholder: globalMessages.form.namePlaceholder,

  name: globalMessages.common.name,
  client: globalMessages.common.client,
  searchClient: globalMessages.client.searchClient,
  contract: globalMessages.common.contract,
  searchContract: globalMessages.contract.searchContract,
  jobType: globalMessages.job.jobType,
  searchJobType: globalMessages.job.searchJobType,
  selectPlaceholder: globalMessages.form.selectPlaceholder,
  startDate: globalMessages.pmPlan.startDate,
  endDate: globalMessages.pmPlan.endDate,
  location: globalMessages.common.location,
  searchLocation: globalMessages.location.searchLocation,
  clientLocations: globalMessages.location.clientLocations,
  commonLocations: globalMessages.location.commonLocations,
  description: globalMessages.common.description,
  descriptionPlaceholder: globalMessages.form.descriptionPlaceholder,
};
