import React from 'react';
import { shallow } from 'enzyme';
import ContractsView from '../view';
import ContractTableContainer from '../components/contractTable';
import IconButton from '@/componentsPP/IconButton';

describe('<ContractsView />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      contracts: [],
      loading: false,
      onRefreshContracts: jest.fn(),
      totalPage: 0,
      selectedRows: [],
      onSelectRow: jest.fn(),
    };
    wrapper = shallow(<ContractsView {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(ContractTableContainer).length).toBe(1);
  });
  it('should render correctly when selectedRows is not empty', () => {
    props = {
      ...props,
      selectedRows: [1].map((value) => ({ id: `${value}` })),
    };

    wrapper = shallow(<ContractsView {...props} />);

    expect(wrapper.find(IconButton).length).toBe(2);
  });
});
