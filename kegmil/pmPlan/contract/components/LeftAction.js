import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import IconButton from '@/componentsPP/IconButton';
import deleteModal from '@/componentsPP/DeleteModal';
import ContractModalContainer from '@/componentsPP/_share/ContractModal';
import { CONFIRM_MODAL_TYPE } from '@/constants';
import NewAction from './NewAction';
import messages from '../messages';
import Styles from './LeftAction.less';

class LeftActionView extends PureComponent {
  static propTypes = {
    selectedContracts: PropTypes.array.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleReloadContract: PropTypes.func.isRequired,
  };

  state = {
    isEdit: false,
  };

  handleDelete = () => {
    const { handleDelete, selectedContracts } = this.props;
    if (selectedContracts && selectedContracts.length > 0) {
      const [deletedContract] = selectedContracts;
      deleteModal({
        type: CONFIRM_MODAL_TYPE.delete,
        title: formatMessage(messages.deleteContract),
        content: (
          <div>
            {formatMessage(messages.deleteContent)}
            <span className={Styles.highlight}>{deletedContract.name}</span>
            ?
          </div>
        ),
        okText: formatMessage(messages.delete),
        cancelText: formatMessage(messages.cancel),
        onOk(closeModal) {
          return new Promise(() => handleDelete(selectedContracts, closeModal)).catch(() => { });
        },
        onCancel() { },
      });
    }
  };

  toggleEdit = () => this.setState(({ isEdit }) => ({ isEdit: !isEdit }));

  handleCloseEditModal = (callback) => {
    this.setState({ isEdit: false }, () => {
      if (callback) callback();
    });
  };

  renderContractModal = () => {
    const { selectedContracts, handleReloadContract } = this.props;
    if (selectedContracts.length !== 1) return null;
    const { isEdit } = this.state;
    return (
      <ContractModalContainer
        isOpen={isEdit}
        contractId={selectedContracts[0].id}
        clientId={selectedContracts[0].clientId}
        handleCancel={this.handleCloseEditModal}
        callback={handleReloadContract}
      />
    )
  }

  render() {
    const { selectedContracts, handleReloadContract } = this.props;
    if (selectedContracts && selectedContracts.length > 0) {
      const isEditVisible = selectedContracts.length === 1;
      const isDeleteVisible = selectedContracts.length === 1;

      return (
        <Fragment>
          <div className={Styles.wrapper}>
            <span className={Styles.selectedRow}>
              {`${selectedContracts.length} ${formatMessage(messages.selected)}`}
            </span>
            {isEditVisible && <IconButton icon="edit" onClick={this.toggleEdit} />}
            {isDeleteVisible && <IconButton icon="delete" onClick={this.handleDelete} />}
            {this.renderContractModal()}
          </div>
        </Fragment>
      );
    }
    return <NewAction handleReloadContract={handleReloadContract} />;
  }
}

export default LeftActionView;
