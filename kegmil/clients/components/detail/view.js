import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Spin } from '@/componentsPP/UIElements';
import withBreadcrumb from '@/utils/breadcrumb';
import Content from './components/content';
import Information from './components/information';
import RightAction from './components/RightAction';
import Sider from './components/Sider';
import { DETAIL_MENU } from './constants';
import Styles from './styles.less';

const root = { id: 'client', url: '/client/clients', name: 'Clients' };
const PageHeaderLayout = withBreadcrumb(root);

class ClientDetailView extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    client: PropTypes.object,
    locations: PropTypes.array,
    initData: PropTypes.object,
    onFetchQuantity: PropTypes.func.isRequired,
  }

  static defaultProps = {
    loading: false,
    client: {},
    locations: [],
    initData: {},
  }

  state = {
    activeMenu: DETAIL_MENU[0].key,
  }

  handleMenuSelect = ({ key }) => {
    this.setState({ activeMenu: key });
  }

  renderLeftAction = () => {
    const { client } = this.props;
    return (<Information client={client} />);
  }

  renderRightAction = () => (
    <RightAction />
  );

  render() {
    const { activeMenu } = this.state;
    const { loading, client, initData, quantity, locations, onFetchQuantity, clientId } = this.props;
    return (
      <PageHeaderLayout
        hideBreadcrumb
        leftAction={this.renderLeftAction()}
        action={this.renderRightAction()}
        contentClassName={Styles.contentWrapper}
        pageHeaderWrapper={Styles.pageHeaderWrapper}
        actionClassName={Styles.actionWrapper}
      >
        <Spin spinning={loading}>
          <div className={Styles.wrapper}>
            <Sider
              menu={DETAIL_MENU}
              activeMenu={activeMenu}
              onMenuSelect={this.handleMenuSelect}
              quantity={quantity}
            />
            <Content
              activeMenu={activeMenu}
              client={client}
              initData={initData}
              locations={locations}
              onFetchQuantity={onFetchQuantity}
              clientId={clientId}
            />
          </div>
        </Spin>
      </PageHeaderLayout>
    )
  }
}

export default ClientDetailView;
