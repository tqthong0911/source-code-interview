import { pathOr } from 'ramda';
import { fetchClientContracts } from '@/utils/contract';
import { PAGE_SIZE } from '@/constants';

export const fetchContracts = async(id, params) => {
  const fetcher = fetchClientContracts(`/client/${id}/contracts`)({
    size: PAGE_SIZE,
    ...params,
  });
  const response = await fetcher();
  const contracts = pathOr([], ['results', 'contracts'], response);
  const totalPage = pathOr(0, ['results', 'totalPage'], response);
  const totalItem = pathOr(0, ['results', 'totalItem'], response);
  return {
    contracts,
    totalPage,
    totalItem,
  };
}
