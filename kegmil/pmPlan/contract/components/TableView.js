import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, isEqual, get } from 'lodash';
import { formatMessage } from 'umi/locale';
import { Link } from 'dva/router';

import StandardTable from '@/componentsPP/StandardTable';
import { Tag } from '@/componentsPP/UIElements';
import { renderTextColumn, renderBasicText, renderOptionText } from '@/utils/layout/renderTable';
import { PAGE_SIZE, SORT_ORDER, PP_FONT_MEDIUM, EMPTY_VALUE } from '@/constants';

import { mapContractToView } from '../service';
import messages from '../messages';
import Styles from './TableView.less';

class TableView extends PureComponent {
  static propTypes = {
    contracts: PropTypes.array.isRequired,
    currentPage: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    totalPage: PropTypes.number.isRequired,
    selectedRows: PropTypes.array.isRequired,
    handleUpdateSelectedRows: PropTypes.func.isRequired,
    handleChangeFilterTableView: PropTypes.func.isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  state = {
    data: {
      list: [],
    },
  }

  componentDidUpdate(prevProps) {
    const { contracts } = this.props;
    if (!isEqual(prevProps.contracts, contracts)) {
      this.setState({
        data: {
          list: mapContractToView(contracts),
          pagination: this.getPaginationConfig,
        },
      })
    }
  };

  get getPaginationConfig() {
    const { totalPage, currentPage } = this.props;
    const allItems = totalPage * PAGE_SIZE;
    return {
      simple: true,
      total: allItems,
      current: currentPage,
      pageSize: PAGE_SIZE,
    };
  };

  get getColumnConfig() {
    return [
    //   {
    //   title: renderBasicText(formatMessage(messages.type)),
    //   dataIndex: 'type',
    //   key: 'type',
    //   width: '10%',
    //   sorter: true,
    //   render: text => (
    //     <div className={Styles.contractType}>
    //       {renderOptionText(text, { className: PP_FONT_MEDIUM })}
    //     </div>
    //   ),
    // }, 
      {
        title: renderBasicText(formatMessage(messages.name)),
        dataIndex: 'name',
        key: 'name',
        width: '25%',
        sorter: true,
        render: renderTextColumn,
        defaultSortOrder: SORT_ORDER.ascend,
      }, {
        title: renderBasicText(formatMessage(messages.slaTerms)),
        dataIndex: 'terms',
        key: 'terms',
        width: '120px',
        sorter: true,
        render: this.renderSLATermColumn,
      }, {
        title: renderBasicText(formatMessage(messages.client)),
        dataIndex: 'client',
        key: 'clientId',
        width: '10%',
        sorter: true,
        render: (text, record) => (
          <Link to={`/client/clients/${record.clientId}`}>
            {renderOptionText(text, { className: PP_FONT_MEDIUM })}
          </Link>
        ),
      }, {
        title: renderBasicText(formatMessage(messages.startDate)),
        dataIndex: 'startDate',
        key: 'startDate',
        width: '15%',
        sorter: true,
        render: renderTextColumn,
      }, {
        title: renderBasicText(formatMessage(messages.endDate)),
        dataIndex: 'endDate',
        key: 'endDate',
        width: '15%',
        sorter: true,
        render: renderTextColumn,
      }, {
        title: renderBasicText(formatMessage(messages.leadTime)),
        dataIndex: 'leadTimes',
        key: 'leadTimes',
        sorter: true,
        render: renderTextColumn,
      }];
  };

  renderSLATermColumn = text => (
    <Tag className={Styles.tag}>
      {renderOptionText(get(text, '[0]', EMPTY_VALUE), { className: PP_FONT_MEDIUM })}
    </Tag>
  );

  handleChange = (pagination, _, sorter) => {
    const { columnKey, order } = sorter;
    const sort = isEmpty(sorter) ? undefined : { field: columnKey, order };
    const { handleChangeFilterTableView } = this.props;
    handleChangeFilterTableView(sort, pagination.current);
  };

  render() {
    const { selectedRows, handleUpdateSelectedRows, loading, className } = this.props;
    const { data } = this.state;

    return (
      <StandardTable
        scroll={{ y: 'calc(100vh - 262px)' }}
        loading={loading}
        data={data}
        columns={this.getColumnConfig}
        selectedRows={selectedRows}
        onSelectRow={handleUpdateSelectedRows}
        onChange={this.handleChange}
        rowKey={record => record.id}
        className={className}
      />
    );
  }
}

export default TableView;
