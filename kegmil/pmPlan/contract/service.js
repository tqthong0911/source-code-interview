import request from '@/utils/request';
import moment from 'moment';
import { get } from 'lodash';
import { buildFilterParam } from '@/utils/job';
import { EMPTY_VALUE, PAGE_SIZE, DATE_TIME_FORMAT } from '@/constants';

export async function getContract(filter = {}, currentPage = 1) {
  const filterQueryString = buildFilterParam(filter);
  return request(`/client/contracts/query?page=${currentPage}&size=${PAGE_SIZE}${filterQueryString}`);
}

export async function deleteContract(contractId, clientId) {
  return request(`/client/${clientId}/contracts/${contractId}`, {
    method: 'DELETE',
  });
}

export function mapContractToView(contracts) {
  return contracts.map(contract => {
    const { type, id, name, terms, startDate, endDate, leadTimes, client, clientId } = contract;

    const result = {
      id,
      type: type || EMPTY_VALUE,
      name: name || EMPTY_VALUE,
      client: get(client, 'name', EMPTY_VALUE),
      clientId,
      terms: terms || EMPTY_VALUE,
      endDate: endDate ? moment(+endDate).format(DATE_TIME_FORMAT) : EMPTY_VALUE,
      startDate: startDate ? moment(+startDate).format(DATE_TIME_FORMAT) : EMPTY_VALUE,
      leadTimes: leadTimes || EMPTY_VALUE,
    };
    return result;
  });
}
