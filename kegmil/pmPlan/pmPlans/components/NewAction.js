import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { Button } from '@/componentsPP/UIElements';
import PMPlanModal from '@/componentsPP/_share/PmPlanModal';
import messages from '../messages';
import Styles from './NewAction.less';

class NewActionContainer extends PureComponent {
  static propTypes = {
    handleReloadPMPlan: PropTypes.func.isRequired,
  };

  state = {
    isPMPlanOpen: false,
  };

  handleCancel = (callback) => {
    this.setState({ isPMPlanOpen: false }, () => {
      if (callback) callback();
    });
  };

  handleNewClick = () => {
    this.setState({ isPMPlanOpen: true });
  };

  render() {
    const { handleReloadPMPlan } = this.props;
    const { isPMPlanOpen } = this.state;

    return (
      <Fragment>
        <div className={Styles.wrapper}>
          <Button type="primary" onClick={this.handleNewClick} size="small">
            {formatMessage(messages.new)}
          </Button>
        </div>
        <PMPlanModal
          isOpen={isPMPlanOpen}
          handleCancel={this.handleCancel}
          callback={handleReloadPMPlan}
        />
      </Fragment>
    );
  }
}

export default NewActionContainer;
