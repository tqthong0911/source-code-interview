import React from 'react';
import { shallow } from 'enzyme';

import DropDownMenuButton from '@/componentsPP/DropdownMenuButton';
import ClientModal from '@/componentsPP/_share/ClientModal';
import LocationModal from '@/componentsPP/_share/LocationModal';
import ContactModal from '@/componentsPP/_share/ContactModal';

import { NewAction } from '../NewAction';
import { MODAL_TYPE } from '../../constants';

describe('<NewAction />', () => {
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    wrapper = shallow(<NewAction permission={{}} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(DropDownMenuButton).length).toBe(1);
    expect(wrapper.find(ClientModal).length).toBe(1);
    expect(wrapper.find(LocationModal).length).toBe(1);
    expect(wrapper.find(ContactModal).length).toBe(1);
  });

  it('should update modelType state when it calls onItemClick', () => {
    wrapperInstance.state.modelType = '';
    const item = { name: 'New Client', icon: 'add', key: MODAL_TYPE.client };
    wrapperInstance.onItemClick(item);

    expect(wrapperInstance.state.modelType).toBe(MODAL_TYPE.client);
  });

  it('should update modelType state to empty when it calls handleCancel', () => {
    wrapperInstance.state.modelType = MODAL_TYPE.client;
    wrapperInstance.handleCancel();

    expect(wrapperInstance.state.modelType).toBe('');
  });

  it('should update modelType state to job type when it calls handleNewClick', () => {
    wrapperInstance.state.modelType = '';
    wrapperInstance.handleNewClick();

    expect(wrapperInstance.state.modelType).toBe('');
  });
});
