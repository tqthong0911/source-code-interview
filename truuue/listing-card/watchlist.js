import {CALL_API, getJSON} from 'redux-api-middleware';
import {getCSRFTokenKey} from '../utils/TokenHelper';
import {TRUUUE_BASE_URL} from './'

window.CALL_API = CALL_API;

const GET_WATCHLIST_ITEMS_REQUEST = 'GET_WATCHLIST_ITEMS_REQUEST';
const GET_WATCHLIST_ITEMS_SUCCESS = 'GET_WATCHLIST_ITEMS_SUCCESS';
const GET_WATCHLIST_ITEMS_FAILURE = 'GET_WATCHLIST_ITEMS_FAILURE';

const GET_WATCHLIST_ITEM_REQUEST = 'GET_WATCHLIST_ITEM_REQUEST';
const GET_WATCHLIST_ITEM_SUCCESS = 'GET_WATCHLIST_ITEM_SUCCESS';
const GET_WATCHLIST_ITEM_FAILURE = 'GET_WATCHLIST_ITEM_FAILURE';

const POST_WATCHLIST_ITEM_REQUEST = 'POST_WATCHLIST_ITEM_REQUEST';
const POST_WATCHLIST_ITEM_SUCCESS = 'POST_WATCHLIST_ITEM_SUCCESS';
const POST_WATCHLIST_ITEM_FAILURE = 'POST_WATCHLIST_ITEM_FAILURE';

const REMOVE_WATCHLIST_ITEM_REQUEST = 'REMOVE_WATCHLIST_ITEM_REQUEST';
const REMOVE_WATCHLIST_ITEM_SUCCESS = 'REMOVE_WATCHLIST_ITEM_SUCCESS';
const REMOVE_WATCHLIST_ITEM_FAILURE = 'REMOVE_WATCHLIST_ITEM_FAILURE';

const REMOVE_WATCHLIST_LIST_REQUEST = 'REMOVE_WATCHLIST_LIST_REQUEST';
const REMOVE_WATCHLIST_LIST_SUCCESS = 'REMOVE_WATCHLIST_LIST_SUCCESS';
const REMOVE_WATCHLIST_LIST_FAILURE = 'REMOVE_WATCHLIST_LIST_FAILURE';

const ADD_WATCHLIST_TO_LIST = 'ADD_WATCHLIST_TO_LIST';
const REMOVE_WATCHLIST_FROM_LIST = 'REMOVE_WATCHLIST_FROM_LIST';

export const actionTypes = {
    GET_WATCHLIST_ITEMS_REQUEST,
    GET_WATCHLIST_ITEMS_SUCCESS,
    GET_WATCHLIST_ITEMS_FAILURE,

    GET_WATCHLIST_ITEM_REQUEST,
    GET_WATCHLIST_ITEM_SUCCESS,
    GET_WATCHLIST_ITEM_FAILURE,

    POST_WATCHLIST_ITEM_REQUEST,
    POST_WATCHLIST_ITEM_SUCCESS,
    POST_WATCHLIST_ITEM_FAILURE,

    REMOVE_WATCHLIST_ITEM_REQUEST,
    REMOVE_WATCHLIST_ITEM_SUCCESS,
    REMOVE_WATCHLIST_ITEM_FAILURE,

    REMOVE_WATCHLIST_LIST_REQUEST,
    REMOVE_WATCHLIST_LIST_SUCCESS,
    REMOVE_WATCHLIST_LIST_FAILURE,

    ADD_WATCHLIST_TO_LIST,
    REMOVE_WATCHLIST_FROM_LIST
};

export const getWatchlistItems = () => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/watch-list/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                GET_WATCHLIST_ITEMS_REQUEST,
                GET_WATCHLIST_ITEMS_SUCCESS,
                GET_WATCHLIST_ITEMS_FAILURE,
            ]
        }
    })
};

export const getWatchlistItem = (id_hash) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'GET',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/watch-list/${id_hash}/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                GET_WATCHLIST_ITEM_REQUEST,
                GET_WATCHLIST_ITEM_SUCCESS,
                GET_WATCHLIST_ITEM_FAILURE,
            ]
        }
    })
};

const processResponse = (res) => {
    const contentType = res.headers.get('Content-Type');
    if (contentType && ~contentType.indexOf('json')) {
        return getJSON(res).then((json) => json);
    }
};

export const postWatchlistItem = (body) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'POST',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/watch-list/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            body: JSON.stringify(body),
            types: [
                POST_WATCHLIST_ITEM_REQUEST,
                {
                    type: POST_WATCHLIST_ITEM_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(getWatchlistItems());
                        return processResponse(res);
                    }
                },
                POST_WATCHLIST_ITEM_FAILURE,
            ]
        }
    })
};

export const removeWatchlistItem = (id) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'DELETE',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/kf/watch-list/${id}/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            credentials: 'same-origin',
            types: [
                REMOVE_WATCHLIST_ITEM_REQUEST,
                {
                    type: REMOVE_WATCHLIST_ITEM_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(getWatchlistItems());
                        return processResponse(res);
                    }
                },
                REMOVE_WATCHLIST_ITEM_FAILURE,
            ]
        }
    })
};

export const removeWatchlistList = (list) => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            method: 'DELETE',
            endpoint: `${TRUUUE_BASE_URL}/api/v1/user/me/watchlist/delete/`,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCSRFTokenKey()
            },
            body: JSON.stringify({items: list}),
            credentials: 'same-origin',
            types: [
                REMOVE_WATCHLIST_LIST_REQUEST,
                REMOVE_WATCHLIST_LIST_SUCCESS,
                REMOVE_WATCHLIST_LIST_FAILURE,
            ]
        }
    })
};

export const addWatchlistToList = (id) => (dispatch) => {
    return dispatch({
        type: ADD_WATCHLIST_TO_LIST,
        payload: {
            watchlistId: id
        }
    })
};

export const removeWatchlistFromList = (id) => (dispatch) => {
    return dispatch({
        type: REMOVE_WATCHLIST_FROM_LIST,
        payload: {
            watchlistId: id
        }
    })
};

export const actionCreators = {
    getWatchlistItems,
    getWatchlistItem,
    postWatchlistItem,
    removeWatchlistItem
};
