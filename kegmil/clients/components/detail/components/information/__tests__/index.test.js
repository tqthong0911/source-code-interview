import React from 'react';
import { shallow } from 'enzyme';

import Avatar from '@/componentsPP/Avatar';

import ShortInfo from '../index';

describe('<ShortInfo />', () => {
  let props;
  beforeEach(() => {
    props = {
      client: {
        name: 'name',
        address: {},
        owner: {},
      },
    }
  });
  it('should render correctly', () => {
    const wrapper = shallow(<ShortInfo {...props} />);

    expect(wrapper.find(Avatar).length).toBe(2);
  });

  it('should render correctly when data is valid', () => {
    props = {
      ...props,
      client: {
        ...props.client,
        address: { city: 'city' },
        owner: { id: 'id' },
      },
      logo: 'logo',
    };

    const wrapper = shallow(<ShortInfo {...props} />);

    expect(wrapper.find(Avatar).length).toBe(2);
  });
});
