import globalMessages from '@/messages';

export default {
  name: globalMessages.common.name,
  slaTerms: globalMessages.contract.slaTerms,
  startDate: globalMessages.contract.startDate,
  endDate: globalMessages.contract.endDate,
  leadTime: globalMessages.contract.leadTime,
};
