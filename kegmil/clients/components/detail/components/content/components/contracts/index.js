import React, { PureComponent } from 'react';
import { connect } from 'dva';
import withRouter from 'umi/withRouter';

import { PAGE_SIZE } from '@/constants';

import ContractsView from './view';
import { NAMESPACE } from './constants';

@withRouter
@connect((state) => ({
  contracts: state[NAMESPACE].contracts,
  totalPage: state[NAMESPACE].totalPage,
  loading: state.loading.effects[`${NAMESPACE}/fetch`],
}))
class ContractsContainer extends PureComponent {

  state = {
    selectedRows: [],
    visibleModal: null,
  }

  componentDidMount() {
    this.handleRefreshContracts();
  }

  getIDFromProps = () => {
    const { match: { params: { id } } } = this.props;
    return id;
  }
  
  handleRefreshContracts = (page = 1, sorter) => {
    const { dispatch } = this.props;
    const id = this.getIDFromProps();
    dispatch({
      type: `${NAMESPACE}/fetch`,
      payload: {
        id,
        page,
        sorter,
        size: PAGE_SIZE,
      },
    });
  }

  handleSelectRow = selectedRows => this.setState({
    selectedRows,
  });

  handleRefreshData = () => {
    this.handleRefreshContracts();
    const { onFetchQuantity } = this.props;
    onFetchQuantity();
  }

  handleOpenModal = modalType => () => this.setState({ visibleModal: modalType });

  handleCloseModal = () => this.setState({ visibleModal: null });

  handleDelete = (id) => {
    const { onFetchQuantity, dispatch } = this.props;
    const onSuccessCallback = () => {
      this.setState({
        selectedRows: [],
      }, () => {
        onFetchQuantity();
        this.handleRefreshContracts();
      });
    }
    const { id: clientId } = this.props;
    dispatch({
      type: `${NAMESPACE}/delete`,
      payload: {
        id,
        clientId,
        callback: onSuccessCallback,
      },
    });
  }

  render() {
    const { loading, contracts, totalPage } = this.props;
    const id = this.getIDFromProps();
    const { selectedRows, visibleModal } = this.state;
    return (
      <ContractsView
        loading={loading}
        contracts={contracts}
        onRefreshContracts={this.handleRefreshContracts}
        totalPage={totalPage}
        onSelectRow={this.handleSelectRow}
        selectedRows={selectedRows}
        visibleModal={visibleModal}
        onCloseModal={this.handleCloseModal}
        onOpenModal={this.handleOpenModal}
        onSuccessCallback={this.handleRefreshData}
        clientId={id}
        onDelete={this.handleDelete}
      />
    );
  }
}

export default ContractsContainer;
export { default as model } from './model';
