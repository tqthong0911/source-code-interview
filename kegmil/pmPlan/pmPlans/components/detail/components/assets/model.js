import { get } from 'lodash';
import { getAssetDetail } from './service';
import { NAMESPACE } from './constants';

const model = {
  namespace: NAMESPACE,

  state: {
    assets: [],
  },

  effects: {
    *getListAssetDetail({ assetIds }, { call, all, put }) {
      const assetDetailPromises = assetIds.map(assetId => {
        return call(getAssetDetail, assetId);
      });
      const response = yield all(assetDetailPromises);
      if (response) {
        yield put({
          type: 'saveListAssetDetail',
          payload: response,
        });
      }
    },
  },
  reducers: {
    saveListAssetDetail(state, { payload }) {
      const assets = payload.map(item => get(item, 'results.asset', {}));
      return {
        ...state,
        assets,
      }
    },
  },
};

export default model;
