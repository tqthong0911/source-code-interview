import { get } from 'lodash';

import { getJobType } from '@/componentsPP/_share/PmPlanModal/service';
import { deletePMPlan } from '@/utils/pmPlan';

import { getPMPlans } from './service';
import { NAMESPACE } from './constants';

const model = {
  namespace: NAMESPACE,

  state: {
    pmPlans: [],
    jobsType: [],
    totalPage: 0,
    totalItem: '0',
  },

  effects: {
    *fetch({ currentPage, sort }, { call, all, put }) {
      const [pmPlansResponse, jobsTypeResponse] = yield all([
        call(getPMPlans, { sort }, currentPage),
        call(getJobType),
      ]);
      if (pmPlansResponse || jobsTypeResponse) {
        yield put({
          type: 'save',
          payload: {
            jobsType: get(jobsTypeResponse, 'results.jobType', []),
            pmPlans: get(pmPlansResponse, 'results.pmPlans', []),
            totalPage: get(pmPlansResponse, 'results.totalPage', 0),
            totalItem: get(pmPlansResponse, 'results.totalItem', '0'),
          },
        });
      }
    },
    *delete({ payload }, { call }) {
      const { id, isDeleteAllJobs, callback } = payload;
      yield call(deletePMPlan, id, isDeleteAllJobs);
      callback();
    },
  },

  reducers: {
    save(state, { payload: { jobsType, pmPlans, totalPage, totalItem } }) {
      return {
        ...state,
        jobsType,
        pmPlans,
        totalPage,
        totalItem,
      };
    },
    clear(state) {
      return {
        ...state,
        pmPlans: [],
        totalPage: 0,
      };
    },
  },
};

export default model;
