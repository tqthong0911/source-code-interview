import React from 'react';
import PropTypes from 'prop-types';
import { pathOr, isEmpty, path } from 'ramda';
import Link from 'umi/link';

import Avatar from '@/componentsPP/Avatar';
import { formatAddress } from '@/utils/utils';
import { getImageURL, getAvatarURL } from '@/utils/image';
import { DEFAULT_AVATAR } from '@/constants';

import Status from './Status';
import Styles from './styles.less';

const emptyNode = (<span className={Styles.empty}>--None--</span>);
const valueOrEmpty = pathOr(emptyNode);

const ShortInfo = (props) => {
  const { client } = props;
  const name = valueOrEmpty(['name'], client);
  const address = pathOr({}, ['address'], client);
  const addressString = isEmpty(address) ? emptyNode : formatAddress(address);
  const { logo } = client;
  const logoURL = logo ? getImageURL(logo) : null;
  const ownerID = path(['owner', 'id'], client);
  const ownerName = pathOr(emptyNode, ['owner', 'name'], client);
  const ownerAvatarURL = ownerID ? getAvatarURL(ownerID) : null;
  return (
    <div className={Styles.wrapper}>
      <Avatar
        shape="square"
        size={104}
        src={logoURL}
      >
        <i className={`material-icons ${Styles.icon}`}>domain</i>
      </Avatar>
      <div className={Styles.textInfo}>
        <div className={Styles.clientName}>{name}</div>
        <div className={Styles.clientAddress}>{addressString}</div>
        <div className={Styles.shortInfo}>
          <Status client={client} className={Styles.status} />
          <div className={Styles.clientOwner}>
            <Avatar
              icon="user"
              size={32}
              fallbackSrc={DEFAULT_AVATAR}
              src={ownerAvatarURL}
            />
            <Link to='/people' className={Styles.ownerName}>{ownerName}</Link>
          </div>
        </div>
      </div>
    </div>
  );
}

ShortInfo.propTypes = {
  client: PropTypes.object,
}

ShortInfo.defaultProps = {
  client: {},
}

export default ShortInfo;
