import { get, parseInt, isObject, has } from 'lodash';
import moment from 'moment';

import request from '@/utils/request';
import { trimString } from '@/utils/utils';
import { PAGE_BIG_SIZE, SORT_ORDER } from '@/constants';
import { buildFilterParam } from '@/utils/client';
import { fetchClientContracts } from '@/utils/contract';
import { PMPLAN_FORM } from './constants';

export async function create(pmPlan) {
  const postData = mapToPostData(pmPlan);
  return request('/job/contract/pmplans', {
    method: 'POST',
    body: postData,
  });
}

export async function update(pmPlan) {
  const postData = mapToPostData(pmPlan);
  return request('/job/contract/pmplans', {
    method: 'PUT',
    body: postData,
  });
}

export async function getDetail(id) {
  return request(`/job/contract/pmplans/${id}`);
}

export async function getAssetByClient(clientId) {
  const sort = buildFilterParam({ sort: { order: SORT_ORDER.ascend, field: 'name' } });
  return request(`/assets?page=1&client_id=${clientId}&incl_subs=true&size=${PAGE_BIG_SIZE}${sort}`);
}

export async function getClients() {
  const sort = buildFilterParam({ sort: { order: SORT_ORDER.ascend, field: 'name' } });
  return request(`/client?page=1&size=${PAGE_BIG_SIZE}${sort}`);
}

export async function getJobType() {
  const sort = buildFilterParam({ sort: { order: SORT_ORDER.ascend, field: 'name' } });
  return request(`/jobs/jobTypes?page=1&size=${PAGE_BIG_SIZE}${sort}`);
}

export function mapToFormData(pmPlan) {
  const pmPlanInfo = {};
  Object.keys(pmPlan).forEach(key => {
    const value = trimString(get(pmPlan, key));
    if (value) {
      if (key === PMPLAN_FORM.firstDate || key === PMPLAN_FORM.endDate || key === PMPLAN_FORM.startDate) {
        pmPlanInfo[key] = { value: moment(parseInt(value)) };
      } else if (key === PMPLAN_FORM.assets) {
        pmPlanInfo[key] = value.reduce((acc, { id, genTask = false }) => {
          return {
            ...acc,
            [id]: {
              id,
              shouldGenerateTask: genTask,
            },
          }
        }, {})
      } else {
        pmPlanInfo[key] = { value };
      }
    }
  })
  return pmPlanInfo;
}

export function mapToPostData(pmPlan) {
  const newValues = {};
  Object.keys(pmPlan).forEach(key => {
    const value = trimString(get(pmPlan, key));
    if (value) {
      if (key === PMPLAN_FORM.frequency || key === PMPLAN_FORM.genTimeframe || key === PMPLAN_FORM.genHorizon) {
        newValues[key] = parseInt(value);
      } else if (key === PMPLAN_FORM.firstDate || key === PMPLAN_FORM.endDate || key === PMPLAN_FORM.startDate) {
        newValues[key] = value.valueOf().toString();
      } else if (key === PMPLAN_FORM.assets) {
        newValues[key] = Object.keys(value).reduce((acc, id) => {
          return [ ...acc, {
            id: value[id].id,
            genTask: value[id].shouldGenerateTask,
          } ];
        }, []);
      } else {
        newValues[key] = value;
      }
    }
  })
  return newValues;
}

export function valueComparator(objValue, othValue) {
  if (isObject(objValue) && isObject(othValue) && has(objValue, 'value') && has(othValue, 'value')) {
    return objValue.value === othValue.value;
  }
  return undefined;
}

const clientContractsFetcher = (parrams) => async(id) => {
  const fetcher = fetchClientContracts(`/client/${id}/contracts`)(parrams);
  const response = await fetcher();
  const contracts = get(response, 'results.contracts', []);
  return contracts;
}

export const fetchContracts = clientContractsFetcher({
  page: 1,
  size: PAGE_BIG_SIZE,
});
