import React from 'react';
import { shallow } from 'enzyme';

import ClientContainer from '../index';
import ClientView from '../view';
import { NAMESPACE } from '../constants';
import { SORT_ORDER } from '@/constants';

describe('<ClientContainer />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      clients: [1, 2].map(value => {
        return { id: value, company: { name: `Client ${value}` } };
      }),
      filter: { sort: { order: SORT_ORDER.ascend, field: 'name', columnKey: 'name' } },
      currentPage: 1,
      newClient: {},
      totalPage: 2,
      totalItem: 10,
      loading: false,
      isClientDeleted: false,
      dispatch: jest.fn(),
      handleChange: jest.fn(),
    };
    wrapper = shallow(<ClientContainer.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(ClientView).length).toBe(1);
  });

  it('should call fetch client list in componentDidMount', () => {
    wrapperInstance.componentDidMount();
    const currentPage = 1;
    const filter = { sort: { order: SORT_ORDER.ascend, field: 'name', columnKey: 'name' } };
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetch`, currentPage, filter });
  });

  describe('componentWillReceiveProps', () => {
    it('should call handleReloadPage when it adds a new client', () => {
      wrapperInstance.handleReloadPage = jest.fn();
      wrapper.setProps({ newClient: { name: 'test' } });
      expect(wrapperInstance.handleReloadPage).toHaveBeenCalled();
    });

    it('should call fetch client list when it delete a client', () => {
      wrapperInstance.handleReloadPage = jest.fn();
      wrapper.setProps({ isClientDeleted: true });
      expect(wrapperInstance.handleReloadPage).toHaveBeenCalled();
    });
  });

  it('should call fetch client list by current page when it calls handleChange', () => {
    const currentPage = 2;
    const filter = { sort: { order: SORT_ORDER.ascend, field: 'name', columnKey: 'name' } };
    wrapperInstance.handleChange(filter, currentPage);

    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetch`,
      currentPage,
      filter,
    });
  });

  it('should fetch client list by current page when it calls handleReloadPage', () => {
    const currentPage = 1;
    const filter = { sort: { order: SORT_ORDER.ascend, field: 'name', columnKey: 'name' } };
    wrapperInstance.currentPage = currentPage;
    wrapperInstance.filter = filter;

    wrapperInstance.handleChange = jest.fn();
    wrapperInstance.handleReloadPage();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetch`,
      currentPage,
      filter,
    });
  });

  it('should call clear data in componentWillUnmount', () => {
    wrapperInstance.componentWillUnmount();
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/clear` });
  });

  it('should call delete first client select in handleDelete', () => {
    const selectedClients = [{ id: 1 }, { id: 2 }];
    const callback = () => {};
    wrapperInstance.handleDelete(selectedClients, callback);
    expect(props.dispatch).toHaveBeenCalled();
  });

  it('should refesh View After Delete when it calls refeshViewAfterDelete', () => {
    wrapperInstance.currentPage = 1;
    wrapperInstance.refeshViewAfterDelete();
    expect(wrapperInstance.currentPage).toEqual(1);
  });

});
