import globalMessages from '@/messages';

export default {
  deleteContent: globalMessages.common.deleteContent,
  deleteMultiContent: globalMessages.common.deleteMultiContent,
  deleteAsset: globalMessages.asset.deleteAsset,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  edit: globalMessages.common.edit,
  selected: globalMessages.common.selected,
  name: globalMessages.common.name,
  id: globalMessages.common.id,
  status: globalMessages.asset.status,
  assets: globalMessages.asset.assets,
};
