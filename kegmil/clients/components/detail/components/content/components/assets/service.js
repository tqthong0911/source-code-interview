import requestCustomField from '@/utils/customField/requestCustomField';

export async function getAssetDetail(id) {
  return requestCustomField(`/assets/${id}`);
}
