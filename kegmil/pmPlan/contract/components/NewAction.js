import React, { PureComponent, Fragment } from 'react';
import { formatMessage } from 'umi/locale';
import { Menu } from '@/componentsPP/UIElements';
import DropDownMenuButton from '@/componentsPP/DropdownMenuButton';

import ContractModal from '@/componentsPP/_share/ContractModal';
import ImportModal, { IMPORT_TYPE } from '@/componentsPP/_share/ImportModal';
import { MODAL_TYPE } from '../constants';

import messages from '../messages';
import Styles from './NewAction.less';

class NewActionContainer extends PureComponent {
  state = {
    isOpen: false,
    modelType: '',
  };

  toggleNew = () => this.setState(({ isOpen }) => ({ isOpen: !isOpen }));

  handleCancel = (callback) => {
    this.setState({ isOpen: false }, () => {
      if (callback) callback();
    });
  };

  cancelContractImport = () => {
    this.setState({ modelType: '' });
  };

  onItemClick = item => {
    this.setState({ modelType: item.key });
  };

  render() {
    const { isOpen, modelType } = this.state;
    const { handleReloadContract } = this.props;

    const isContractImportOpen = modelType === MODAL_TYPE.import;

    const menu = (
      <Menu onClick={this.onItemClick}>
        <Menu.Item key={MODAL_TYPE.import}>
          <i className={`material-icons ${Styles.itemIcon}`}>vertical_align_top</i>
          {formatMessage(messages.importContract)}
        </Menu.Item>
      </Menu>
    );

    return (
      <Fragment>
        <div className={Styles.wrapper}>
          <div className={Styles.leftAction}>
            <DropDownMenuButton
              btn={{
                onClick: this.toggleNew,
                childrenContent: formatMessage(messages.new),
              }}
              menu={menu}
            />
          </div>
          <ContractModal
            isOpen={isOpen}
            handleCancel={this.handleCancel}
            callback={handleReloadContract}
          />
          <ImportModal
            type={IMPORT_TYPE.contract}
            title={formatMessage(messages.importContract)}
            description={formatMessage(messages.importContractDescription)}
            isOpen={isContractImportOpen}
            handleCancel={this.cancelContractImport}
          />
        </div>
      </Fragment>
    );
  }
}

export default NewActionContainer;
