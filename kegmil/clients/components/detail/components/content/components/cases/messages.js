import globalMessages from '@/messages';

export default {
  delete: globalMessages.common.delete,
  edit: globalMessages.common.edit,
  cancel: globalMessages.common.cancel,
  deleteCase: globalMessages.case.deleteCase,
  deleteContent: globalMessages.common.deleteContent,
  deleteMultiContent: globalMessages.common.deleteMultiContent,

  caseNumber: globalMessages.case.caseNumber,
  caseName: globalMessages.case.caseName,
  type: globalMessages.case.type,
  dueDate: globalMessages.case.dueDate,
  status: globalMessages.case.status,
  priority: globalMessages.case.priority,
  selectPlaceholder: globalMessages.form.selectPlaceholder,
};
