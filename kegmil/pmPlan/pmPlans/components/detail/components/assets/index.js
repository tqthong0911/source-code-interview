import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import { isEqual, get } from 'lodash';
import { formatMessage } from 'umi/locale';
import { Col, Row, Tabs } from '@/componentsPP/UIElements';
import AssetModal, { NAMESPACE as ASSET_MODAL_NAMESPACE } from '@/componentsPP/_share/AssetModal';
import DropdownMenu from '@/componentsPP/DropdownMenu';
import IconButton from '@/componentsPP/IconButton';
import deleteModal from '@/componentsPP/DeleteModal';
import { PP_FONT_MEDIUM, CONFIRM_MODAL_TYPE } from '@/constants';
import OpenAssets from './components/open';
import { NAMESPACE as PMPLAN_NAMESPACE } from '../../../../constants';
import { NAMESPACE } from './constants';
import messages from './messages';
import Styles from './styles.less';

const { TabPane } = Tabs;

@connect(state => ({
  assets: state[NAMESPACE].assets,
  changedIndexPMPlan: state[PMPLAN_NAMESPACE].changedIndex,
  modelAssetFinish: state[ASSET_MODAL_NAMESPACE].changedKey,
  loadingAssets: state.loading.effects[`${NAMESPACE}/getListAssetDetail`],
}))
class Assets extends PureComponent {
  static propTypes = {
    pmPlan: PropTypes.object.isRequired,
    assetIds: PropTypes.array.isRequired,
    initData: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,

    assets: PropTypes.array.isRequired,
    modelAssetFinish: PropTypes.number.isRequired,
    changedIndexPMPlan: PropTypes.number,
    loadingAssets: PropTypes.bool,
  };

  static defaultProps = {
    loadingAssets: false,
    changedIndexPMPlan: 0,
  };

  state = {
    selectedRows: [],
    isOpenAssetModal: false,
  }

  componentDidMount() {
    this.getListAssetDetail();
  }

  componentDidUpdate(prevProps) {
    const { assetIds, modelAssetFinish, changedIndexPMPlan } = this.props;
    if (
      !isEqual(prevProps.assetIds, assetIds) ||
      !isEqual(prevProps.modelAssetFinish, modelAssetFinish) ||
      !isEqual(prevProps.changedIndexPMPlan, changedIndexPMPlan)
    ) {
      this.getListAssetDetail();
    }
  }

  getListAssetDetail = () => {
    const { dispatch, assetIds } = this.props;
    dispatch({
      type: `${NAMESPACE}/getListAssetDetail`,
      assetIds,
    });
    this.setState({ selectedRows: [] });
  };

  handleDeleteAsset = (assetIds, closeModal) => {
    if (assetIds) {
      const { pmPlan, actions: { handleChangeAsset } } = this.props;
      const newAssets = pmPlan.assets.filter(({ id }) => {
        return assetIds.indexOf(id) < 0;
      });
      handleChangeAsset(newAssets);
      closeModal();
    }
  };

  handleEdit = (e) => {
    e.stopPropagation();
    this.setState({ isOpenAssetModal: true });
  };

  handleCancelEditAsset = () => {
    this.setState({ isOpenAssetModal: false });
  };

  handleDelete = (e) => {
    e.stopPropagation();
    const { selectedRows } = this.state;

    const renderContent = () => {
      if (selectedRows.length === 1) {
        return (
          <span>
            {`${formatMessage(messages.deleteContent)} `}
            <a>{selectedRows[0].name}</a>
            {`?`}
          </span>
        )
      }
      return <span>{formatMessage(messages.deleteMultiContent)}</span>
    };

    if (selectedRows && selectedRows.length > 0) {
      deleteModal({
        type: CONFIRM_MODAL_TYPE.delete,
        title: formatMessage(messages.deleteAsset),
        content: renderContent(),
        okText: formatMessage(messages.delete),
        cancelText: formatMessage(messages.cancel),
        onOk: (closeModal) => {
          return new Promise(
            () => this.handleDeleteAsset(selectedRows.map(select => select.id), closeModal)
          ).catch(() => { });
        },
        onCancel() { },
      });
    }
  };

  handleSelectRow = (newSelectedRows) => {
    this.setState({ selectedRows: newSelectedRows });
  }

  renderActionButton = () => {
    const { selectedRows } = this.state;
    const style = selectedRows.length <= 0 ? { display: 'none' } : {};
    const shouldDisplayEditIcon = selectedRows.length === 1;

    return (
      <div className={Styles.headerWrapper}>
        <DropdownMenu>
          <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
          <i className="material-icons">arrow_drop_down</i>
        </DropdownMenu>
        <div className={Styles.actions} style={style}>
          <span>{`${selectedRows.length} ${formatMessage(messages.selected)}`}</span>
          {
            shouldDisplayEditIcon && (
              <IconButton
                icon="edit"
                onClick={this.handleEdit}
                tooltip={{ title: formatMessage(messages.edit) }}
              />
            )
          }
          <IconButton
            icon="delete"
            onClick={this.handleDelete}
            tooltip={{ title: formatMessage(messages.delete) }}
          />
        </div>
      </div>
    )
  }

  render() {
    const {
      loadingAssets,
      assets,
      initData: {
        commonLocations,
      },
      ...rest
    } = this.props;
    const { selectedRows, isOpenAssetModal } = this.state;
    const assetIdSelected = get(selectedRows, '0.id', '');
    return (
      <Row justify="space-between">
        <Col>
          {this.renderActionButton()}
          <Tabs defaultActiveKey="1" type="card" onChange={this.handleTabChanged} className={Styles.tabWrapper}>
            <TabPane tab={formatMessage(messages.assets)} key="1" className={Styles.tabPane} forceRender>
              <Fragment>
                <OpenAssets
                  panelProp={rest}
                  assets={assets}
                  loadingAssets={loadingAssets}
                  selectedRows={selectedRows}
                  handleSelectRow={this.handleSelectRow}
                />
                <AssetModal
                  commonLocations={commonLocations}
                  assetId={assetIdSelected}
                  isOpen={isOpenAssetModal}
                  handleCancel={this.handleCancelEditAsset}
                />
              </Fragment>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    );
  }
}

export default Assets;
