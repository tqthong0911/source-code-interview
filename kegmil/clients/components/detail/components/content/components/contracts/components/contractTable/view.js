import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { pathOr, compose, isNil } from 'ramda';
import { Tag } from '@/componentsPP/UIElements';
import StandardTable from '@/componentsPP/StandardTable';
import Ellipsis from '@/componentsPP/Ellipsis';
import { PAGE_SIZE, SORT_ORDER, EMPTY_VALUE, PP_FONT_MEDIUM, DATE_TIME_FORMAT } from '@/constants';
import messages from './messages';
import Styles from './styles.less';

function renderColumn(value = EMPTY_VALUE) {
  return (
    <Ellipsis lines="1" tooltip={value}>
      {value}
    </Ellipsis>
  );
}

function parseDateTimeValue(value) {
  const columnValue = isNil(value) ? EMPTY_VALUE : moment(parseInt(value, 10)).format(DATE_TIME_FORMAT);
  return columnValue;
}

function renderSlaTerms(value) {
  const columnValue = pathOr(EMPTY_VALUE, ['0'], value);
  return (
    <Tag className={Styles.tag}>
      <Ellipsis
        lines="1"
        tooltip={columnValue}
        className={PP_FONT_MEDIUM}
      >
        {columnValue}
      </Ellipsis>
    </Tag>
  );
}

function getColumnConfig() {
  const dateTimeColumnRenderer = compose(renderColumn, parseDateTimeValue);
  return [
    {
      title: formatMessage(messages.name),
      dataIndex: 'name',
      key: 'name',
      width: '30%',
      sorter: true,
      render: renderColumn,
      defaultSortOrder: SORT_ORDER.ascend,
    },
    {
      title: formatMessage(messages.slaTerms),
      dataIndex: 'terms',
      key: 'terms',
      width: '15%',
      sorter: true,
      render: renderSlaTerms,
    },
    {
      title: formatMessage(messages.startDate),
      dataIndex: 'startDate',
      key: 'startDate',
      width: '20%',
      sorter: true,
      render: dateTimeColumnRenderer,
    },
    {
      title: formatMessage(messages.endDate),
      dataIndex: 'endDate',
      key: 'endDate',
      width: '20%',
      sorter: true,
      render: dateTimeColumnRenderer,
    },
    {
      title: formatMessage(messages.leadTime),
      dataIndex: 'leadTimes',
      key: 'leadTimes',
      width: '15%',
      sorter: true,
      render: renderColumn,
    },
  ];
}

function paginationConfig(props) {
  const { totalPage, page, onPageChange } = props;
  const allItems = totalPage * PAGE_SIZE;

  return {
    simple: true,
    total: allItems,
    current: page,
    pageSize: PAGE_SIZE,
    onChange: onPageChange,
  };
}

const ContractTableView = (props) => {
  const { contracts, loading, onChange, selectedRows, onSelectRow } = props;
  return (
    <StandardTable
      loading={loading}
      columns={getColumnConfig(props)}
      onChange={onChange}
      selectedRows={selectedRows}
      onSelectRow={onSelectRow}
      data={{
        list: contracts,
        pagination: paginationConfig(props),
      }}
      rowKey={({ id }) => id}
      className={Styles.contractTable}
    />
  );
}

ContractTableView.propTypes = {
  contracts: PropTypes.array,
  loading: PropTypes.bool,
  selectedRows: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  onSelectRow: PropTypes.func.isRequired,
}

ContractTableView.defaultProps = {
  contracts: [],
  loading: false,
  selectedRows: [],
}

export default ContractTableView;
