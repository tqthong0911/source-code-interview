import { isEmpty, isNil } from 'ramda';

import { deleteContract } from '@/pages/pmPlan/contract/service';

import { NAMESPACE } from './constants';
import { fetchContracts } from './services';

const SORT_ORDER_MAP = Object.freeze({
  ascend: 'asc',
  descend: 'desc',
});

const initialState = {
  contracts: [],
  totalPage: 0,
}

export default {
  namespace: NAMESPACE,

  state: initialState,

  effects: {
    *fetch({ payload: { id, page, sorter, size } }, { call, put }) {
      const sort = isNil(sorter) || isEmpty(sorter) ? undefined : `${sorter.field}:${SORT_ORDER_MAP[sorter.order]}`;
      const { totalPage, contracts } = yield call(fetchContracts, id, { page, sort, size });
      yield put({
        type: 'save',
        payload: {
          totalPage,
          contracts,
        },
      });
    },
    *delete({ payload }, { call }) {
      const { id, clientId, callback } = payload;
      yield call(deleteContract, id, clientId);
      callback();
    },
  },

  reducers: {
    save: (state, { payload: { contracts, totalPage } }) => ({
      ...state,
      totalPage,
      contracts,
    }),
    clear: () => initialState,
  },
}
