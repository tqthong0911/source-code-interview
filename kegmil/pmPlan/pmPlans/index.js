import React, { PureComponent } from 'react';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { SORT_ORDER } from '@/constants';
import PMPlansView from './view';
import { NAMESPACE } from './constants';

@connect(state => ({
  pmPlans: state[NAMESPACE].pmPlans,
  totalPage: state[NAMESPACE].totalPage,
  totalItem: state[NAMESPACE].totalItem,
  jobsType: state[NAMESPACE].jobsType,
  loading: state.loading.effects[`${NAMESPACE}/fetch`],
}))
class PMPlansContainer extends PureComponent {
  static propTypes = {
    pmPlans: PropTypes.array.isRequired,
    jobsType: PropTypes.array.isRequired,
    totalPage: PropTypes.number.isRequired,
    totalItem: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
  };

  currentPage = 1;

  sort = { order: SORT_ORDER.ascend, field: 'name' };

  componentDidMount() {
    this.clearBreadcrumb();
    this.fetchPMPlan();
  };

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  };

  clearBreadcrumb = () => {
    const { dispatch } = this.props;
    dispatch({ type: 'breadcrumb/clear' });
  };

  fetchPMPlan = () => {
    const { props: { dispatch }, currentPage, sort } = this;
    dispatch({
      type: `${NAMESPACE}/fetch`,
      currentPage,
      sort,
    });
  };

  handleChangeFilterTableView = (sorter, pageIndex) => {
    this.sort = sorter;
    this.currentPage = pageIndex;
    this.fetchPMPlan();
  };

  refeshViewAfterDelete = () => {
    const { props: { pmPlans }, currentPage } = this;
    const isLatestUserInPage = (pmPlans.length === 1 && currentPage !== 1);
    this.currentPage = isLatestUserInPage ? (currentPage - 1) : currentPage;
    this.fetchPMPlan();
  };

  handleDelete = (selectedPMPlans, isDeleteAllJobs, closeModal) => {
    if (!isEmpty(selectedPMPlans)) {
      const { props: { dispatch }, refeshViewAfterDelete } = this;
      dispatch({
        type: `${NAMESPACE}/delete`,
        payload: {
          id: selectedPMPlans[0].id,
          isDeleteAllJobs,
          callback: () => {
            closeModal();
            refeshViewAfterDelete();
          },
        },
      });
    }
  };

  render() {
    const { pmPlans, totalPage, totalItem, jobsType, loading } = this.props;

    return (
      <PMPlansView
        pmPlans={pmPlans}
        totalPage={totalPage}
        totalItem={totalItem}
        currentPage={this.currentPage}
        jobsType={jobsType}
        loading={loading}
        handleDelete={this.handleDelete}
        handleReloadPMPlan={this.fetchPMPlan}
        fetchPMPlan={this.fetchPMPlan}
        handleChangeFilterTableView={this.handleChangeFilterTableView}
      />
    );
  }
}

export default PMPlansContainer;
