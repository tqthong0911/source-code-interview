import React from 'react';
import { shallow } from 'enzyme';
import ContractsContainer from '..';
import ContractsView from '../view';

describe('<ContractsContainer />', () => {
  let props;
  let wrapper;
  let wrappedInstance;
  beforeEach(() => {
    props = {
      contracts: [],
      totalPage: 0,
      loading: false,
      id: 1,
      dispatch: jest.fn(),
      match: { params: { id: 'id' } },
    };
    wrapper = shallow(<ContractsContainer.WrappedComponent {...props} />);
    wrappedInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(ContractsView).length).toBe(1);
    expect(props.dispatch).toBeCalled();
  });

  it('should call onRefreshContracts when it calls handleRefreshContracts', () => {
    wrappedInstance.handleRefreshContracts(1, {});
    
    expect(props.dispatch).toBeCalled();
  });
});
