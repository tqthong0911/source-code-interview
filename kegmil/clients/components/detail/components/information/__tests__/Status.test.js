import React from 'react';
import * as umiLocale from 'umi/locale';
import { shallow } from 'enzyme';

import { Dropdown } from '@/componentsPP/UIElements';

import Status from '../Status';

umiLocale.formatMessage = jest.fn(() => 'message');

describe('<Status />', () => {
  let props;
  beforeEach(() => {
    props = {
      client: { active: 0 },
    };
  });
  it('should render correctly when client is active', () => {
    const wrapper = shallow(<Status {...props} />);

    expect(wrapper.find(Dropdown).length).toBe(1);
  });
  it('should render correctly when client is active', () => {
    props = {
      ...props,
      client: { active: 1 },
    };

    const wrapper = shallow(<Status {...props} />);

    expect(wrapper.find(Dropdown).length).toBe(1);
  });
});
