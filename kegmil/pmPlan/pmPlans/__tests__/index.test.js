import React from 'react';
import { shallow } from 'enzyme';

import PMPlansContainer from '../index';
import PMPlansView from '../view';
import { NAMESPACE } from '../constants';

describe('<PMPlansContainer />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      jobsTypes: [1, 2].map(value => ({
        id: `${value}`,
        name: `JobType ${value}`,
      })),
      pmPlans: [1, 2].map(value => ({
        id: `${value}`,
        title: `Job ${value}`,
      })),
      totalPage: 2,
      dispatch: jest.fn(),
      loading: false,

    };
    wrapper = shallow(<PMPlansContainer.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(PMPlansView).length).toBe(1);
  });

  it('should fetch pmPlans list and init data in componentDidMount', () => {
    wrapperInstance.componentDidMount();
    const { sort, currentPage } = wrapperInstance;
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetch`, sort, currentPage });
  });

  it('should clear pmPlans list in componentWillUnmount', () => {
    wrapperInstance.componentWillUnmount();

    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/clear` });
  });

  it('should fetch pmPlans when it calls fetchPMPlan', () => {
    wrapperInstance.fetchPMPlan();
    const { sort, currentPage } = wrapperInstance;
    expect(props.dispatch).toHaveBeenCalledWith({ type: `${NAMESPACE}/fetch`, sort, currentPage });
  });

  it('should fetch pmPlans list by current page when it calls handleChangeFilterTableView', () => {
    const currentPage = 2;
    wrapperInstance.handleChangeFilterTableView({}, currentPage);

    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetch`,
      sort: {},
      currentPage,
    });
  });

  it('should fetch pmPlans when it calls fetchPMPlan', () => {
    wrapperInstance.fetchPMPlan = jest.fn();
    wrapperInstance.fetchPMPlan();

    expect(wrapperInstance.fetchPMPlan).toHaveBeenCalled();
  });

  it('should fetch pmPlans when it calls refeshViewAfterDelete', () => {
    wrapperInstance.fetchPMPlan = jest.fn();
    wrapperInstance.refeshViewAfterDelete();

    expect(wrapperInstance.fetchPMPlan).toHaveBeenCalled();
  });

  it('should handle delete correctly when selected PM Plan is not empty', () => {
    const seletedItems = [{ id: '1' }];

    wrapperInstance.handleDelete(seletedItems);

    expect(props.dispatch).toBeCalled();
  });
});
