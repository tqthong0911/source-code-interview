import React, { PureComponent, Fragment } from 'react';
import { formatMessage } from 'umi/locale';

import { Tabs, Button, Checkbox } from '@/componentsPP/UIElements';
import DropDownMenu from '@/componentsPP/DropdownMenu';
import IconButton from '@/componentsPP/IconButton';
import PmPlanModal from '@/componentsPP/_share/PmPlanModal';
import deleteModal from '@/componentsPP/DeleteModal';
import getValueFromEvent from '@/componentsPP/_utils/getValueFromEvent';
import { PP_FONT_MEDIUM, CONFIRM_MODAL_TYPE } from '@/constants';
import messages from '@/messages';

import PMPlanTable from './components/pmPlanTable';
import Styles from './styles.less';

const { TabPane } = Tabs;

class PMPlansView extends PureComponent {

  state = {
    shouldDeleteJob: false,
  }

  handleShouldDeleteJobChange = (e) => {
    const value = getValueFromEvent(e);
    this.setState({ shouldDeleteJob: value });
  }

  renderConfirmModalContent = () => {
    const { selectedRows } = this.props;
    const { name } = selectedRows[0];
    return (
      <Fragment>
        {formatMessage(messages.pmPlan.deletePMPlanContent)}
        <span className={Styles.pmPlanName}>{name}</span>
        ?
        <div>
          <Checkbox onChange={this.handleShouldDeleteJobChange}>
            {formatMessage(messages.pmPlan.deleteJobCreatedByPmPlan)}
          </Checkbox>
        </div>
      </Fragment>
    );
  }

  handleClickDelete = (e) => {
    e.preventDefault();
    const { selectedRows, onDelete } = this.props;
    const { id } = selectedRows[0];
    const callback = () => {
      this.setState({
        shouldDeleteJob: false,
      });
    }
    deleteModal({
      type: CONFIRM_MODAL_TYPE.delete,
      title: formatMessage(messages.pmPlan.deletePMPlanTitle),
      content: this.renderConfirmModalContent(),
      okText: formatMessage(messages.common.delete),
      okButtonProps: { type: 'danger' },
      onOk: () => {
        const { shouldDeleteJob } = this.state;
        onDelete({
          id,
          shouldDeleteJob,
          callback,
        });
      },
    });
  }

  handleClickEdit = () => {
    const { onOpenModal } = this.props;
    onOpenModal('edit')();
  }

  handleClickNew = () => {
    const { onOpenModal } = this.props;
    onOpenModal('new')();
  }

  render() {
    const {
      loading,
      pmPlans,
      onRefreshPMPlans,
      totalPage,
      jobTypes,
      selectedRows,
      onSelectRow,
      visibleModal,
      onCloseModal,
      clientID,
      onSuccessCallback,
    } = this.props;
    const style = selectedRows.length <= 0 ? { display: 'none' } : {};
    const shouldDisplayEditActions = selectedRows.length === 1;
    return (
      <div className={Styles.pmPlansWrapper}>
        <div className={Styles.headerWrapper}>
          <div className={Styles.leftActionWrapper}>
            <DropDownMenu>
              <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
              <i className="material-icons">arrow_drop_down</i>
            </DropDownMenu>
            <div className={Styles.actions} style={style}>
              <span className={Styles.selected}>{`${selectedRows.length} selected`}</span>
              {shouldDisplayEditActions && (
                <Fragment>
                  <IconButton
                    icon="edit"
                    onClick={this.handleClickEdit}
                  />
                  <IconButton
                    icon="delete"
                    onClick={this.handleClickDelete}
                  />
                  <PmPlanModal
                    isOpen={visibleModal === 'edit'}
                    pmPlanId={selectedRows[0].id}
                    handleCancel={onCloseModal}
                    callback={onSuccessCallback}
                  />
                </Fragment>
              )}
            </div>
            {selectedRows.length === 0 && (
              <Fragment>
                <PmPlanModal
                  isOpen={visibleModal === 'new'}
                  handleCancel={onCloseModal}
                  clientId={clientID}
                  callback={onSuccessCallback}
                />
                <Button
                  type="primary"
                  size="small"
                  className={Styles.newBtn}
                  onClick={this.handleClickNew}
                >
                  {formatMessage(messages.common.new)}
                </Button>
              </Fragment>
            )}
          </div>
        </div>
        <Tabs type="card">
          <TabPane key="pmPlans" tab="PM Plans" className={Styles.pmPlansTabWrapper}>
            <PMPlanTable
              loading={loading}
              pmPlans={pmPlans}
              onRefreshPMPlans={onRefreshPMPlans}
              totalPage={totalPage}
              jobTypes={jobTypes}
              selectedRows={selectedRows}
              onSelectRow={onSelectRow}
            />
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

export default PMPlansView;
