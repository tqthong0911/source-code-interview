import globalMessages from '@/messages';

export default {
  deleteContent: globalMessages.common.deleteContent,
  delete: globalMessages.common.delete,
  new: globalMessages.common.new,
  deleteContract: globalMessages.contract.deleteContract,
  contracts: globalMessages.contract.contracts,
};
