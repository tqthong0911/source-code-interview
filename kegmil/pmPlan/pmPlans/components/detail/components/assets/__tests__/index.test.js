import React from 'react';
import { shallow } from 'enzyme';
import Assets from '../';
import OpenAssets from '../components/open';

describe('<Assets />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      pmPlan: {
        id: '1',
        assets: [1, 2, 3].map(e => ({
          id: e,
          name: `${e}`,
        })),
      },
      assets: [1, 2, 3].map(e => ({
        id: e,
        name: `${e}`,
      })),
      assetIds: ['1', '2', '3'],
      changedIndexPMPlan: 0,
      modelAssetFinish: 0,
      initData: {
        commonLocations: [],
      },
      actions: {
        handleChangeAsset: jest.fn(),
      },
      dispatch: jest.fn(),
      loadingAssets: false,
    };
    wrapper = shallow(<Assets.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(OpenAssets).length).toBe(1);
  });

  it('should get list asset detail when it calls componentDidMount', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapperInstance.componentDidMount();

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should get list asset detail when it calls getListAssetDetail', () => {
    wrapper.setProps({ assetIds: [1] });
    wrapperInstance.getListAssetDetail();

    expect(props.dispatch).toHaveBeenCalled();
  });

  // it('should delete asset in pmPlan detail when call handleDeleteAsset method', () => {
  //   const assetIds = [1, 2];
  //   const closeModal = jest.fn();
  //   wrapperInstance.handleDeleteAsset(assetIds, closeModal);
  //   expect(props.handleChangeAsset).toHaveBeenCalled();
  // });

  it('should get list asset detail when it change assetIds in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ assetIds: [1] });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should edit asset finish when it change modelAssetFinish in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ modelAssetFinish: 1 });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should pmPlan detail change when it change changedIndexPMPlan in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ changedIndexPMPlan: 1 });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should show dialog Asset when it call handleEdit', () => {
    wrapperInstance.state.isOpenAssetModal = false;
    wrapperInstance.handleEdit({ stopPropagation: jest.fn() });

    expect(wrapperInstance.state.isOpenAssetModal).toBe(true);
  });

  it('should close dialog Asset when it call handleCancelEditAsset', () => {
    wrapperInstance.state.isOpenAssetModal = true;
    wrapperInstance.handleCancelEditAsset();

    expect(wrapperInstance.state.isOpenAssetModal).toBe(false);
  });

  it('should set new selectRow when it call handleSelectRow', () => {
    wrapperInstance.state.selectedRows = [];
    wrapperInstance.handleSelectRow([1, 2]);

    expect(wrapperInstance.state.selectedRows.length).toBe(2);
  });
});
