import React from 'react';
import PropTypes from 'prop-types';
import { pathOr, isEmpty } from 'ramda';

import { Row, Col } from '@/componentsPP/UIElements';
import Avatar from '@/componentsPP/Avatar';
import IconButton from '@/componentsPP/IconButton';
import deleteModal from '@/componentsPP/DeleteModal';
import { DEFAULT_AVATAR, CONFIRM_MODAL_TYPE } from '@/constants';
import { getAvatarURL, FILE_TYPES } from '@/utils/image';

import Styles from './ContactItem.less';

const emptyNode = (<span className={Styles.empty}>--None--</span>);

const getArrayOrEmpty = pathOr([]);

const valueOrEmpty = pathOr(emptyNode);

const onEditHandler = (props) => (e) => {
  e.preventDefault();
  const { contact, onEdit } = props;
  onEdit(contact);
}

const onDeleteHandler = (props) => (e) => {
  e.preventDefault();
  const { contact, onDelete } = props;
  const { id, firstName, lastName } = contact;
  deleteModal({
    type: CONFIRM_MODAL_TYPE.delete,
    title: 'Delete Contact',
    content: (
      <div>
        Are you sure you want to delete&nbsp;
        <span className={Styles.contactName}>{`${firstName} ${lastName}`}</span>
        ?
      </div>
    ),
    okText: 'Delete',
    onOk: () => onDelete(id),
  });
}

const ContactItem = (props) => {
  const { contact } = props;
  const { firstName, lastName, id } = contact;
  const phones = getArrayOrEmpty(['phones'], contact);
  const phoneNode = isEmpty(phones) ? null : (
    <a href={`tel:${phones[0].value}`}>{phones[0].value}</a>
  );
  const emails = getArrayOrEmpty(['emails'], contact);
  const emailNode = isEmpty(emails) ? null : (
    <a href={`mailto:${emails[0].value}`}>{emails[0].value}</a>
  );
  const avatarURL = getAvatarURL(id, FILE_TYPES.CONTACT);
  return (
    <div className={Styles.contactItemWrapper}>
      <Row gutter={16}>
        <Col md={9}>
          <div className={Styles.shortInfo}>
            <Avatar fallbackSrc={DEFAULT_AVATAR} size={60} src={avatarURL} icon="user" />
            <div className={Styles.contactInfo}>
              <span className={Styles.name}>{`${firstName} ${lastName}`}</span>
              <span className={Styles.title}>{valueOrEmpty(['title'], contact)}</span>
            </div>
          </div>
        </Col>
        <Col md={9}>
          <div className={Styles.contacts}>
            <div>{phoneNode}</div>
            <div>{emailNode}</div>
          </div>
        </Col>
        <Col md={6} className={Styles.contactAction}>
          <div className={Styles.actionContainer}>
            <IconButton icon="edit" className={Styles.action} onClick={onEditHandler(props)} />
            <IconButton icon="delete" className={Styles.action} onClick={onDeleteHandler(props)} />
            <IconButton icon="more_horiz" className={Styles.action} />
          </div>
        </Col>
      </Row>
    </div>
  );
}

ContactItem.propTypes = {
  contact: PropTypes.object,
}

ContactItem.defaultProps = {
  contact: {},
}

export default ContactItem;
