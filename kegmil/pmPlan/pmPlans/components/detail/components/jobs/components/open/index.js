import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { formatMessage } from 'umi/locale';
import { isEmpty, isEqual } from 'lodash';
import { Select, Tag } from '@/componentsPP/UIElements';
import Ellipsis from '@/componentsPP/Ellipsis';
import StandardTable from '@/componentsPP/StandardTable';
import { renderTextColumn, renderOptionText } from '@/utils/layout/renderTable';
import { JOB_PRIORITY } from '@/utils/job';
import { getNextStatuses } from '@/utils/job/service';
import { convertJobsToDataView } from '@/utils/job/jobView';
import { PAGE_SIZE, PP_FONT_SEMIBOLD, PP_FONT_MEDIUM, PP_TEXT_UPPERCASE } from '@/constants';
import messages from '../../messages';
import Styles from './styles.less';

const { Option } = Select;

class OpenJob extends PureComponent {
  static propType = {
    loadingJobs: PropTypes.bool.isRequired,
    totalItem: PropTypes.number.isRequired,
    totalPage: PropTypes.number.isRequired,
    jobTypes: PropTypes.array.isRequired,
    accessedStatuses: PropTypes.array.isRequired,
    selectedRows: PropTypes.array.isRequired,
    jobs: PropTypes.array.isRequired,
    priorityDS: PropTypes.array,
    handleSelectRow: PropTypes.func.isRequired,
    handleSortPage: PropTypes.func.isRequired,
    fetchData: PropTypes.func.isRequired,
    updateJob: PropTypes.func.isRequired,
  };

  static defaultProps = {
    priorityDS: [
      { value: 'Low', name: 'Low' },
      { value: 'Medium', name: 'Medium' },
      { value: 'High', name: 'High' },
    ],
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    const { jobs, jobTypes } = nextProps;

    const shouldUpdateJobs = !isEqual(jobs, nextState.props.jobs) ||
      !isEqual(jobTypes, nextState.props.jobTypes);

    if (shouldUpdateJobs) {
      return {
        props: {
          jobs,
          jobTypes,
        },
        dataView: convertJobsToDataView(jobs, jobTypes),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    const { jobs, jobTypes } = this.props;
    this.state = {
      props: {
        jobs,
        jobTypes,
      },
      dataView: [],
      currentPage: 1,
    };
  }

  get paginationConfig() {
    const { totalPage } = this.props;
    const { currentPage } = this.state;
    const allItems = totalPage * PAGE_SIZE;

    return {
      simple: true,
      total: allItems,
      current: currentPage,
      pageSize: PAGE_SIZE,
      onChange: this.handlePagination,
    };
  }

  get columnConfig() {
    const renderHeader = renderTextColumn({ className: PP_TEXT_UPPERCASE });

    return [{
      title: renderHeader(formatMessage(messages.id)),
      dataIndex: 'jobNumber',
      key: 'jobNumber',
      sorter: true,
      render: (text, record) => (
        <Link to={`/jobs/${record.id}`}>
          <Ellipsis lines="1" tooltip={text}>{text}</Ellipsis>
        </Link>
      ),
      className: PP_FONT_SEMIBOLD,
    }, {
      title: renderHeader(formatMessage(messages.type)),
      dataIndex: 'jobType',
      key: 'jobType',
      render: this.renderTypeColumn,
      sorter: true,
      width: '100px',
    }, {
      title: renderHeader(formatMessage(messages.status)),
      dataIndex: 'status',
      key: 'status',
      render: this.renderStatusColumn,
      sorter: true,
      width: '160px',
    }, {
      title: renderHeader(formatMessage(messages.priority)),
      dataIndex: 'priority',
      key: 'priority',
      render: this.renderPriorityColumn,
      sorter: true,
    }, {
      title: renderHeader(formatMessage(messages.startTime)),
      dataIndex: 'startTime',
      key: 'caseName',
      render: renderTextColumn,
      sorter: true,
    }, {
      title: renderHeader(formatMessage(messages.endTime)),
      dataIndex: 'endTime',
      key: 'endTime',
      render: renderTextColumn,
      sorter: true,
    }];
  };

  renderTypeColumn = text => (
    <Tag>
      {renderOptionText(text, { className: PP_FONT_MEDIUM })}
    </Tag>
  );

  renderStatusColumn = (status, jobRecord) => {
    const { statusObj, id } = jobRecord;
    const { accessedStatuses, updateJob } = this.props;
    const nextStatuses = getNextStatuses(jobRecord, accessedStatuses);
    const options = statusObj ? [statusObj, ...nextStatuses] : nextStatuses;

    return (
      <Select
        value={status}
        className={`${Styles.select} ${Styles.status}`}
        onChange={value => updateJob(id, { 'status': value })}
        disabled={isEmpty(nextStatuses)}
      >
        {options.map(({ id: optId, name, color }) => (
          <Option key={optId} value={id}>
            <Tag
              style={{
                color,
                borderColor: color,
                cursor: isEmpty(nextStatuses) ? 'no-drop' : 'pointer',
                width: '100%',
              }}
            >
              {renderOptionText(name, { className: PP_FONT_MEDIUM })}
            </Tag>
          </Option>
        )
        )}
      </Select>
    );
  };

  renderPriorityColumn = (text, { id }) => {
    const { priorityDS, updateJob } = this.props;
    const getStyle = value => ({ color: value === JOB_PRIORITY.high ? '#F5222D' : '#FAAD14' });

    return (
      <Select
        value={text}
        className={Styles.selectNoneBorder}
        onChange={value => updateJob(id, { 'priority': value })}
        dropdownClassName={Styles.dropDownMinWidth}
      >
        {priorityDS.map(item => (
          <Select.Option key={item.value} value={item.value}>
            <div className={Styles.priority}>
              <span className={PP_FONT_MEDIUM}>{item.name}</span>
              {item.name !== JOB_PRIORITY.low &&
                <i className={`material-icons ${Styles.priorityIcon}`} style={getStyle(item.name)}>report_problem</i>}
            </div>
          </Select.Option>
        ))}
      </Select>
    );
  };

  handlePagination = pageIndex => {
    const { fetchData } = this.props;
    this.setState({ currentPage: pageIndex }, () => {
      fetchData(pageIndex);
    });
  };

  handleChange = (pagination, _, sorter) => {
    let sortParam = {};

    if (!isEmpty(sorter)) {
      const { columnKey, order } = sorter;
      sortParam = { field: columnKey, order };
    }

    const { handleSortPage } = this.props;
    if (handleSortPage) {
      handleSortPage(pagination.current, { sort: sortParam });
    }
  };

  render() {
    const { loadingJobs, selectedRows, handleSelectRow } = this.props;
    const { dataView } = this.state;

    return (
      <StandardTable
        loading={loadingJobs}
        selectedRows={selectedRows}
        onSelectRow={handleSelectRow}
        data={{
          list: dataView,
          pagination: this.paginationConfig,
        }}
        columns={this.columnConfig}
        onChange={this.handleChange}
        rowKey={record => record.id}
      />
    );
  }
}

export default OpenJob;
