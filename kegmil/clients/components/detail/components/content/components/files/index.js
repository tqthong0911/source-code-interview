import React from 'react';

import { Tabs } from '@/componentsPP/UIElements';
import Styles from './styles.less';

const { TabPane } = Tabs;

const Files = () => (
  <Tabs type="card">
    <TabPane key="photos" tab="Photos" className={Styles.tab} />
  </Tabs>
);

Files.propTypes = {
}

Files.defaultProps = {
}

export default Files;
