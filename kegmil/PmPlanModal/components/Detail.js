import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { get } from 'lodash';
import { Form, Row, Col, Select, DatePicker, Checkbox, InputNumber } from '@/componentsPP/UIElements';
import { withAuthorized, PERMISSIONS } from '@/utils/permission';
import { PP_FORM_INPUT, PP_FORM_SELECT } from '@/constants';
import { FREQUENCY_TYPE, GEN_TIMEFRAME_TYPE, PMPLAN_FORM } from '../constants';
import messages from '../messages';
import Styles from '../styles.less';

const { Item: FormItem } = Form;
const { Option } = Select;

const UpdateAuthorizedCheckbox = withAuthorized({
  requiredPermission: PERMISSIONS.job.update,
})(Checkbox);

class Detail extends PureComponent {
  static propTypes = {
    form: PropTypes.object.isRequired,
  };

  render() {
    const { form: { getFieldDecorator }, pmPlan } = this.props;
    const autoGen = get(pmPlan, 'autoGen.value', false);
    return (
      <Fragment>
        <Row gutter={16}>
          <Col sm={24} md={10}>
            <div className={Styles.labelRequired}>
              {formatMessage(messages.frequency)}
            </div>
            <div className={Styles.doubleFormItem}>
              <FormItem>
                {getFieldDecorator(PMPLAN_FORM.frequency, {
                  rules: [{ required: true, message: formatMessage(messages.required), type: 'integer' }],
                })(
                  <InputNumber
                    className={PP_FORM_INPUT}
                    min={1}
                    placeholder={formatMessage(messages.frequencyPlaceholder)}
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator(PMPLAN_FORM.frequencyType, {
                  rules: [{ required: true, message: formatMessage(messages.required) }],
                })(
                  <Select
                    className={PP_FORM_SELECT}
                    placeholder={formatMessage(messages.frequencyTypePlaceholder)}
                  >
                    {FREQUENCY_TYPE.map(opt => <Option key={opt} value={opt}>{opt}</Option>)}
                  </Select>
                )}
              </FormItem>
            </div>
          </Col>
          <Col sm={24} md={10}>
            <div className={Styles.labelRequired}>
              {formatMessage(messages.generateTimeframe)}
            </div>
            <div className={Styles.doubleFormItem}>
              <FormItem>
                {getFieldDecorator(PMPLAN_FORM.genTimeframe, {
                  rules: [{ required: true, message: formatMessage(messages.required), type: 'integer' }],
                })(
                  <InputNumber
                    className={PP_FORM_INPUT}
                    min={1}
                    placeholder={formatMessage(messages.genTimeframePlaceholder)}
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator(PMPLAN_FORM.genTimeframeType, {
                  rules: [{ required: true, message: formatMessage(messages.required) }],
                })(
                  <Select
                    className={PP_FORM_SELECT}
                    placeholder={formatMessage(messages.genTimeframeTypePlaceholder)}
                  >
                    {GEN_TIMEFRAME_TYPE.map(opt => <Option key={opt} value={opt}>{opt}</Option>)}
                  </Select>
                )}
              </FormItem>
            </div>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col sm={24} md={10}>
            <FormItem label={formatMessage(messages.firstDate)}>
              {getFieldDecorator(PMPLAN_FORM.firstDate, {
                rules: [{ required: true, message: formatMessage(messages.required), type: 'object' }],
              })(
                <DatePicker className={Styles.datePicker} placeholder={formatMessage(messages.datePlaceholder)} />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col sm={24} md={10}>
            <FormItem>
              {getFieldDecorator(PMPLAN_FORM.autoGen, {
                valuePropName: 'checked',
              })(<UpdateAuthorizedCheckbox>{formatMessage(messages.autoGen)}</UpdateAuthorizedCheckbox>)}
            </FormItem>
          </Col>
        </Row>
        {autoGen && (
          <Row gutter={16}>
            <Col sm={24} md={10}>
              <FormItem label={formatMessage(messages.labelGenHorizon)}>
                {getFieldDecorator(PMPLAN_FORM.genHorizon)(
                  <InputNumber
                    className={PP_FORM_INPUT}
                    min={1}
                    placeholder={formatMessage(messages.placeholderGenHorizon)}
                  />
                )}
              </FormItem>
            </Col>
          </Row>
        )}
      </Fragment>
    );
  }
}

export default Detail;
