import { PAGE_SIZE, EMPTY_VALUE } from '@/constants';
import { getClient, mapClientsToView } from '../service';

let mockRequest;
let mockRequestCustomField;
jest.mock('@/utils/request', () => {
  mockRequest = jest.fn();
  return mockRequest;
});
jest.mock('@/utils/customField/requestCustomField', () => {
  mockRequestCustomField = jest.fn();
  return mockRequestCustomField;
});

describe('Client service', () => {
  describe('getClient', () => {
    it('should call request by default page when it calls getAll', async() => {
      await getClient();
      expect(mockRequestCustomField).toHaveBeenCalledWith(`/client?page=1&size=${PAGE_SIZE}`);
    });

    it('should call request by current page when it calls getAll', async() => {
      await getClient(2);
      expect(mockRequestCustomField).toHaveBeenCalledWith(`/client?page=2&size=${PAGE_SIZE}`);
    });
  });

  describe('mapClientsToView', () => {
    let clients;
    beforeEach(() => {
      clients = [1, 2].map(value => ({
        id: value,
        name: `Company ${value}`,
        ownerId: `${value}`,
        owner: { id: `${value}`, name: `Owner ${value}` },
        address: {
          unitNum: `97${value}`,
          street: 'Main Street',
          city: 'N.Y',
          country: 'US',
        },
      }));
    });

    it('should return client list which maps to address', () => {
      const result = mapClientsToView(clients);

      expect(result.length).toBe(2);
      expect(result[0].id).toBe(1);
      expect(result[0].name).toBe('Company 1');
      expect(result[0].address).toBe('Main Street, 971, N.Y, US');
      expect(result[0].phones).toBe('--');
      expect(result[0].owner).toBe('Owner 1');
      expect(result[0].recentActivity).toBe(EMPTY_VALUE);
      expect(result[1].id).toBe(2);
      expect(result[1].name).toBe('Company 2');
      expect(result[1].address).toBe('Main Street, 972, N.Y, US');
      expect(result[1].phones).toBe('--');
      expect(result[1].owner).toBe('Owner 2');
      expect(result[1].recentActivity).toBe(EMPTY_VALUE);
    });

    it('should return client list which maps to phones', () => {
      const newClients = clients.map(client => ({
        ...client,
        phones: [{ value: '0909999999' }],
      }));
      const result = mapClientsToView(newClients);

      expect(result.length).toBe(2);
      expect(result[0].phones).toBe('0909999999');
      expect(result[1].phones).toBe('0909999999');
    });
  });
});
