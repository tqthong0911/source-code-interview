import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { Modal, Button, Form, Row, Col, Tabs, Spin } from '@/componentsPP/UIElements';
import { SUBMIT_BUTTON } from '@/constants';
import Information from './components/Information';
import Detail from './components/Detail';
import Asset from './components/Asset';
import messages from './messages';
import Styles from './styles.less';

const { createFormField } = Form;
const { TabPane } = Tabs;

@Form.create({
  mapPropsToFields(props) {
    return Object.keys(props.pmPlan).reduce((acc, fieldName) => {
      acc[fieldName] = createFormField(props.pmPlan[fieldName]);
      return acc;
    }, {});
  },
  onValuesChange: (props, values, allValues) => {
    props.handleValuesChange(values, allValues);
  },
  onFieldsChange(props, fields) {
    props.handleFieldsChange(fields);
  },
})
class PmPlanModalView extends PureComponent {
  static propTypes = {
    isEdit: PropTypes.bool.isRequired,
    initData: PropTypes.object.isRequired,
    handleSave: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired,
    handleSaveAndNew: PropTypes.func.isRequired,
    handleUpdate: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      submitButton: '',
    }
  }

  handleSubmit = (submit, values, submitButton) => {
    const { form } = this.props;
    this.setState({ submitButton }, () => {
      submit(form, values);
    });
  };

  renderFooter = () => {
    const { isEdit, handleSave, handleSaveAndNew, handleUpdate, handleCancel, loading, disableUpdate } = this.props;
    const { submitButton } = this.state;

    const handleSubmit = values => this.handleSubmit(handleSave, values, SUBMIT_BUTTON.create);
    const handleSubmitAndNew = values => this.handleSubmit(handleSaveAndNew, values, SUBMIT_BUTTON.saveAndNew);
    const handleSubmitUpdate = values => this.handleSubmit(handleUpdate, values, SUBMIT_BUTTON.update);
    const submitButtons = isEdit
      ? [
        <Button
          key="submit"
          type="primary"
          loading={loading && submitButton === SUBMIT_BUTTON.update}
          disabled={loading || disableUpdate}
          onClick={handleSubmitUpdate}
        >
          {formatMessage(messages.update)}
        </Button>,
      ]
      : [
        <Button
          key="submitAndNew"
          loading={loading && submitButton === SUBMIT_BUTTON.saveAndNew}
          disabled={loading}
          onClick={handleSubmitAndNew}
        >
          {formatMessage(messages.saveAndNew)}
        </Button>,
        <Button
          key="submit"
          type="primary"
          loading={loading && submitButton === SUBMIT_BUTTON.save}
          disabled={loading}
          onClick={handleSubmit}
        >
          {formatMessage(messages.save)}
        </Button>,
      ];
    return [
      <Button key="cancel" onClick={handleCancel}>
        {formatMessage(messages.cancel)}
      </Button>,
      ...submitButtons,
    ];
  };

  render() {
    const {
      form, isEdit, initData, isOpen, handleCancel, loading, pmPlan, onAssetsChange, clientId } = this.props;
    const title = formatMessage(isEdit ? messages.editPmPlan : messages.newPmPlan);
    const { assets, serviceContracts } = initData;
    return (
      <Modal
        title={title}
        centered
        visible={isOpen}
        onCancel={handleCancel}
        destroyOnClose
        maskClosable={false}
        footer={null}
        width={1000}
        className={Styles.wrapper}
      >
        <Spin spinning={loading}>
          <Form layout="vertical">
            <Row type="flex">
              <Col className={Styles.leftSide}>
                <Information
                  isEdit={isEdit}
                  form={form}
                  initData={initData}
                  pmPlan={pmPlan}
                  clientId={clientId}
                />
              </Col>
              <Col className={Styles.rightSide}>
                <Row type="flex" justify="space-between">
                  <Col xs={24}>
                    <Tabs defaultActiveKey="1" onChange={() => { }} className={Styles.tabWrapper}>
                      <TabPane tab={formatMessage(messages.details)} key="1" className={Styles.tabPane} forceRender>
                        <Detail form={form} pmPlan={pmPlan} />
                      </TabPane>
                      <TabPane tab={formatMessage(messages.asset)} key="2" className={Styles.tabPane} forceRender>
                        <Asset
                          pmPlan={pmPlan}
                          assets={assets}
                          serviceContracts={serviceContracts}
                          form={form}
                          onAssetsChange={onAssetsChange}
                        />
                      </TabPane>
                      <TabPane tab={formatMessage(messages.triggers)} key="3" className={Styles.tabPane} forceRender />
                    </Tabs>
                  </Col>
                  <Col xs={24} className={Styles.footer}>
                    {this.renderFooter()}
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Spin>
      </Modal>
    );
  }
}
export default PmPlanModalView;
