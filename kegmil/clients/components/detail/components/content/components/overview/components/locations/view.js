import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';

import { Card } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';
import LocationModal from '@/componentsPP/_share/LocationModal';

import Styles from './styles.less';
import LocationItem from './LocationItem';
import { ACTIONS } from './constants';

const LocationsView = (props) => {
  const {
    locations,
    contacts,
    onEdit,
    onDelete,
    modalType,
    onNew,
    onCloseNew,
    onCloseEdit,
    selectedLocation,
    onCreateOrUpdateSuccess,
    clientId,
  } = props;
  return (
    <Fragment>
      <LocationModal
        visible={modalType === ACTIONS.new}
        onCancel={onCloseNew}
        onSuccess={onCreateOrUpdateSuccess}
        overriddenLocation={{ clientIds: [clientId] }}
      />
      <LocationModal
        visible={modalType === ACTIONS.edit}
        onCancel={onCloseEdit}
        onSuccess={onCreateOrUpdateSuccess}
        id={selectedLocation && selectedLocation.id}
      />
      <Card
        title="Client Locations"
        bordered={false}
        className={Styles.wrapper}
        extra={<IconButton className={Styles.addButton} icon="add" onClick={onNew} />}
      >
        {locations.map((location) => (
          <LocationItem
            key={location.id}
            location={location}
            contacts={contacts}
            onEdit={onEdit}
            onDelete={onDelete}
          />
        ))}
      </Card>
    </Fragment>
  )
}

LocationsView.propTypes = {
  locations: PropTypes.array,
  contacts: PropTypes.array,
};

LocationsView.defaultProps = {
  locations: [],
  contacts: [],
}

export default memoize(LocationsView);
