import React from 'react';
import { shallow } from 'enzyme';
import Assets from '../';
import OpenAssets from '../components/open';

describe('<Assets />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      client: {
        id: '1',
      },
      assets: [1, 2, 3].map(e => ({
        id: e,
        name: `${e}`,
      })),
      changedIndexClientDetail: 0,
      modelAssetFinish: '0',
      initData: {
        commonLocations: [],
      },
      actions: {
        handleChangeAsset: jest.fn(),
      },
      dispatch: jest.fn(),
      loadingAssets: false,
    };
    wrapper = shallow(<Assets.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(OpenAssets).length).toBe(1);
  });

  it('should get list asset detail when it calls componentDidMount', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapperInstance.componentDidMount();

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should delete asset in pmPlan detail when call handleDeleteAsset method', () => {
    const assetIds = [1, 2];
    const closeModal = jest.fn();
    wrapperInstance.handleDeleteAsset(assetIds, closeModal);
    expect(props.dispatch).toHaveBeenCalled();
  });

  it('should get list asset detail when it change assetIds in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ client: { id: 2 } });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should edit asset finish when it change modelAssetFinish in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ modelAssetFinish: '123' });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should pmPlan detail change when it change changedIndexClientDetail in componentDidUpdate', () => {
    wrapperInstance.getListAssetDetail = jest.fn();
    wrapper.setProps({ changedIndexClientDetail: 1 });

    expect(wrapperInstance.getListAssetDetail).toHaveBeenCalled();
  });

  it('should show dialog Asset when it call handleEdit', () => {
    wrapperInstance.state.isOpenAssetModal = false;
    wrapperInstance.handleEdit({ stopPropagation: jest.fn() });

    expect(wrapperInstance.state.isOpenAssetModal).toBe(true);
  });

  it('should close dialog Asset when it call handleCancelEditAsset', () => {
    wrapperInstance.state.isOpenAssetModal = true;
    wrapperInstance.handleCancelEditAsset();

    expect(wrapperInstance.state.isOpenAssetModal).toBe(false);
  });

  it('should set new selectRow when it call handleSelectRow', () => {
    wrapperInstance.state.selectedRows = [];
    wrapperInstance.handleSelectRow([1, 2]);

    expect(wrapperInstance.state.selectedRows.length).toBe(2);
  });
});
