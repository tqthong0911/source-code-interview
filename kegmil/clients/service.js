import { get } from 'lodash';
import request from '@/utils/request';
import requestCustomField from '@/utils/customField/requestCustomField';
import { formatAddress, getContact } from '@/utils/utils';
import { buildFilterParam } from '@/utils/client';
import { PAGE_SIZE, EMPTY_VALUE } from '@/constants';

export async function getClient(currentPage = 1, filter = {}) {
  const filterQueryString = buildFilterParam(filter);
  return requestCustomField(`/client?page=${currentPage}&size=${PAGE_SIZE}${filterQueryString}`);
}

export async function deleteClient(clientId) {
  return request(`/client/${clientId}`, {
    method: 'DELETE',
  });
}

export function mapClientsToView(clients) {
  return clients.map(client => {
    const { id, name, owner, address, phones } = client;
    return {
      id,
      name,
      address: formatAddress(address),
      phones: getContact(phones),
      owner: get(owner, 'name', EMPTY_VALUE),
      recentActivity: EMPTY_VALUE,
    };
  });
}
