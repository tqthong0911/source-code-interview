import moment from 'moment';
import { get } from 'lodash';
import { EMPTY_VALUE, DATE_FORMAT } from '@/constants';

export const getFieldsToDisplay = (pmPlan, initData) => {
  const { client = {}, jobTypes = [], location = {}, contract, statuses = [] } = initData;
  const {
    name,
    description,
    firstDate,
    endDate,
    status,
    frequency,
    frequencyType,
    genTimeframe,
    genTimeframeType,
    jobType: jobTypeId,
    autoGen,
    genHorizon,
  } = pmPlan;
  return {
    name: name || EMPTY_VALUE,
    clientId: get(client, 'id', ''),
    clientName: client.name || EMPTY_VALUE,

    contractId: get(contract, 'id'),
    contractName: get(contract, 'name'),

    jobTypeName: (jobTypes.find(jobType => jobType.id === jobTypeId) || {}).name || EMPTY_VALUE,
    status: (statuses.find(e => e.id === status) || {}).name || EMPTY_VALUE,

    locationId: get(location, 'id', ''),
    locationName: location.name || EMPTY_VALUE,

    description: description || EMPTY_VALUE,
    firstDate: firstDate ? moment(parseInt(firstDate, 0)).format(DATE_FORMAT) : EMPTY_VALUE,
    endDate: endDate ? moment(parseInt(endDate, 0)).format(DATE_FORMAT) : EMPTY_VALUE,

    frequencyDisplay: frequency ? `${frequency} ${frequencyType}` : EMPTY_VALUE,
    genTimeframeDisplay: genTimeframe ? `${genTimeframe} ${genTimeframeType}` : EMPTY_VALUE,
    
    autoGen: autoGen || false,
    genHorizon: genHorizon || EMPTY_VALUE,
  }
};
