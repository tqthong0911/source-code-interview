import React from 'react';
import { shallow } from 'enzyme';
import { Modal } from '@/componentsPP/UIElements';
import Overview from '../components/overview';
import PmPlanDetailView from '../view';

describe('<PmPlanDetailView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      handleDelete: jest.fn(),
      updatePmPlanDetail: jest.fn(),
      handleRefresh: jest.fn(),
      pmPlanId: `1`,
      pmPlan: {
        id: `1`,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
      },
      initData: {
        statuses: [1, 2].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
      fetchPmPlanIndex: 0,
      changedIndex: 0,
      pmPlanUpdating: false,
      isCounting: false,
      quantity: {
        jobs: 0,
        assets: 0,
      },
      match: { params: { id: '1' } },
      loading: false,
      dispatch: jest.fn(),
    };
    wrapper = shallow(<PmPlanDetailView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(Overview).length).toBe(1);
  });

  it('should set false for isOpenEditModal when it calls handleCloseEditModal', () => {
    wrapperInstance.handleCloseEditModal();
    expect(wrapperInstance.state.isOpenEditModal).toBe(false);
  });

  it('should refresh when it calls handleRefresh', () => {
    wrapperInstance.handleRefresh();
    expect(props.handleRefresh).toHaveBeenCalled();
  });

  it('should set true for isOpenEditModal when it calls handleEdit', () => {
    wrapperInstance.state.isOpenEditModal = false;
    wrapperInstance.handleEdit();
    expect(wrapperInstance.state.isOpenEditModal).toBe(true);
  });

  it('should change status when it calls handleChangeStatus', () => {
    wrapperInstance.handleChangeStatus({ status: 'status' });
    expect(props.updatePmPlanDetail).toHaveBeenCalled();
  });

  it('should change autoGen when it calls handleChangeAutoGen', () => {
    wrapperInstance.handleChangeAutoGen({ status: 'status' });
    expect(props.updatePmPlanDetail).toHaveBeenCalled();
  });

  it('should set value for delete all job create by PmPlan when it calls handleCheckbox', () => {
    wrapperInstance.state.isDeleteAllJobs = false;
    wrapperInstance.handleCheckbox({ target: { checked: true } });

    expect(wrapperInstance.state.isDeleteAllJobs).toBeTruthy();
  });

  it('should show dialog confirm delete pmPlan when it calls handleDelete', () => {
    Modal.confirm = jest.fn();
    wrapperInstance.handleDelete();

    expect(Modal.confirm).toHaveBeenCalled();
  });
});
