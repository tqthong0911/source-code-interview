import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { get } from 'lodash';
import { Form, Select, Input, DatePicker } from '@/componentsPP/UIElements';
import { PM_PLAN_STATUSES } from '@/utils/pmPlan';
import withConfirmation from '@/componentsPP/withConfirmation';
import { PP_FORM_INPUT, PP_FORM_SELECT } from '@/constants';
import { PMPLAN_FORM } from '../constants';
import messages from '../messages';
import Styles from '../styles.less';

const { Item: FormItem } = Form;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const WithConfirmationSelect = withConfirmation(Select);

class Information extends PureComponent {
  static propTypes = {
    form: PropTypes.object.isRequired,
    initData: PropTypes.object.isRequired,
  };

  handleOk = (dataSelect) => {
    const { form: { setFieldsValue } } = this.props
    setFieldsValue({
      [PMPLAN_FORM.clientId]: dataSelect.id,
    });
  };

  isAssetsNotEmpty = () => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {});
    return Object.keys(assets).length > 0;
  }

  render() {
    const { form, initData, clientId } = this.props;
    const { jobType, status, serviceContracts, clients, locations, commonLocations } = initData;
    const { getFieldDecorator } = form;
    return (
      <div>
        <FormItem label={formatMessage(messages.name)}>
          {getFieldDecorator(PMPLAN_FORM.name, {
            rules: [{
              required: true,
              message: formatMessage(messages.required),
            }, {
              whitespace: true,
              message: formatMessage(messages.whitespace),
            }],
          })(
            <Input className={PP_FORM_INPUT} placeholder={formatMessage(messages.namePlaceholder)} />
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.client)}>
          {getFieldDecorator(PMPLAN_FORM.clientId, {
            initialValue: get(clients, '0.id'),
            rules: [
              { required: true, message: formatMessage(messages.required), whitespace: true }],
          })(
            <WithConfirmationSelect
              className={PP_FORM_SELECT}
              placeholder={formatMessage(messages.searchClient)}
              confirm={{
                title: formatMessage(messages.changingConfirm),
                okText: formatMessage(messages.yes),
                cancelText: formatMessage(messages.no),
              }}
              confirmWhen={this.isAssetsNotEmpty}
              disabled={!!clientId}
            >
              {clients.map(({ id, name }) => (
                <Option key={id} value={id}>{name}</Option>
              ))}
            </WithConfirmationSelect>
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.contract)}>
          {getFieldDecorator(PMPLAN_FORM.contractId)(
            <WithConfirmationSelect
              className={PP_FORM_SELECT}
              placeholder={formatMessage(messages.searchContract)}
              confirm={{
                title: formatMessage(messages.changingConfirm),
                okText: formatMessage(messages.yes),
                cancelText: formatMessage(messages.no),
              }}
              confirmWhen={this.isAssetsNotEmpty}
            >
              {serviceContracts.map(({ id, name }) => (
                <Option key={id} value={id}>{name}</Option>
              ))}
            </WithConfirmationSelect>
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.jobType)}>
          {getFieldDecorator(PMPLAN_FORM.jobType, {
            initialValue: get(jobType, '0.id'),
            rules: [
              {
                required: true,
                message: formatMessage(messages.required),
              },
            ],
          })(
            <Select className={PP_FORM_SELECT} placeholder={formatMessage(messages.searchJobType)}>
              {jobType.map(opt => (
                <Option key={opt.id} value={opt.id}>
                  {opt.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.status)}>
          {getFieldDecorator(PMPLAN_FORM.status, {
            initialValue: PM_PLAN_STATUSES[0].id,
          })(
            <Select className={PP_FORM_SELECT} placeholder={formatMessage(messages.selectPlaceholder)}>
              {status.map(opt => (
                <Option key={opt.id} value={opt.id}>
                  {opt.name}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.startDate)}>
          {getFieldDecorator(PMPLAN_FORM.startDate, {
            rules: [{ required: true, message: formatMessage(messages.required), type: 'object' }],
          })(
            <DatePicker className={Styles.datePicker} placeholder={formatMessage(messages.datePlaceholder)} />
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.endDate)}>
          {getFieldDecorator(PMPLAN_FORM.endDate)(
            <DatePicker className={Styles.datePicker} placeholder={formatMessage(messages.datePlaceholder)} />
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.location)}>
          {getFieldDecorator(PMPLAN_FORM.locationId)(
            <Select className={PP_FORM_SELECT} placeholder={formatMessage(messages.searchLocation)}>
              <OptGroup label={formatMessage(messages.clientLocations)}>
                {locations.map(opt => (
                  <Option key={opt.id} value={opt.id}>
                    {opt.name}
                  </Option>
                ))}
              </OptGroup>
              <OptGroup label={formatMessage(messages.commonLocations)}>
                {commonLocations.map(opt => (
                  <Option key={opt.id} value={opt.id}>
                    {opt.name}
                  </Option>
                ))}
              </OptGroup>
            </Select>
          )}
        </FormItem>
        <FormItem label={formatMessage(messages.description)}>
          {getFieldDecorator(PMPLAN_FORM.description)(
            <TextArea className={PP_FORM_INPUT} placeholder={formatMessage(messages.descriptionPlaceholder)} />
          )}
        </FormItem>
      </div>
    );
  }
}

export default Information;
