import { getJobType } from '@/utils/jobType';
import { get } from 'lodash';

import { convertAccessedStatuses, deleteJob } from '@/utils/job';
import { getJobsByPmPlanId, getAccessedStatuses, updateJob } from '@/utils/job/service';

import { NAMESPACE } from './constants';

const defaultState = {
  jobs: [],
  totalPage: 0,
  totalItem: 0,
  fetchJobIndex: 0,
  changedIndex: 0,
  deletedIndex: 0,
  priority: [],
  jobTypes: [],
  accessedStatuses: [],
};

const model = {
  namespace: NAMESPACE,

  state: defaultState,

  effects: {
    *fetchJobs({ pmPlanId, currentPage, filter }, { call, put }) {
      const response = yield call(getJobsByPmPlanId, pmPlanId, currentPage, filter);
      if (response) {
        yield put({
          type: 'saveJobs',
          payload: {
            jobs: get(response, 'results.jobs', []),
            totalPage: get(response, 'results.totalPage', 0),
            totalItem: get(response, 'results.totalItem', 0),
          },
        });
      }
    },
    *fetchRelevantData(_, { all, call, put }) {
      const [jobTypeResponse, accessedStatuses] = yield all([
        call(getJobType),
        call(getAccessedStatuses),
      ]);
      if (jobTypeResponse || accessedStatuses) {
        yield put({
          type: 'saveRelevantData',
          payload: {
            jobTypes: get(jobTypeResponse, 'results.jobType', []),
            accessedStatuses: convertAccessedStatuses(get(accessedStatuses, 'results', [])),
          },
        });
      }
    },
    *refreshPage({ payload: { callback } }, { put }) {
      yield put({ type: 'update' });
      if (typeof callback === 'function') {
        callback();
      }
    },
    *updateJob({ payload: { jobInfo } }, { call, put }) {
      const response = yield call(updateJob, jobInfo);
      if (response) {
        yield put({ type: 'update' });
      }
    },
    *deleteJob({ payload: { jobIds, callback, triggerSideBarReload } }, { call, put }) {
      const response = yield call(deleteJob, jobIds);
      if (response) {
        yield put({ type: 'delete' });
        if (typeof callback === 'function') {
          callback();
        }
        if (typeof triggerSideBarReload === 'function') {
          triggerSideBarReload();
        }
      }
    },
  },

  reducers: {
    saveRelevantData(state, { payload }) {
      const { jobTypes, accessedStatuses } = payload;
      return {
        ...state,
        jobTypes,
        accessedStatuses,
      };
    },
    saveJobs(state, { payload: { jobs, totalPage, totalItem } }) {
      return {
        ...state,
        jobs,
        totalPage,
        totalItem,
        fetchJobIndex: state.fetchJobIndex + 1,
      };
    },
    update(state) {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
      };
    },
    delete(state) {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
        deletedIndex: state.deletedIndex + 1,
      };
    },
    clear(state) {
      return {
        ...state,
        ...defaultState,
        fetchJobIndex: state.fetchJobIndex,
        deletedIndex: state.deletedIndex,
        changedIndex: state.changedIndex,
      }
    },
  },
};

export default model;
