import { getClient, deleteClient } from './service';
import { NAMESPACE } from './constants';

const model = {
  namespace: NAMESPACE,

  state: {
    clients: [],
    totalPage: 0,
    totalItem: 0,
  },

  effects: {
    *fetch({ currentPage, filter }, { call, put }) {
      const response = yield call(getClient, currentPage, filter);
      if (response) {
        const {
          results: { clients, totalPage, totalItem },
        } = response;
        yield put({
          type: 'save',
          payload: {
            clients: clients || [],
            totalPage: totalPage || 0,
            totalItem: totalItem || 0,
          },
        });
      }
    },

    *delete({ payload }, { call }) {
      const { clientId, callback } = payload;
      yield call(deleteClient, clientId);
      callback();
    },
  },

  reducers: {
    save(state, { payload: { clients, totalPage, totalItem } }) {
      return {
        ...state,
        clients,
        totalPage,
        totalItem,
      };
    },

    clear(state) {
      return {
        ...state,
        clients: [],
        totalPage: 0,
        totalItem: 0,
      };
    },
  },
};

export default model;
