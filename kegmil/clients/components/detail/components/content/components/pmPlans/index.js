import React, { PureComponent } from 'react';
import { connect } from 'dva';
import withRouter from 'umi/withRouter';

import { PAGE_SIZE } from '@/constants';

import { NAMESPACE } from './constants';
import PMPlansView from './view';

@withRouter
@connect((state) => ({
  pmPlans: state[NAMESPACE].pmPlans,
  jobTypes: state[NAMESPACE].jobTypes,
  totalPage: state[NAMESPACE].totalPage,
  loading: (
    state.loading.effects[`${NAMESPACE}/fetch`] ||
    state.loading.effects[`${NAMESPACE}/fetchJobTypes`]
  ),
}))
class PMPlansContainer extends PureComponent {

  state = {
    selectedRows: [],
    visibleModal: null,
  }

  componentDidMount() {
    this.handleRefreshPMPlans();
    this.fetchJobTypes();
  }

  getIDFromProps = () => {
    const { match: { params: { id } } } = this.props;
    return id;
  }

  handleRefreshPMPlans = (page = 1, sorter) => {
    const { dispatch } = this.props;
    const id = this.getIDFromProps();
    dispatch({
      type: `${NAMESPACE}/fetch`,
      payload: {
        id,
        page,
        sorter,
        size: PAGE_SIZE,
      },
    });
  }

  handleRefreshData = () => {
    this.handleRefreshPMPlans();
    const { onFetchQuantity } = this.props;
    onFetchQuantity();
  }

  fetchJobTypes = () => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchJobTypes`,
    });
  }

  handleSelectRow = selectedRows => this.setState({ selectedRows });

  handleOpenModal = modalType => () => this.setState({ visibleModal: modalType });

  handleCloseModal = () => this.setState({ visibleModal: null });

  handleDelete = ({ id, shouldDeleteJob, callback }) => {
    const { onFetchQuantity, dispatch } = this.props;
    const onSuccessCallback = () => {
      this.setState({
        selectedRows: [],
      }, () => {
        callback();
        onFetchQuantity();
        this.handleRefreshPMPlans();
      });
    }
    dispatch({
      type: `${NAMESPACE}/delete`,
      payload: {
        shouldDeleteJob,
        id,
        callback: onSuccessCallback,
      },
    });
  }

  render() {
    const { loading, pmPlans, jobTypes, totalPage } = this.props;
    const { selectedRows, visibleModal } = this.state;
    const clientID = this.getIDFromProps();
    return (
      <PMPlansView
        loading={loading}
        pmPlans={pmPlans}
        jobTypes={jobTypes}
        onRefreshPMPlans={this.handleRefreshPMPlans}
        totalPage={totalPage}
        selectedRows={selectedRows}
        onSelectRow={this.handleSelectRow}
        visibleModal={visibleModal}
        onOpenModal={this.handleOpenModal}
        onCloseModal={this.handleCloseModal}
        clientID={clientID}
        onSuccessCallback={this.handleRefreshData}
        onDelete={this.handleDelete}
      />
    );
  }
}

export default PMPlansContainer;
export { default as model } from './model';
