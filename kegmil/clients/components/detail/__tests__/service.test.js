import { getClientDetail, deleteClient } from '../service';

let mockRequest;
jest.mock('@/utils/request', () => {
  mockRequest = jest.fn();
  return mockRequest;
});

describe('ClientDetail service', () => {
  it('should call request when call getClientDetail function', async() => {
    await getClientDetail('1');
    expect(mockRequest).toHaveBeenCalledWith('/client/1');
  });

  it('should call request when call deleteClient function', async() => {
    await deleteClient('1');
    expect(mockRequest).toHaveBeenCalledWith('/client/1', { method: 'DELETE' });
  });
});
