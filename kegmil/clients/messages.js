import { defineMessages } from 'umi/locale';
import globalMessages from '@/messages';
import { NAMESPACE } from './constants';

const messages = defineMessages({
  allClients: {
    id: `${NAMESPACE}.allClients`,
    defaultMessage: 'All Clients',
  },
  importClients: {
    id: `${NAMESPACE}.importClients`,
    defaultMessage: 'Import Clients',
  },
  importClientsDescription: {
    id: `${NAMESPACE}.importClientsDescription`,
    defaultMessage: 'Download clients import tempate as CSV file',
  },
  importContacts: {
    id: `${NAMESPACE}.importContacts`,
    defaultMessage: 'Import Contacts',
  },
  importContactsDescription: {
    id: `${NAMESPACE}.importContactsDescription`,
    defaultMessage: 'Download contacts import tempate as CSV file',
  },
});

export default {
  ...messages,

  name: globalMessages.common.name,
  address: globalMessages.common.address,
  phone: globalMessages.common.phone,
  owner: globalMessages.client.owner,
  recentActivity: globalMessages.common.recentActivity,

  deleteClient: globalMessages.client.deleteClient,
  deleteContent: globalMessages.common.deleteContent,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  refresh: globalMessages.common.refresh,
  selected: globalMessages.common.selected,
  newLocation: globalMessages.location.newLocation,
  newContact: globalMessages.contact.newContact,
  new: globalMessages.common.new,
};
