import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';

import { NAMESPACE, ACTIONS } from './constants';
import ContactsView from './view';

@connect()
class Contacts extends PureComponent {
  static propTypes = {
    clientId: PropTypes.string,
    contacts: PropTypes.array,
  }

  static defaultProps = {
    contacts: [],
    clientId: '',
  }

  state = {
    modalType: '',
    selectedContact: {},
  }

  handleDelete = (id) => {
    const { dispatch, fetchContacts } = this.props;
    dispatch({
      type: `${NAMESPACE}/delete`,
      payload: { id, callback: fetchContacts },
    });
  }

  handleNew = () => this.setState({ modalType: ACTIONS.new });

  handleCloseNew = () => this.setState({ modalType: '' });

  handleEdit = (contact) => this.setState({
    selectedContact: contact,
    modalType: ACTIONS.edit,
  });

  handleCloseEdit = () => this.setState({ selectedContact: {}, modalType: '' })

  handleCreateOrUpdateSuccess = () => {
    const { fetchContacts } = this.props;
    fetchContacts();
  }
  
  render() {
    const { contacts, clientId } = this.props;
    const { modalType, selectedContact } = this.state;
    return (
      <ContactsView
        contacts={contacts}
        modalType={modalType}
        clientId={clientId}
        selectedContact={selectedContact}
        onNew={this.handleNew}
        onCloseNew={this.handleCloseNew}
        onEdit={this.handleEdit}
        onCloseEdit={this.handleCloseEdit}
        onDelete={this.handleDelete}
        onCreateOrUpdateSuccess={this.handleCreateOrUpdateSuccess}
      />
    );
  }
}

export default Contacts;
export { default as model } from './model';
