import React from 'react';
import { shallow } from 'enzyme';
import { Modal } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';
import LeftActionView from '../LeftAction';
import NewAction from '../NewAction';

describe('<LeftActionView />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  beforeEach(() => {
    props = {
      selectedPMPlans: [1, 2].map(value => ({ id: `${value}`, title: `PMPlan ${value}` })),
      handleDelete: jest.fn(),
      handleAssign: jest.fn(),
    };
    wrapper = shallow(<LeftActionView {...props} />);
    wrapperInstance = wrapper.instance();
  });

  // INIT
  it('should render correctly when length of selectedPMPlans is equal 0', () => {
    wrapper.setProps({ selectedPMPlans: [] });

    expect(wrapper.find(NewAction).length).toBe(1);
  });

  it('should render correctly when length of selectedPMPlans is greater than 1', () => {
    // expect(wrapper.find(IconButton).length).toBe(1);
  });

  it('should render correctly when length of selectedPMPlans is equal 1', () => {
    wrapper.setProps({ selectedPMPlans: [{ id: '1', title: 'PMPlan 1' }] });

    expect(wrapper.find(IconButton).length).toBe(2);
  });

  // END INIT

  it('should handle handleCheckbox correctly', () => {
    const e = { target: { checked: true } };
    wrapperInstance.handleCheckbox(e);

    expect(wrapperInstance.state.isDeleteAllJobs).toBeTruthy();
  });

  it('should show modal confirm delete job created by PmPlan when it calls handleDelete', () => {
    Modal.confirm = jest.fn();
    wrapperInstance.handleDelete();

    expect(Modal.confirm ).toHaveBeenCalled();
  });

  it('should handle openEditModal correctly', () => {
    wrapperInstance.handleOpenEditModal();

    expect(wrapperInstance.state.isEdit).toBeTruthy();
  });

  it('should handle cancelEditModal correctly', () => {
    const callback = jest.fn();

    wrapperInstance.handleCloseEditModal(callback);

    expect(wrapperInstance.state.isEdit).toBeFalsy();
    expect(callback).toBeCalled();
  });
});
