import { getWrapper } from '@/utils/test';
import { Form } from '@/componentsPP/UIElements';

import Information from '../Information';

const { Item: FormItem } = Form;

describe('<Information />', () => {
  let props;
  let wrapper;
  const getFormDecoratorResult = jest.fn();
  const options = [1, 2].map(value => ({ id: `${value}`, name: `name${value}` }));
  beforeEach(() => {
    props = {
      form: {
        getFieldDecorator: jest.fn(() => getFormDecoratorResult),
        getFieldValue: jest.fn(),
      },
      initData: {
        clients: options,
        serviceContracts: options,
        jobType: options,
        status: options,
        locations: options,
        commonLocations: options,
      },
    };
    wrapper = getWrapper(Information, props);
  });

  it('should render correctly', () => {
    expect(wrapper.find(FormItem).length).toBe(9);
  });
});
