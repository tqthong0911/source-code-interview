module.exports = [
  { path: '/pm-plan', component: './exception/404' },
  { path: '/pm-plan/contract', name: 'contract', component: './pmPlan/contract' },
  { path: '/pm-plan/pm-plans', name: 'pmPlan', component: './pmPlan/pmPlans' },
  { path: '/pm-plan/pm-plans/:id', component: './pmPlan/pmPlans/components/detail' },
];
