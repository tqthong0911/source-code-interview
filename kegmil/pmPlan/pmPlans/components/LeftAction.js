import React, { PureComponent, Fragment } from 'react';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import { Checkbox } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';
import PMPlanModal from '@/componentsPP/_share/PmPlanModal';
import deleteModal from '@/componentsPP/DeleteModal';
import { CONFIRM_MODAL_TYPE } from '@/constants';
import NewAction from './NewAction';
import messages from '../messages';
import Styles from './LeftAction.less';

class LeftActionView extends PureComponent {
  static propTypes = {
    selectedPMPlans: PropTypes.array.isRequired,
    handleDelete: PropTypes.func.isRequired,
    fetchPMPlan: PropTypes.func.isRequired,
  };

  state = {
    isEdit: false,
    isDeleteAllJobs: true,
  };

  handleCheckbox = (e) => this.setState({ isDeleteAllJobs: e.target.checked });

  handleDelete = () => {
    const { selectedPMPlans, handleDelete } = this.props;
    const { isDeleteAllJobs: prevIsDeleteAllJobs } = this.state;
    const { name } = selectedPMPlans[0];

    deleteModal({
      type: CONFIRM_MODAL_TYPE.delete,
      title: formatMessage(messages.deletePMPlan),
      content: (
        <Fragment>
          <div>
            {formatMessage(messages.deleteContent)}
            <span className={Styles.highlight}>{` ${name}`}</span>
            ?
          </div>
          <div className={Styles.deleteGeneratedJobs}>
            <Checkbox onChange={this.handleCheckbox} defaultChecked={prevIsDeleteAllJobs}>
              {formatMessage(messages.deleteJobCreatedByPmPlan)}
            </Checkbox>
          </div>
        </Fragment>
      ),
      okText: formatMessage(messages.delete),
      cancelText: formatMessage(messages.cancel),
      onOk: (closeModal) => {
        const { isDeleteAllJobs } = this.state;
        return new Promise(() => handleDelete(selectedPMPlans, isDeleteAllJobs, closeModal)).catch(() => { });
      },
      onCancel: () => {},
    });
  };

  handleOpenEditModal = () => this.setState({ isEdit: true });

  handleCloseEditModal = (callback) => {
    this.setState({ isEdit: false }, () => {
      if (callback) callback();
    });
  };

  render() {
    const { fetchPMPlan, selectedPMPlans } = this.props;
    const { isEdit } = this.state;
    if (!isEmpty(selectedPMPlans)) {
      const isEditVisible = selectedPMPlans.length === 1;
      const isDeleteVisible = selectedPMPlans.length === 1;

      return (
        <Fragment>
          <div className={Styles.wrapper}>
            <span className={Styles.selectedRow}>
              {`${selectedPMPlans.length} ${formatMessage(messages.selected)}`}
            </span>
            {/* {isEditVisible && <IconButton icon="vertical_align_bottom" />} */}
            {isEditVisible && <IconButton icon="edit" onClick={this.handleOpenEditModal} />}
            {isDeleteVisible && <IconButton icon="delete" onClick={this.handleDelete} />}
            {/* <IconButton icon="more_horiz" /> */}
          </div>
          <PMPlanModal
            isOpen={isEdit}
            pmPlanId={selectedPMPlans[0].id}
            handleCancel={this.handleCloseEditModal}
            callback={fetchPMPlan}
          />
        </Fragment>
      );
    }
    return <NewAction handleReloadPMPlan={fetchPMPlan} />;
  }
}

export default LeftActionView;
