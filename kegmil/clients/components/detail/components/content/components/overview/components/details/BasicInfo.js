/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { isEmpty, pathOr, compose, isNil, path } from 'ramda';

import { Row, Col, Tag } from '@/componentsPP/UIElements';
import { renderBasicText } from '@/utils/layout/renderTable';

import DetailItem from './DetailItem';
import Styles from './BasicInfo.less';

const emptyNode = (<span className={Styles.empty}>{renderBasicText('--None--')}</span>);

function renderTags(tags) {
  if (isEmpty(tags)) {
    return emptyNode;
  }
  return tags.map((tag) => (<Tag key={tag}>{tag}</Tag>));
}

const renderWebsite = (website) => {
  if (isNil(website)) {
    return emptyNode;
  }
  return <a target='_blank' href={website}>{renderBasicText(website)}</a>;
}

const renderLink = type => (values) => {
  if (isEmpty(values)) {
    return emptyNode;
  }
  return values.map(({ value }) => (
    <a key={value} target='_blank' href={`${type}:${value}`}>{renderBasicText(value)}</a>)
  )
}

const renderPhones = renderLink('tel');

const renderEmails = renderLink('mailto');

const BasicInfo = ({ client }) => {
  return (
    <div className={Styles.detailWrapper}>
      <Row gutter={16}>
        <Col md={6}>
          <DetailItem label="ID">
            {get(client, 'refId', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Type">
            {get(client, 'clientTypeObj.name', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Parent Client">
            {get(client, 'parent.name', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Industry">
            {get(client, 'industryObj.name', '--None--')}
          </DetailItem>
        </Col>
      </Row>
      <Row gutter={16} className={Styles.detailRow}>
        <Col md={6}>
          <DetailItem label="Phone">
            {compose(renderPhones, pathOr([], ['phones']))(client)}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Email">
            {compose(renderEmails, pathOr([], ['emails']))(client)}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Website">
            {compose(renderWebsite, path(['website']))(client)}
          </DetailItem>
        </Col>
        <Col md={5}>
          <DetailItem label="Ownership">
            {get(client, 'ownership', '--None--')}
          </DetailItem>
        </Col>
      </Row>
      <Row gutter={16} className={Styles.detailRow}>
        <Col md={6}>
          <DetailItem label="Employees">
            {get(client, 'employees', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Visibility">
            {get(client, 'visibility', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="SLA">
            {get(client, 'slaObj.name', '--None--')}
          </DetailItem>
        </Col>
        <Col md={6}>
          <DetailItem label="Tags">
            {compose(renderTags, pathOr([], ['tags']))(client)}
          </DetailItem>
        </Col>
      </Row>
    </div>
  );
}

BasicInfo.propTypes = {
  client: PropTypes.object,
}

BasicInfo.defaultProps = {
  client: {},
}

export default BasicInfo;
