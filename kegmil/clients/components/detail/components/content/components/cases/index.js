import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { isEqual, get } from 'lodash';
import { formatMessage } from 'umi/locale';
import deleteModal from '@/componentsPP/DeleteModal';
import IconButton from '@/componentsPP/IconButton';
import { Col, Row, Tabs } from '@/componentsPP/UIElements';
import DropdownMenu from '@/componentsPP/DropdownMenu';
import CaseModal, { NAMESPACE as CASE_MODAL_NAMESPACE } from '@/componentsPP/_share/CaseModal';
import { SORT_ORDER, CONFIRM_MODAL_TYPE, PP_FONT_MEDIUM } from '@/constants';
import { NAMESPACE as DETAIL_NAMESPACE } from '../../../../constants';
import Open from './components/open';
import { NAMESPACE } from './constants';
import messages from './messages';
import Styles from './styles.less';

const { TabPane } = Tabs;

class Cases extends Component {
  static propTypes = {
    client: PropTypes.object.isRequired,
    cases: PropTypes.array.isRequired,
    refreshPageIndex: PropTypes.number,
    loadingCases: PropTypes.bool,
    totalItem: PropTypes.number.isRequired,
    totalPage: PropTypes.number.isRequired,
    commonLocations: PropTypes.array.isRequired,
    fetchCaseIndex: PropTypes.number.isRequired,
    changedIndex: PropTypes.number.isRequired,
    deletedIndex: PropTypes.number.isRequired,
    changedIndexModal: PropTypes.number.isRequired,
  };

  static defaultProps = {
    loadingCases: false,
    refreshPageIndex: 0,
  };

  constructor(props) {
    super(props);
    this.filter = { sort: { order: SORT_ORDER.ascend, field: 'caseNumber', columnKey: 'caseNumber' } };
    this.state = {
      selectedRows: [],
      isOpen: false,
    }
  }

  componentDidMount() {
    this.fetchData();
    this.triggerSideBarReload();
  }

  shouldComponentUpdate(nextProps, nextState ) {
    return !isEqual(nextProps, this.props) || !isEqual(nextState, this.state);
  }

  componentDidUpdate(prevProps) {
    const { refreshPageIndex, fetchCaseIndex, deletedIndex } = this.props;

    if (this.shouldRefetchCases(prevProps)) {
      this.fetchData();
    }
    if (prevProps.fetchCaseIndex !== fetchCaseIndex) {
      this.fetchRelevantData();
    }
    if (refreshPageIndex !== prevProps.refreshPageIndex) {
      this.handleRefresh();
    }
    if (deletedIndex !== prevProps.deletedIndex) {
      this.setState({ selectedRows: [] })
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  }

  get renderCaseModal() {
    const { commonLocations } = this.props;
    const { selectedRows, isOpen } = this.state;
    const caseId = get(selectedRows, '0.id', '');

    return caseId ? (
      <CaseModal
        commonLocations={commonLocations}
        caseId={caseId}
        isOpen={isOpen}
        handleCancel={this.handleCancel}
      />
    ) : ''
  }

  get renderActionButton() {
    const { selectedRows } = this.state;
    const style = selectedRows.length <= 0 ? { display: 'none' } : {};
    const shouldDisplayEditIcon = selectedRows.length === 1;

    return (
      <div className={Styles.headerWrapper}>
        <div className={Styles.leftActionWrapper}>
          <DropdownMenu>
            <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
            <i className="material-icons">arrow_drop_down</i>
          </DropdownMenu>
          <div className={Styles.actions} style={style}>
            <span className={Styles.selected}>{`${selectedRows.length} selected`}</span>
            {
              shouldDisplayEditIcon && (
                <IconButton
                  icon="edit"
                  onClick={this.handleOpen}
                  tooltip={{ title: formatMessage(messages.edit) }}
                />
              )
            }
            <IconButton
              icon="delete"
              onClick={this.handButtonDeleteClicked}
              tooltip={{ title: formatMessage(messages.delete) }}
            />
          </div>
        </div>
        <div className={Styles.rightActionWrapper} />
      </div>
    )
  }

  handleSelectRow = selectedRows => {
    this.setState({ selectedRows });
  };

  shouldRefetchCases = prevProps => {
    const { changedIndex, changedIndexModal, client: { id: clientId } } = this.props;
    const assetIdChanged = !isEqual(clientId, prevProps.client.id);
    const caseChanged = changedIndex !== prevProps.changedIndex;
    const caseModalChanged = changedIndexModal !== prevProps.changedIndexModal;

    return caseChanged || caseModalChanged || assetIdChanged;
  };

  handleRefresh = (callback = this.fetchData) => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/refreshPage`,
      payload: {
        callback,
      },
    });
  };

  fetchData = (currentPage = 1, filter = this.filter) => {
    const { dispatch, client: { id } } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchCases`,
      clientId: id,
      currentPage,
      filter,
    });
  };

  fetchRelevantData = () => {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/fetchRelevantData` });
  };
  
  handleSortPage = (currentPage, filter) => {
    this.filter = filter;
    this.fetchData(currentPage, filter);
  };

  updateCase = (caseId, item) => {
    const { dispatch, cases } = this.props;
    const selectedCase = cases.filter(({ id }) => id === caseId)[0];
    if (selectedCase) {
      dispatch({
        type: `${NAMESPACE}/updateCase`,
        payload: {
          caseInfo: Object.assign(selectedCase, item),
        },
      })
    }
  };

  deleteCase = (caseIds, closeModal) => {
    const { client: { id: clientId } } = this.props;
    if (caseIds.length > 0 && clientId) {
      const { dispatch } = this.props;
      dispatch({
        type: `${NAMESPACE}/deleteCase`,
        payload: {
          caseIds,
          closeModal,
          triggerSideBarReload: this.triggerSideBarReload,
        },
      });
    }
  };

  handButtonDeleteClicked = e => {
    e.stopPropagation();
    const { selectedRows } = this.state;
    const renderContent = () => {
      if (selectedRows.length === 1) {
        return (
          <span>
            {formatMessage(messages.deleteContent)}
            {' '}
            <a>{selectedRows[0].caseNumber}</a>
            {' '}
            {' '}

            ?
          </span>
        )
      }
      return <span>{formatMessage(messages.deleteMultiContent)}</span>
    };

    if (selectedRows && selectedRows.length > 0) {
      deleteModal({
        type: CONFIRM_MODAL_TYPE.delete,
        title: formatMessage(messages.deleteCase),
        content: renderContent(),
        okText: formatMessage(messages.delete),
        cancelText: formatMessage(messages.cancel),
        onOk: closeModal => {
          const caseIds = selectedRows.map(item => item.id);
          return this.deleteCase(caseIds, closeModal);
        },
        onCancel() {},
      })
    }
  };

  handleTabChanged = () => {
    this.setState({ selectedRows: [] })
  };

  handleOpen = e => {
    e.stopPropagation();
    this.setState({ isOpen: true });
  };

  handleCancel = () => {
    this.setState({ isOpen: false });
  };

  triggerSideBarReload = () => {
    const { dispatch, client } = this.props;
    dispatch({
      type: `${DETAIL_NAMESPACE}/counting`,
      payload: {
        client,
      },
    });
  };

  render() {
    const {
      cases,
      statusDS,
      totalItem,
      totalPage,
      loadingCases,
    } = this.props;
    const { selectedRows } = this.state;

    return (
      <Row justify="space-between">
        <Col>
          {this.renderActionButton}
          <Tabs defaultActiveKey="1" type="card" onChange={this.handleTabChanged} className={Styles.tabWrapper}>
            <TabPane tab="Cases" key="1" className={Styles.tabPane} forceRender>
              <Open
                cases={cases}
                statusDS={statusDS}
                totalItem={totalItem}
                totalPage={totalPage}
                selectedRows={selectedRows}
                loadingCases={loadingCases}
                handleSelectRow={this.handleSelectRow}
                updateCase={this.updateCase}
                fetchData={this.fetchData}
                handleSortPage={this.handleSortPage}
              />
            </TabPane>
          </Tabs>
        </Col>
        {this.renderCaseModal}
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    cases,
    statusDS,
    totalPage,
    totalItem,
    fetchCaseIndex,
    changedIndex,
    deletedIndex,
  } = state[NAMESPACE];
  return {
    cases,
    statusDS,
    totalPage,
    totalItem,
    fetchCaseIndex,
    changedIndex,
    deletedIndex,
    changedIndexModal: state[CASE_MODAL_NAMESPACE].changedIndex,
    loadingCases: state.loading.effects[`${NAMESPACE}/fetchCases`],
    refreshPageIndex: state[`${DETAIL_NAMESPACE}`].refreshPageIndex,
  };
};

export default connect(mapStateToProps)(Cases);
