import { getWrapper } from '@/utils/test';
import StandardTable from '@/componentsPP/StandardTable';

import Asset from '../Asset';

describe('<Asset />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      pmPlan: {
        assets: {},
      },
      form: { getFieldValue: jest.fn() },
      assets: [1, 2].map(value => ({ id: `${value}`, name: `name${value}` })),
    };
    wrapper = getWrapper(Asset, props);
  });
  
  it('should render correctly', () => {
    expect(wrapper.find(StandardTable).length).toBe(1);
  });
});
