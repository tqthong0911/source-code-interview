import requestCustomField from '@/utils/customField/requestCustomField';
import { PAGE_SIZE } from '@/constants';
import { mapSortToBE } from '@/utils/utils';

export async function getJobsByAssetId(assetId, currentPage = 1, filter = {}) {
  return requestCustomField(`/job?assetId=${assetId}&page=${currentPage}&size=${PAGE_SIZE}${mapSortToBE(filter.sort)}`);
}

export async function updateJob(jobInfo) {
  return requestCustomField(`/job`, {
    method: 'PUT',
    body: jobInfo,
  });
}

export async function getAccessedStatuses() {
  return requestCustomField(`/jobs/status/transit`);
}
