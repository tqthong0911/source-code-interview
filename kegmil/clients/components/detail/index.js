import React, { PureComponent } from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { isEqual } from 'lodash';
import ClientDetailView from './view';
import { NAMESPACE } from './constants';

@connect((state) => ({
  client: state[NAMESPACE].client,
  loading: state.loading.effects[`${NAMESPACE}/fetchClientDetails`] ||
    state.loading.effects[`${NAMESPACE}/fetchContacts`] ||
    state.loading.effects[`${NAMESPACE}/fetchLocations`],
  initData: state[NAMESPACE].initData,
  quantity: state[NAMESPACE].quantity,
  changedIndex: state[NAMESPACE].changedIndex,
}))
class ClientDetailContainer extends PureComponent {
  static propTypes = {
    client: PropTypes.object,
    initData: PropTypes.object.isRequired,
    loading: PropTypes.bool,
  }
  
  static defaultProps = {
    client: {},
    loading: false,
  }

  componentDidMount() {
    this.refreshPage();
  }

  componentDidUpdate(prevProps) {
    const { changedIndex } = this.props;
    if (!isEqual(prevProps.changedIndex, changedIndex)) {
      this.refreshPage();
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  }

  refreshPage = () => {
    this.fetchClientDetails();
    this.fetchInitData();
    this.fetchQuantity();
  }

  fetchClientDetails = () => {
    const { dispatch, match: { params: { id } } } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchClientDetails`,
      payload: { id },
    });
  };

  fetchInitData = () => {
    const { dispatch, match: { params: { id } } } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchInitData`,
      payload: { clientId: id },
    });
  };

  fetchQuantity = () => {
    const { dispatch, match: { params: { id } } } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchQuantity`,
      payload: { clientId: id },
    });
  };

  render() {
    const { client, loading, contacts, initData, quantity, locations, match: { params: { id } } } = this.props;
    return (
      <ClientDetailView
        client={client}
        contacts={contacts}
        initData={initData}
        quantity={quantity}
        loading={loading}
        locations={locations}
        clientId={id}
        onFetchQuantity={this.fetchQuantity}
      />
    )
  }
}

export default withRouter(ClientDetailContainer);
export { default as model } from './models/model';
export { NAMESPACE };
