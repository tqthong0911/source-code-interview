import React from 'react';
import { shallow } from 'enzyme';
import { SORT_ORDER } from '@/constants';
import Jobs from '../index';
import Open from '../components/open';
import { NAMESPACE as DETAIL_NAMESPACE } from '../../../../../constants';
import { NAMESPACE } from '../constants';

describe('<Jobs />', () => {
  let props;
  let wrapper;
  let wrapperInstance;
  const filter = { sort: { order: SORT_ORDER.ascend, field: 'jobNumber', columnKey: 'jobNumber' } };
  beforeEach(() => {
    props = {
      client: {
        id: '1',
      },
      jobs: [],
      totalItem: 0,
      totalPage: 0,
      fetchJobIndex: 0,
      jobModalStatus: {},
      changedIndex: 0,
      deletedIndex: 0,
      jobTypes: [],
      accessedStatuses: [],
      refreshPageIndex: 0,
      loadingJobs: false,
      dispatch: jest.fn(),
    };
    wrapper = shallow(<Jobs.WrappedComponent {...props} />);
    wrapperInstance = wrapper.instance();
  });

  it('should render correctly', () => {
    expect(wrapper.find(Open).length).toBe(1);
  });

  it('componentDidMount', () => {
    wrapperInstance.componentDidMount();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetchJobs`,
      clientId: '1',
      currentPage: 1,
      filter,
    });
  });

  it('handleOpen', () => {
    const e = { stopPropagation: jest.fn() };
    wrapperInstance.handleOpen(e);
    expect(wrapperInstance.state.isOpen).toBe(true);
  });

  it('handleCancel', () => {
    wrapperInstance.handleCancel();
    expect(wrapperInstance.state.isOpen).toBe(false);
  });

  it('handleTabChanged', () => {
    wrapperInstance.handleTabChanged();
    expect(wrapperInstance.state.selectedRows.length).toBe(0);
  });

  it('fetchRelevantData', () => {
    wrapperInstance.fetchRelevantData();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/fetchRelevantData`,
    });
  });

  it('should triggerSideBarReload in componentDidMount', () => {
    wrapperInstance.componentDidMount();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${DETAIL_NAMESPACE}/counting`,
      payload: {
        client: props.client,
      },
    });
  });

  it('should clear client list in componentWillUnmount', () => {
    wrapperInstance.componentWillUnmount();
    expect(props.dispatch).toHaveBeenCalledWith({
      type: `${NAMESPACE}/clear`,
    });
  });
});
