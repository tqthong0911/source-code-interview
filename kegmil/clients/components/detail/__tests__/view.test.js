import React from 'react';
import { shallow } from 'enzyme';

import ClientDetailView from '../view';
import Sider from '../components/Sider';
import Content from '../components/content';

describe('<ClientDetailView />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      loading: false,
      client: {},
      initData: {},
      locations: [],
      onFetchQuantity: jest.fn(),
    };
    wrapper = shallow(<ClientDetailView {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Sider).length).toBe(1);
    expect(wrapper.find(Content).length).toBe(1);
  });
});
