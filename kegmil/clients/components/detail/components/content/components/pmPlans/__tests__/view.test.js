import React from 'react';
import { shallow } from 'enzyme';
import PMPlansView from '../view';
import PMPlanTable from '../components/pmPlanTable';
import IconButton from '@/componentsPP/IconButton';

describe('<ContractsView />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      pmPlans: [],
      loading: false,
      onRefreshPMPlans: jest.fn(),
      totalPage: 0,
      selectedRows: [],
      onSelectRow: jest.fn(),
      onOpenModal: jest.fn(),
    };
    wrapper = shallow(<PMPlansView {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(PMPlanTable).length).toBe(1);
  });
  it('should render correctly when selectedRows is not empty', () => {
    props = {
      ...props,
      selectedRows: [{
        id: 'id',
      }],
    };

    wrapper = shallow(<PMPlansView {...props} />);

    expect(wrapper.find(IconButton).length).toBe(2);
  });
});
