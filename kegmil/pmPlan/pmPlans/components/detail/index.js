import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import PmPlanDetailView from './view';
import { NAMESPACE } from './constants';

@connect(state => ({
  pmPlan: state[NAMESPACE].pmPlan,
  quantity: state[NAMESPACE].quantity,
  initData: state[NAMESPACE].initData,
  fetchPmPlanIndex: state[NAMESPACE].fetchPmPlanIndex,
  changedIndex: state[NAMESPACE].changedIndex,
  pmPlanUpdating: state.loading.effects[`${NAMESPACE}/updatePmPlan`],
  isCounting: state.loading.effects[`${NAMESPACE}/counting`],
  loading: state.loading.effects[`${NAMESPACE}/fetchPmPlanDetail`],
}))
class PmPlanDetailContainer extends PureComponent {
  static propTypes = {
    pmPlan: PropTypes.object.isRequired,
    fetchPmPlanIndex: PropTypes.number.isRequired,
    changedIndex: PropTypes.number.isRequired,
    pmPlanUpdating: PropTypes.bool,
    isCounting: PropTypes.bool,
    initData: PropTypes.object.isRequired,
    quantity: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
    pmPlanUpdating: false,
    isCounting: false,
  };

  componentDidMount() {
    this.initData();
  }

  componentDidUpdate(prevProps) {
    const { fetchPmPlanIndex } = this.props;

    if (this.shouldRefetchPmPlanDetail(prevProps)) {
      this.fetchPmPlanDetail();
    }
    if (prevProps.fetchPmPlanIndex !== fetchPmPlanIndex) {
      this.counting();
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/clear` });
  }

  initData = () => {
    this.fetchPmPlanDetail();
    this.fetchInitData();
  };

  counting = () => {
    const { dispatch, pmPlan } = this.props;
    dispatch({
      type: `${NAMESPACE}/counting`,
      payload: {
        pmPlan,
      },
    });
  };

  shouldRefetchPmPlanDetail = (prevProps) => {
    const { changedIndex } = this.props;
    const pmPlanUpdated = changedIndex !== prevProps.changedIndex;
    return pmPlanUpdated;
  };

  fetchInitData = () => {
    const { dispatch } = this.props;
    dispatch({ type: `${NAMESPACE}/fetchInitData` });
  };

  fetchPmPlanDetail = () => {
    const { match: { params: { id } }, dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/fetchPmPlanDetail`,
      payload: {
        pmPlanId: id,
      },
    });
  };

  updatePmPlanDetail = item => {
    const { dispatch, pmPlan } = this.props;
    dispatch({
      type: `${NAMESPACE}/updatePmPlan`,
      payload: {
        pmPlanInfo: { ...pmPlan, ...item },
      },
    })
  };

  handleDelete = (pmPlanId, isDeleteAllJobs, closeModal) => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/delete`,
      payload: {
        pmPlanId,
        isDeleteAllJobs,
        closeModal,
      },
    });
  };

  handleRefresh = (callback = this.initData) => {
    const { dispatch } = this.props;
    dispatch({
      type: `${NAMESPACE}/refreshPage`,
      payload: {
        callback,
      },
    });
  };

  render() {
    const {
      pmPlan,
      pmPlanUpdating,
      loading,
      isCounting,
      quantity,
      initData,
      match: { params: { id: pmPlanId } },
    } = this.props;
    return (
      <PmPlanDetailView
        pmPlanId={pmPlanId}
        pmPlan={pmPlan}
        initData={initData}
        pmPlanUpdating={pmPlanUpdating}
        loading={loading}
        isCounting={isCounting}
        quantity={quantity}
        updatePmPlanDetail={this.updatePmPlanDetail}
        handleDelete={this.handleDelete}
        handleRefresh={this.handleRefresh}
      />
    );
  }
}

export default PmPlanDetailContainer;
export { NAMESPACE } from './constants';
