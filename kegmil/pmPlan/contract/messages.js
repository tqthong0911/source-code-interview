import { defineMessages } from 'umi/locale';
import globalMessages from '@/messages';

import { NAMESPACE } from './constants';

const messages = defineMessages({
  contractAll: {
    id: `${NAMESPACE}.contractAll`,
    defaultMessage: 'Contract All',
  },
  type: {
    id: `${NAMESPACE}.type`,
    defaultMessage: 'CONTRACT TYPE',
  },
  importContract: {
    id: `${NAMESPACE}.importContract`,
    defaultMessage: 'Import Contracts',
  },
  importContractDescription: {
    id: `${NAMESPACE}.importContractDescription`,
    defaultMessage: 'Download contracts import tempate as CSV file',
  },
});

export default {
  ...messages,
  client: globalMessages.client.client,
  
  deleteContract: globalMessages.contract.deleteContract,
  deleteContent: globalMessages.common.deleteContent,

  description: globalMessages.common.description,
  modified: globalMessages.common.modified,
  name: globalMessages.common.name,
  slaTerms: globalMessages.common.slaTerms,
  startDate: globalMessages.common.startDate,
  endDate: globalMessages.common.endDate,
  leadTime: globalMessages.common.leadTime,
  new: globalMessages.common.new,
  delete: globalMessages.common.delete,
  cancel: globalMessages.common.cancel,
  refresh: globalMessages.common.refresh,
  selected: globalMessages.common.selected,
};
