import { pathOr } from 'ramda';
import { get } from 'lodash';

import { NAMESPACE } from '../constants';
import { getClientDetail } from '../service';
import { getCommonLocations } from '@/utils/location';
import { getJobsByClient } from '@/utils/job/service';
import { getAssetByClient } from '@/utils/asset';
import { getCasesByClientId } from '@/utils/case';
import { fetchContracts } from '../components/content/components/contracts/services';
import { fetchClientPMPlans } from '../components/content/components/pmPlans/services';
import { fetchClientContacts } from '@/componentsPP/_share/JobModal';

const initialState = {
  client: {},
  contacts: [],
  locations: [],
  initData: {
    commonLocations: [],
    contacts: [],
  },
  quantity: {},
  changedIndex: 0,
};

export default {
  namespace: NAMESPACE,
  state: initialState,

  effects: {
    *fetchClientDetails({ payload: { id } }, { call, put }) {
      const response = yield call(getClientDetail, id);
      const client = pathOr({}, ['results', 'client'], response);
      yield put({
        type: 'saveClient',
        payload: {
          client,
        },
      });
      yield put({ 
        type: 'breadcrumb/add',
        payload: { 
          item: { id: client.id, url: `/client/clients/${client.id}`, name: client.name },
        },
      });
    },
    *fetchQuantity({ payload: { clientId } }, { call, all, put }) {
      const [
        responseAssetByClient,
        responseJobsByClient,
        responseCasesByClient,
        contractsResponse,
        pmPlansResponse,
      ] = yield all([
        call(getAssetByClient, clientId),
        call(getJobsByClient, clientId),
        call(getCasesByClientId, clientId),
        call(fetchContracts, clientId),
        call(fetchClientPMPlans, clientId),
      ]);
      yield put({
        type: 'saveQuantity',
        payload: {
          quantity: {
            assets: get(responseAssetByClient, 'results.totalItem', 0),
            jobs: get(responseJobsByClient, 'results.totalItem', 0),
            cases: get(responseCasesByClient, 'results.totalItem', 0),
            contracts: get(contractsResponse, 'totalItem', 0),
            pmPlans: get(pmPlansResponse, 'totalItem', 0),
          },
        },
      })
    },
    *fetchInitData({ payload: { clientId } }, { call, all, put }) {
      const [
        responseCommonLocations,
        responseContacts,
      ] = yield all([
        call(getCommonLocations),
        call(fetchClientContacts, clientId),
      ]);
      yield put({
        type: 'saveInitData',
        payload: {
          commonLocations: get(responseCommonLocations, 'results.locations', []),
          contacts: responseContacts,
        },
      })
    },
    *refreshPage(_, { put }) {
      yield put({ type: 'refresh' });
    },
  },
  reducers: {
    saveClient: (state, { payload: { client } }) => ({
      ...state,
      client,
    }),
    saveContacts: (state, { payload: { contacts } }) => ({
      ...state,
      contacts,
    }),
    saveLocations: (state, { payload: { locations } }) => ({
      ...state,
      locations,
    }),
    saveQuantity(state, { payload: { quantity } }) {
      const newQuantity = Object.assign({}, state.quantity, quantity);
      return {
        ...state,
        quantity: newQuantity,
      };
    },
    saveInitData(state, { payload }) {
      const newInitData = Object.assign({}, state.initData, payload);
      return {
        ...state,
        initData: newInitData,
      };
    },
    refresh: (state) => {
      return {
        ...state,
        changedIndex: state.changedIndex + 1,
      }
    },
    clear: () => initialState,
  },
}
