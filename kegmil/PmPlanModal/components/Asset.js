import React, { PureComponent, Fragment } from 'react';
import { formatMessage } from 'umi/locale';
import { get, has, cloneDeep, isUndefined, isEmpty } from 'lodash';
import { Row, Col, Select, Checkbox } from '@/componentsPP/UIElements';
import StandardTable from '@/componentsPP/StandardTable';
import { renderTextColumn, renderBasicText } from '@/utils/layout/renderTable';
import IconButton from '@/componentsPP/IconButton';
import Ellipsis from '@/componentsPP/Ellipsis';
import { PP_FORM_SELECT, EMPTY_VALUE } from '@/constants';
import { TASK_PAGE_SIZE, PMPLAN_FORM } from '../constants';
import messages from '../messages';
import Styles from './Asset.less';

const { Option } = Select;

export default class Asset extends PureComponent {

  state = {
    page: 1,
  };

  handlePageChange = (page) => this.setState({ page });

  getPaginationConfiguration = () => {
    const { page } = this.state;
    const { pmPlan } = this.props;
    const tasks = get(pmPlan, 'tasks', {});
    return {
      simple: true,
      total: Object.keys(tasks).length,
      current: page,
      pageSize: TASK_PAGE_SIZE,
      onChange: this.handlePageChange,
    };
  }

  getColumnConfig = () => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {});
    const assetOptions = this.getAssetOptions();
    const shouldFixAction = !isEmpty(assets);
    const assetFinder = (id) => (asset) => asset.id === id;

    return [{
      title: renderBasicText(formatMessage(messages.assetName)),
      dataIndex: 'assetName',
      key: 'assetName',
      width: 200,
      render: (_, { id }) => {
        const matchedAsset = assetOptions.find(assetFinder(id));
        const assetName = get(matchedAsset, 'name', EMPTY_VALUE);
        return renderTextColumn(assetName);
      },
    }, {
      title: renderBasicText(formatMessage(messages.assetId)),
      dataIndex: 'assetNumber',
      key: 'assetNumber',
      width: 200,
      render: (_, { id }) => {
        const matchedAsset = assetOptions.find(assetFinder(id));
        const assetNumber = get(matchedAsset, 'assetNumber', EMPTY_VALUE);
        return renderTextColumn(assetNumber);
      },
    }, {
      title: renderBasicText(formatMessage(messages.type)),
      dataIndex: 'category',
      key: 'category',
      width: 200,
      render: (_, { id }) => {
        const matchedAsset = assetOptions.find(assetFinder(id));
        const category = get(matchedAsset, 'categoryObj.name', EMPTY_VALUE);
        return (
          <Ellipsis lines="1">{category}</Ellipsis>
        );
      },
    }, {
      title: renderBasicText(formatMessage(messages.status)),
      dataIndex: 'status',
      key: 'status',
      width: 200,
      render: (_, { id }) => {
        const matchedAsset = assetOptions.find(assetFinder(id));
        const status = get(matchedAsset, 'statusObj.name', EMPTY_VALUE);
        return (
          <Ellipsis lines="1">{status}</Ellipsis>
        );
      },
      sorter: false,
    }, {
      title: renderBasicText(formatMessage(messages.generateTask)),
      dataIndex: 'shouldGenerateTask',
      key: 'shouldGenerateTask',
      width: 200,
      render: (shouldGenerateTask, { id }) => (
        <Checkbox checked={shouldGenerateTask} onChange={(e) => this.handleGenerateTaskChange(id, e.target.checked)} />
      ),
      sorter: false,
    }, {
      dataIndex: 'action',
      key: 'action',
      width: 50,
      fixed: shouldFixAction ? 'right' : undefined,
      render: (_, { id }) => (
        <IconButton
          icon="remove_circle_outline"
          onClick={() => this.handleRemoveAsset(id)}
          isBorder={false}
          style={{ float: 'right' }}
        />
      ),
      sorter: false,
    }];
  };

  renderAssetTable = () => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {});
    const data = {
      list: Object.keys(assets).map((id) => assets[id]),
      pagination: this.getPaginationConfiguration(),
    };
    return (
      <StandardTable
        data={data}
        className={Styles.assetTable}
        columns={this.getColumnConfig()}
        onChange={this.handleChange}
        rowKey={({ id }) => id}
        rowSelection={null}
        scroll={{ x: 1050 }}
      />
    )
  }

  handleSelectAssets = (value = []) => {
    this.propagateAssetsChange(value);
  }

  handleRemoveAsset = (assetId) => {
    const { pmPlan } = this.props;
    const selectedAssets = Object.keys(get(pmPlan, 'assets', {}));
    const newSelectedAssets = selectedAssets.filter(asset => asset !== assetId);
    this.propagateAssetsChange(newSelectedAssets);
  }

  propagateAssetsChange = (selectedAssets) => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {});
    if (isUndefined(selectedAssets)) {
      return;
    }
    const { onAssetsChange } = this.props;
    if (isEmpty(selectedAssets)) {
      onAssetsChange({});
      return;
    }
    const newAssets = selectedAssets.reduce((acc, assetId) => {
      if (has(assets, assetId)) {
        return {
          ...acc,
          [assetId]: cloneDeep(assets[assetId]),
        };
      }
      return {
        ...acc,
        [assetId]: {
          id: assetId,
          shouldGenerateTask: true,
        },
      };
    }, {});
    onAssetsChange(newAssets);
  }

  handleGenerateTaskChange = (assetId, value) => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {});
    const newAssets = Object.keys(assets).reduce((acc, id) => {
      if (id === assetId) {
        return {
          ...acc,
          [id]: {
            ...assets[id],
            shouldGenerateTask: value,
          },
        }
      }
      return {
        ...acc,
        [id]: {
          ...assets[id],
        },
      }
    }, {});
    const { onAssetsChange } = this.props;
    onAssetsChange(newAssets);
  }

  getAssetOptions = () => {
    const { form: { getFieldValue }, assets: assetList, serviceContracts } = this.props;
    const contractValue = getFieldValue(PMPLAN_FORM.contractId);
    if (!contractValue) {
      return assetList;
    }
    const byIdFinder = searchId => ({ id }) => searchId === id;
    const matchedServiceContract = serviceContracts.find(byIdFinder(contractValue));
    if (!matchedServiceContract) {
      return assetList;
    }
    const { assets: serviceContractAssets = [] } = matchedServiceContract;
    return serviceContractAssets.map((asset) => {
      const { id, genTask } = asset;
      const matchedAsset = assetList.find(byIdFinder(id));
      if (!matchedAsset) {
        return asset;
      }
      return {
        ...matchedAsset,
        genTask,
      };
    });
  }

  renderAssetSelect = () => {
    const { pmPlan } = this.props;
    const assets = get(pmPlan, 'assets', {})
    const assetOptions = this.getAssetOptions();
    return (
      <Select
        placeholder="Assets"
        showSearch
        mode="tags"
        className={PP_FORM_SELECT}
        onChange={this.handleSelectAssets}
        value={Object.keys(assets)}
        maxTagCount={2}
      >
        {assetOptions.map(({ id, name }) => (
          <Option key={id} value={id}>{name}</Option>
        ))}
      </Select>
    );
  }

  render() {
    return (
      <Fragment>
        <Row>
          <Col md={12}>
            {this.renderAssetSelect()}
          </Col>
        </Row>
        <div className={Styles.taskTableWrapper}>
          {this.renderAssetTable()}
        </div>
      </Fragment>
    );
  }
}
