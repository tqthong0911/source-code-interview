import React from "react";
import {connect} from 'react-redux';
import {toggleMultimedia} from '../../actions/listing';
import { postWatchlistItem, removeWatchlistItem } from '../../actions/watchlist';
import {updateWatchlistInListing} from '../../actions/listing';
import {toggleWatchlist} from '../../utils/common';
import * as _ from 'lodash';
import 'alertify.js/src/css/alertify.css'

class LeftHeader extends React.PureComponent {
    toggle = (type) => {
        this.props.toggleMultimedia(type);
    };

    get isVisibleMultimediaIcons(){
        const {image, video, matterport} = this.props;
        const total = image.concat(video, matterport);
        return total.length > 0;
    }

    get elmWishlist() {
        const { itemIdHash } = this.props;
        const text = itemIdHash ? 'Remove From Watchlist' : 'Add To Watchlist';
        const fileName = `${itemIdHash ? '-filled' : ''}`;

        return (
            <div className="wishlist" onClick={() => toggleWatchlist(this.props)}>
                <img src={require(`./assets/img/heart-outline${fileName}.png`)} alt={text} />
                <span>{text}</span>
            </div>
        );
    }

    render() {
        const { title, addr } = this.props;
        return (
            <div id="listing-left-header" className="col-md-8">
                <div className="wrap">
                    <div className="display-3dots">{title}</div>
                    <small>{addr}</small>
                </div>
                {this.elmWishlist}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        listing: state.ui.listingDetailPage.listing,
        itemIdHash: state.ui.listingDetailPage.itemIdHash,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateWatchlistInListing: (itemIdHash) => dispatch(updateWatchlistInListing(itemIdHash)),
        postWatchlistItem: (body) => dispatch(postWatchlistItem(body)),
        removeWatchlistItem: (id_hash) => dispatch(removeWatchlistItem(id_hash)),
        toggleMultimedia: (typeSlider) => dispatch(toggleMultimedia(typeSlider))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(LeftHeader);
