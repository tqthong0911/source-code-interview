import React, { Fragment, PureComponent } from 'react';
import { formatMessage } from 'umi/locale';
import { Tabs, Button } from '@/componentsPP/UIElements';
import IconButton from '@/componentsPP/IconButton';
import DropDownMenu from '@/componentsPP/DropdownMenu';
import ContractModal from '@/componentsPP/_share/ContractModal';
import deleteModal from '@/componentsPP/DeleteModal';
import { PP_FONT_MEDIUM, CONFIRM_MODAL_TYPE } from '@/constants';
import ContractTable from './components/contractTable';
import messages from './messages';
import Styles from './styles.less';

const { TabPane } = Tabs;

class ContractsView extends PureComponent {

  renderConfirmModalContent = () => {
    const { selectedRows } = this.props;
    const { name } = selectedRows[0];
    return (
      <Fragment>
        {formatMessage(messages.deleteContent)}
        <span className={Styles.contractName}>{name}</span>
        ?
      </Fragment>
    );
  }

  handleClickDelete = (e) => {
    e.preventDefault();
    const { selectedRows, onDelete } = this.props;
    const { id } = selectedRows[0];
    deleteModal({
      type: CONFIRM_MODAL_TYPE.delete,
      title: formatMessage(messages.deleteContract),
      content: this.renderConfirmModalContent(),
      okText: formatMessage(messages.delete),
      okButtonProps: { type: 'danger' },
      onOk: () => onDelete(id),
    });
  }

  handleClickNew = () => {
    const { onOpenModal } = this.props;
    onOpenModal('new')();
  }

  handleClickEdit = () => {
    const { onOpenModal } = this.props;
    onOpenModal('edit')();
  }

  render() {
    const {
      loading,
      contracts,
      onRefreshContracts,
      totalPage,
      onSelectRow,
      selectedRows,
      visibleModal,
      onCloseModal,
      onSuccessCallback,
      clientId,
    } = this.props;
    const style = selectedRows.length <= 0 ? { display: 'none' } : {};
    const shouldDisplayEditIcon = selectedRows.length === 1;
    return (
      <div className={Styles.contractsWrapper}>
        <div className={Styles.headerWrapper}>
          <div className={Styles.leftActionWrapper}>
            <DropDownMenu>
              <span className={PP_FONT_MEDIUM}>Recently Viewed</span>
              <i className="material-icons">arrow_drop_down</i>
            </DropDownMenu>
            <div className={Styles.actions} style={style}>
              <span className={Styles.selected}>{`${selectedRows.length} selected`}</span>
              {shouldDisplayEditIcon && (
                <Fragment>
                  <IconButton
                    icon="edit"
                    onClick={this.handleClickEdit}
                  />
                  <IconButton
                    icon="delete"
                    onClick={this.handleClickDelete}
                  />
                  <ContractModal
                    isOpen={visibleModal === 'edit'}
                    handleCancel={onCloseModal}
                    callback={onSuccessCallback}
                    clientId={clientId}
                    contractId={selectedRows[0].id}
                  />
                </Fragment>
              )}
            </div>
            {selectedRows.length === 0 && (
              <Fragment>
                <ContractModal
                  isOpen={visibleModal === 'new'}
                  handleCancel={onCloseModal}
                  callback={onSuccessCallback}
                  clientId={clientId}
                />
                <Button
                  type="primary"
                  size="small"
                  className={Styles.newBtn}
                  onClick={this.handleClickNew}
                >
                  {formatMessage(messages.new)}
                </Button>
              </Fragment>
            )}
          </div>
        </div>
        <Tabs type="card">
          <TabPane key="contracts" tab={formatMessage(messages.contracts)} className={Styles.contractsTabWrapper}>
            <ContractTable
              loading={loading}
              contracts={contracts}
              onRefreshContracts={onRefreshContracts}
              totalPage={totalPage}
              onSelectRow={onSelectRow}
              selectedRows={selectedRows}
            />
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

export default ContractsView;
