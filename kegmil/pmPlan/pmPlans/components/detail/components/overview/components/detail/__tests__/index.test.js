import React from 'react';
import { shallow } from 'enzyme';

import { Row } from '@/componentsPP/UIElements';
import Detail from '../';

describe('<Detail />', () => {
  let props;
  let wrapper;
  beforeEach(() => {
    props = {
      pmPlan: {
        id: 1,
        name: '1',
        description: '1',
        firstDate: '1',
        endDate: '1',
        status: '1',
        frequency: '1',
        frequencyType: '1',
        genTimeframe: '1',
        genTimeframeType: '1',
        jobType: '1',
      },
      initData: {
        statuses: [1, 2].map(value => ({
          id: `${value}`,
          name: `${value}`,
        })),
      },
    };
    wrapper = shallow(<Detail {...props} />);
  });

  it('should render correctly', () => {
    expect(wrapper.find(Row).length).toBe(6);
  });
});
