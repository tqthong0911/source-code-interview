import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi/locale';
import PageHeaderLayout from '@/layouts/PageHeaderLayout';
import IconButton from '@/componentsPP/IconButton';
import { PP_FONT_MEDIUM } from '@/constants';
import LeftAction from './components/LeftAction';
import TableView from './components/TableView';
import messages from './messages';
import Styles from './styles.less';

class PMPlansView extends PureComponent {
  static propTypes = {
    jobsType: PropTypes.array.isRequired,
    pmPlans: PropTypes.array.isRequired,
    totalItem: PropTypes.string.isRequired,
    currentPage: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleReloadPMPlan: PropTypes.func.isRequired,
    fetchPMPlan: PropTypes.func.isRequired,
    handleChangeFilterTableView: PropTypes.func.isRequired,
  };

  state = {
    selectedRows: [],
  };

  get renderAction() {
    return (
      <div className={Styles.action}>
        <IconButton
          icon="refresh"
          onClick={this.handleReloadPMPlan}
          tooltip={{ title: formatMessage(messages.refresh), placement: 'topRight' }}
        />
        {/* <IconButton icon="view_column" /> */}
      </div>
    );
  };

  handleReloadPMPlan = () => {
    const { handleReloadPMPlan } = this.props
    this.setState({ selectedRows: [] }, () => handleReloadPMPlan());
  }

  handleUpdateSelectedRows = selectedRows => {
    this.setState({ selectedRows });
  };

  renderLeftAction = () => {
    const { pmPlans, fetchPMPlan, handleDelete } = this.props;
    const { selectedRows } = this.state;
    const selectedPMPlans = pmPlans.filter(pmPlan => selectedRows.some(row => row.id === pmPlan.id));
    return (
      <LeftAction
        selectedPMPlans={selectedPMPlans}
        handleDelete={handleDelete}
        fetchPMPlan={fetchPMPlan}
      />
    );
  };

  renderTitle = () => {
    const { totalItem } = this.props;
    return (
      <div>
        <span className={PP_FONT_MEDIUM}>{`${formatMessage(messages.allPMPlans)} (${totalItem})`}</span>
        <i className="material-icons">arrow_drop_down</i>
      </div>
    );
  };

  render() {
    const { pmPlans, jobsType, handleChangeFilterTableView, totalPage, loading, currentPage } = this.props;
    const { selectedRows } = this.state;

    return (
      <PageHeaderLayout
        icon="menu-unfold"
        title={this.renderTitle()}
        hideBreadcrumb
        action={this.renderAction}
        leftAction={this.renderLeftAction()}
        contentClassName={Styles.wrapper}
        iconContainerClassName={Styles.iconContainer}
      >
        <TableView
          pmPlans={pmPlans}
          jobsType={jobsType}
          loading={loading}
          totalPage={totalPage}
          currentPage={currentPage}
          selectedRows={selectedRows}
          handleUpdateSelectedRows={this.handleUpdateSelectedRows}
          handleChangeFilterTableView={handleChangeFilterTableView}
          className={Styles.tableWrapper}
        />
      </PageHeaderLayout>
    );
  }
}

export default PMPlansView;
