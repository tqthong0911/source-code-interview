export const NAMESPACE = 'pages.client.detail';

export const DETAIL_MENU = [
  { key: 'overview', name: 'Overview' },
  { key: 'assets', name: 'Assets' },
  { key: 'jobs', name: 'Jobs' },
  { key: 'cases', name: 'Cases' },
  { key: 'contracts', name: 'Contracts' },
  { key: 'pmPlans', name: 'PM Plans' },
];
